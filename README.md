# squeak

Squeak is a utilities library just like underscore or sugar are (except that it actually uses them and "extends" them).
It doesn't aim to be in any way exhaustive but tries to be a set of rather clean and usable functions to make your programming life faster.

For the full documentation, installation instructions... check [squeak's documentation page](https://squeak.eauchat.org/libs/squeak/).
