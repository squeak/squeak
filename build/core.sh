#!/bin/bash

browserify ./core -d -s squeak -x underscore -o ./dist/core.js
uglifyjs ./dist/core.js > ./dist/core.min.js
