#!/bin/bash

readonly NORMAL=$(printf '\033[0m')
readonly BOLD=$(printf '\033[1m')
readonly faint=$(printf '\033[2m')
readonly UNDERLINE=$(printf '\033[4m')
readonly NEGATIVE=$(printf '\033[7m')
readonly RED=$(printf '\033[31m')
readonly GREEN=$(printf '\033[32m')
readonly ORANGE=$(printf '\033[33m')
readonly BLUE=$(printf '\033[34m')
readonly YELLOW=$(printf '\033[93m')
readonly WHITE=$(printf '\033[39m')
readonly PURPLE=$(printf '\033[0;35m')

function help() {
  cat <<EOF

  ${BOLD}${RED}Build requires 3 arguments:${NORMAL}
      ${GREEN}path/to/file/specifying/required/pieces.js${NORMAL}
      ${GREEN}path/to/where/to/save/build.js${NORMAL}
      ${GREEN}path/to/where/to/save/build.min.js${NORMAL}

EOF
}

###############################################################################


# if not three arguments
if [ "$#" -ne 3 ]; then
  if [ "$1" == "help" ]; then
    help;
    exit;
  fi
  echo "${BOLD}${RED}ERROR:${faint} build requires 3 arguments${NORMAL}";
  exit;
fi

###############################################################################

# browserify and uglify like requested
browserify "$1" -d -s squeak -x underscore -o "$2"
uglifyjs "$2" > "$3"
