#!/bin/bash

FILES=./extension/*
for f in $FILES
do

  # decompose file name
  filename="${f##*/}"
  dir="${f:0:${#f} - ${#filename}}"
  base="${filename%.[^.]*}"
  ext="${filename:${#base} + 1}"
  # filename=$(basename -- "$f")
  # EXTENSION="${filename##*.}"
  # NAME="${filename%.*}"

  browserify $f -d -s squeak_$base -x underscore -u ../core -o ./dist/$f
  uglifyjs ./dist/$f > ./dist/$dir$base.min.$ext
  echo browserified $f

done
