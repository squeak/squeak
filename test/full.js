var _ = require("underscore");
var $$ = require("../extension");
require("../plugin/log.node.js");

$$.log.style("", "magentaBright", true);
$$.log.style("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡", "magentaBright", true);
$$.log.style("!!!!!!!!!!!!!!!!!!!", "magentaBright", true);
$$.log.style("Testing squeak.full", "magentaBright", true);
$$.log.style("", "magentaBright", true);



//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  METHODS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var methods = {

  //
  //                              storage.js (cannot test storage in node, localStorage and sessionStorage don't exist, storage is only for browser)

  // "$$.storage": "",
  // "$$.storage.local": "",
  // "$$.storage.session": "",

  //
  //                              string

  "$$.string": "",

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RUN ALL TESTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

_.each(methods, function(method, methodName){

  var element = eval(methodName);

  if (element) $$.log.style(methodName +" exists", "greenBright")
  else $$.log.style(methodName +" is missing", "redBright");

  if (_.isFunction(method)) {
    $$.log.style("Started testing "+ methodName, "greenBright");
    method(function () {
      $$.log.style("Tested "+ methodName +" successfully", "green");
    }, function () {
      $$.log.style("Error testing "+ methodName, "red");
    });
  };

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

$$.log.style("", "magentaBright", true);
$$.log.style("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡", "magentaBright", true);
$$.log.style("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡", "magentaBright", true);
