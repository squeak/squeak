var _ = require("underscore");
var $$ = require("../core");
require("../plugin/log.node.js");

$$.log.style("!!!!!!!!!!!!!!!!!!!", "magentaBright", true);
$$.log.style("!!!!!!!!!!!!!!!!!!!", "magentaBright", true);
$$.log.style("Testing squeak.core", "magentaBright", true);
$$.log.style("", "magentaBright", true);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  METHODS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var methods = {

  //
  //                              alert.js

  "$$.alert": "",

  //
  //                              async.js

  "$$.asyncEach": function (success, fail) {

    $$.asyncEach(["a", "b", "c"], function (letter, index, asyncIterationIncrement) {

      asyncIterationIncrement();

    }, success);

  },

  "$$.asyncEachPromise": function (success, fail) {

    $$.asyncEachPromise(["a", "b", "c"], function (letter, index, asyncIterationIncrement) {

      asyncIterationIncrement();

    }, 10).then(success).catch(fail);

  },

  //
  //                              function.js

  "$$.results": function (success, fail) {

    var res1 = $$.results({
      a: 1,
      b: function(){ return 2; },
      c: function(arg1){ return arg1; },
    }, 3);

    console.log(res1);

    var res2 = $$.results([
      "a",
      function(){ return "b"; },
      function(arg1){ return arg1; },
    ], "c");

    console.log(res2);

  },

  //
  //                              json.js

  "$$.json": "",

  //
  //                              log.js

  "$$.log": "",
  
  //
  //                              object.js

  "$$.defaults": "",
  "$$.clone": "",


  //
  //                              other.js

  //
  //                              sugar.js

  //
  //                              tests.js

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RUN ALL TESTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

_.each(methods, function(method, methodName){

  var element = eval(methodName);

  if (element) $$.log.style(methodName +" exists", "greenBright")
  else $$.log.style(methodName +" is missing", "redBright");

  if (_.isFunction(method)) {
    $$.log.style("Started testing "+ methodName, "greenBright");
    method(function () {
      $$.log.style("Tested "+ methodName +" successfully", "green");
    }, function () {
      $$.log.style("Error testing "+ methodName, "red");
    });
  };

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
