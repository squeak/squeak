# Browser builds

You can build bundles of squeak for the browser with browserify and uglifyjs.
There are some preset scripts to achieve this in `build/`. But be aware that those scripts having been much tested and may not function.
The recommended way to use squeak is via a nodejs app/framework that will browserify by itself scripts that should ran in the browser.

Default browser builds output in `dist/`.
You can build any custom set of methods with the "build" script.
If you make changes to squeak, you must browserify your changes with one of the build script, or simply run `npm run buildAll`.

## underscore.js in the browser

Underscore.js and other libraries are necessary for squeak to run. For lightness purpose, by default, underscore.js is not bundled in browser builds (but other libraries are).
Get it from https://underscorejs.org/ and include it in your page before requiring squeak.
You can also make you own custom builds with browserify and uglifyjs to include underscorejs (for inspiration on how to do this, see scripts in `build/`).
