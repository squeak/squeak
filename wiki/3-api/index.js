const _ = require("underscore");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// get lib code to pass to api maker (this is useful for automatic listing of methods)
const $$ = require("../../core");

require("../../extension");
require("../../extension/ajax"); // should be done manually because normally ignored server side
// require("../extension/storage"); // should be done manually because normally ignored server side // CANNOT BE DONE SERVER SIDE BECAUSE STORAGES DON'T EXIST THERE

// need to import all plugins manually
require("../../plugin/squeakDate");
require("../../plugin/chatmlString");
require("../../plugin/collection");
require("../../plugin/compare");
require("../../plugin/fileType");
require("../../plugin/log.node");
require("../../plugin/random.position");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  title: "API",
  type: "api",

  moduleName: "squeak",
  libraryName: "$$",
  lib: $$, // this is useful for automatic listing of methods

  documentation: {
    methods: [
      require("./core"),
      require("./extension"),
      require("./plugin"),
    ],
  },

};
