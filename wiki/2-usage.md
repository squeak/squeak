# Usage

Load and use squeak library with:

```javascript
const $$ = require("squeak");
let myRandomNumber = $$.random.number(0, 100); // e.g. 37.3
```

If you want to require automatically not only core features but also all extensions, do this:

```javascript
const $$ = require("squeak/extension");
let myRandomNumber = $$.random.integer(0, 100); // e.g. 17
let contrastColor = $$.color.contrast("#00F"); // "white"
```

To request only a single extension or plugin, you can do it like this:

```javascript
const someSqueakExtension = require("squeak/extension/<extensionName>");
const someSqueakPlugin = require("squeak/plugin/<pluginName>");
```

If you want to use core features and/or extensions and/or plugins, you can do something like this:

```javascript
const $$ = require("squeak");
require("squeak/extension/ajax");
require("squeak/extension/path");
require("squeak/plugin/collection");
require("squeak/plugin/random.position");

$$.log("something");
$$.ajax(...);
$$.path.isHiddenFile(...);
$$.collection(...);
$$.random.position(...);
```

## Some little examples

Squeak is composed of more than a hundred methods, here are illustrated a few randomly chosen ones.

#### Stringify json object with nice layout:
```javascript
const $$ = require("squeak");
$$.json.pretty({
  subObject: {
    a: 1,
    b: 2,
  },
  subArray: [1, 2, 3], // notice that the array is still wrapped like this in the output
  text: "some text",
}, true);
```
> ⇓

```json
{
  "subObject": {
    "a": 1,
    "b": 2
  },
  "subArray": [1, 2, 3],
  "text": "some text"
}
```

#### Get the contrast color of any color:
```javascript
const $$ = require("squeak");
require("squeak/extension/color")
$$.color.contrast("#0F0");
```
> black

#### Make a very condensed string date display:
```javascript
const moment = require("moment");
const $$ = require("squeak");
require("squeak/plugin/squeakDate")
let mySqueakDate = $$.squeakDates([
  {
    start: moment("2019-12-15", "YYYY-MM-DD"),
    end: moment("2019-12-19", "YYYY-MM-DD"),
  },
  moment("2019-12-19", "YYYY-MM-DD"),
  moment("2019-12-21", "YYYY-MM-DD"),
  {
    start: moment("2019-12-28", "YYYY-MM-DD"),
    end: moment("2019-12-30", "YYYY-MM-DD"),
  },
]);
mySqueakDate.getString();
```
> 2019-12-15#19,21,28#30

#### Log a rainbow colored string in the terminal:
```javascript
const $$ = require("squeak/node");
$$.log.node.randomRainbow("A string that will be rainbow colored.");
```
> ![rainbowColoredString](/share/assets/squeak/rainbowColoredString.png)
