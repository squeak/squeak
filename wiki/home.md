
Squeak is a utilities library just like [underscore](https://underscorejs.org/) or [sugar](https://sugarjs.com/) are.
It proposes a set of rather clean and usable functions to make your programming life faster.
