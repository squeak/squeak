var _ = require("underscore");
var $$ = require("../../core");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make page query string from a query object
    ARGUMENTS: ( !queryObject: <QueryObject> )
    RETURN: <QueryString> « e.g. ?key=val&otherKey=otherVal »
  */
  makeQueryStringFromObject: function (queryObject) {

    var queryString = "";

    _.each(queryObject, function (val, key) {

      // add &
      if (queryString) queryString += "&";

      var value = _.isObject(val) ? JSON.stringify(val) : val;
      if (_.isString(value)) value = encodeURIComponent(value);

      // add key and value
      if (value === "") queryString += encodeURIComponent(key)
      else queryString += encodeURIComponent(key) +"="+ value;

    });

    return queryString;

  },

  /**
    DESCRIPTION: get the provided query string as an object
    ARGUMENTS: ( !query <string> )
    RETURN: <object>
  */
  makeQueryObjectFromString: function (query) {
    var match;
    var search = /([^&=]+)=?([^&]*)/g;

    urlParams = {};
    while (match = search.exec(query))
    urlParams[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);

    return _.mapObject(urlParams, function(param){
      return $$.json.parseIfJson(param);
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
