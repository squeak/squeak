var _ = require("underscore");
var $$ = require("../../core");
var utilities = require("./utilities");
var urlifyCorrespondances = require("./urlifyCorrespondances");
if ($$.isNode) var Url = require("url");

$$.url = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DECOMPOSE URL IN IT'S PARTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: decompose a url in it's parts
    ARGUMENTS: ( !url <string> « must be a valid url » )
    RETURN: <{
      href:        <string>  « "https://use:pass@www.example.com:5000/path/to/something/?some=query#hash" »,
      protocol:    <string>  « "https://" »,
      username:    <string>  « "use" »,
      password:    <string>  « "pass" »,
      domain:      <string>  « "www.example.com:5000" »,
      hostname:    <string>  « "www.example.com" »,
      port:        <string>  « "5000" »,
      path:        <string>  « "/path/to/something/" » « will always have final slash »,
      queryString: <string>  « "some=query" »,
      queryObject: <object>  « { some: query } » « if no query, will be: {} »,
      hash:        <string>  « "hash" »,
    }|undefined> « will return undefined if url is not valid »
  */
  decompose: function (url) {

    // split url with system library (node or browser)
    var urlParts;
    if ($$.isNode) {
      urlParts = Url.parse(url);
      // make sure all keys are "" and not null if it's not defined (just like in the browser)
      _.each(urlParts, function (val, key) { if (_.isNull(val)) urlParts[key] = ""; });
    }
    else {
      try { urlParts = new URL (url); }
      catch (e) { return; };
    };

    // process query
    var queryString = urlParts.search.replace(/^\?/, "");
    var queryObject = queryString ? utilities.makeQueryObjectFromString(queryString) : {};

    // process hash
    var hashString = urlParts.hash.replace(/^\#/, "");
    var hashObject = hashString ? utilities.makeQueryObjectFromString(queryString) : {};

    // returned url decomposition object
    return {

      // the full url
      href: urlParts.href,

      // protocol
      protocol: urlParts.protocol + "//",

      // username and password
      username: $$.url.unurlify(urlParts.username, "/"),
      password: $$.url.unurlify(urlParts.password, "/"),

      // domain and port
      domain: urlParts.host,
      hostname: urlParts.hostname,
      port: urlParts.port,

      // path
      path: urlParts.pathname.replace(/\/$/, "") + "/",

      // query and hash
      queryString: queryString,
      queryObject: queryObject,
      hashString: hashString,
      hashObject: hashObject,

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE URL STRING FROM PARTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make a url string from all it's parts
    ARGUMENTS: ({

      |-- !url: <string> « full url, just without query and hash »,
      |-- !protocol,
          ?-- !username,
              !password,
          |-- !domain,
          |-- !hostname,
              !port,
          !path,

      |queryString,
      |queryObject,

      |hashString,
      |hashObject,

      see $$.url.decompose·return for types

    })
    RETURN: <string>
  */
  make: function (urlObject) {

    if (urlObject.url) var url = urlObject.url
    else {
      var url = urlObject.protocol || "";
      if (urlObject.username && urlObject.password) url += $$.url.urlify(urlObject.username, "/") +":"+ $$.url.urlify(urlObject.password, "/") +"@";
      if (urlObject.hostname && url.port) url += urlObject.hostname + urlObject.port
      else url += urlObject.domain || ""
      url += urlObject.path || "";
    };

    // add query
    if (urlObject.queryString) url += "?"+ urlObject.queryString
    else if (_.size(urlObject.queryObject)) url += "?"+ utilities.makeQueryStringFromObject(urlObject.queryObject);
    // add hash
    if (urlObject.hashString) url += "#"+ urlObject.hashString
    else if (_.size(urlObject.hashObject)) url += "#"+ utilities.makeQueryStringFromObject(urlObject.hashObject);

    return url;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  HASH AND QUERY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  hash: require("./hash"),
  query: require("./query"),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BASE 64
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: from base64 data and type, make the corresponding url (for example to display an image from a couchdb attachment)
    ARGUMENTS: (
      !data <string> « base64 data string »,
      !type <string> « the type of the data (check url standards to know how to write it properly) »,
    )
    RETURN: <string> « the resulting url with base64 encoded data »
  */
  makeBase64: function (data, type) {
    return "data:"+ type +";charset=utf-8;base64,"+ data;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  URLIFY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: turn a string into a safe url escaping ["?", "#", "\s"]
    ARGUMENTS: (
      !string <string>,
      ?optionals <string> « pass here a list of optional characters you would like to replace as well (current supported are: "éèàâç/") »,
    )
    RETURN: <string>
    EXAMPLES:
      $$.url.urlify("https://domain.tld/a string with spaces and special?characters"); // "https://domain.tld/a%20string%20with%20spaces%20and%20special%3Fcharacters"
  */
  urlify: function (string, optionals) {
    if (!string) string = "";
    var result = string;
    _.each(urlifyCorrespondances, function (entry) {
      if (!entry.optional || _.indexOf(optionals, entry.outReplacer) !== -1) result = result.replace(entry.inMatch, entry.inReplacer);
    });
    return result;
  },

  /**
    DESCRIPTION: turn a urlified string back into it's normal state
    ARGUMENTS: (
      !string <string>,
      ?optionals <string> « pass here a list of optional characters you would like to replace as well (current supported are: "éèàâç/") »,
    )
    RETURN: <string>
    EXAMPLES:
      $$.url.unurlify("https://domain.tld/a%20string%20with%20spaces%20and%20special%3Fcharacters"); // "https://domain.tld/a string with spaces and special?characters"
  */
  unurlify: function (string, optionals) {
    if (!string) string = "";
    _.each(urlifyCorrespondances, function (entry) {
      if (!entry.optional || _.indexOf(optionals, entry.outReplacer) !== -1) string = string.replace(entry.outMatch, entry.outReplacer);
    });
    return string;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DECOMPOSE URL PATH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: return first parent of this url
    ARGUMENTS: ( <string> )
    RETURN: <string>
    EXAMPLES:
      $$.url.parent("https://domain.tld/parent/child/"); "https://domain.tld/parent/"
  */
  parent: function (url) {
    return url.replace(/\/$/, "").replace(/[^\/]*$/, "");
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$.url;
