var _ = require("underscore");
var $$ = require("../../core");
var utilities = require("./utilities");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get this url hash string
    ARGUMENTS: ( ?hashObject <object> « if nothing passed here, will get hash from page's url » )
    RETURN: <QueryString> « e.g. ?key=val&otherKey=otherVal »
  */
  getAsString: function (hashObject) {
    if (hashObject) return utilities.makeQueryStringFromObject(hashObject)
    else return $$.replace(location.hash, /^\#/, "") || "";
  },

  /**
    DESCRIPTION: get this url hash (or the provided hash) as an object
    ARGUMENTS: ( ?hashString « if nothing passed here or it's not a string, will get hash from page's url » )
    RETURN: <object>
  */
  getAsObject: function (hashString) {
    if (!_.isString(hashString)) hashString = $$.url.hash.getAsString();
    return utilities.makeQueryObjectFromString(hashString);
  },

  /**
    DESCRIPTION: set page hash string in url
    ARGUMENTS: ( !hash: <string|object>, )
    RETURN: <QueryString>
  */
  set: function (hash) {

    // if query is an object, convert it to a query string
    if (_.isObject(hash)) hash = utilities.makeQueryStringFromObject(hash);

    // set query string in url
    return window.location.hash = hash || "";

  },

  /**
    DESCRIPTION: modify page hash string in url from a hash object (only specified keys are modified)
    ARGUMENTS: ( !object <object> )
    RETURN: <QueryString>
  */
  modify: function (newHashObjectPortion) {
    var newHashObject = Object.assign($$.url.hash.getAsObject(), newHashObjectPortion);
    return $$.url.hash.set(newHashObject);
  },

  /**
    DESCRIPTION: add the given key (without value) to hash
    ARGUMENTS: ( !key <string> )
    RETURN: <QueryString>
    EXAMPLES:
      $$.url.hash.addKey("someKey");
  */
  addKey: function (key) {
    var objToAddKey = {};
    objToAddKey[key] = "";
    return $$.url.hash.modify(objToAddKey);
  },

  /**
    DESCRIPTION: remove a key in url hash
    ARGUMENTS: ( !key: <string> )
    RETURN: <QueryString>
  */
  removeKey: function (key) {
    var currentHash = $$.url.hash.getAsObject();
    delete currentHash[key];
    return $$.url.hash.set(currentHash);
  },

  /**
    DESCRIPTION: get a key in url hash
    ARGUMENTS: ( !deepKey: <string> )
    RETURN: <any>
  */
  getKey: function (deepKey) {
    var currentHash = $$.url.hash.getAsObject();
    return $$.getValue(currentHash, deepKey);
  },

  /**
    DESCRIPTION: remove a key in url hash
    ARGUMENTS: ( !deepKey: <string> )
    RETURN: <boolean>
  */
  hasKey: function (deepKey) {
    var currentHash = $$.url.hash.getAsObject();
    return $$.hasValue(currentHash, deepKey)
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
