var _ = require("underscore");
var $$ = require("../../core");
var utilities = require("./utilities");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SET QUERY FROM STRING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function setQueryFromString (queryString, reload) {

  // do it only if query has changed, and pushState method exists
  if (queryString != $$.url.query.getAsString() && history.pushState) {

    var newurl = window.location.protocol +"//"+ window.location.host + window.location.pathname +"?"+ queryString;
    window.history.pushState({ path: newurl }, "", newurl);

    // reload page if asked
    if (reload) document.location.reload();

  };

  return queryString;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get this url query string
    ARGUMENTS: ( ?queryObject <object> « if nothing passed here, will get query from page's url » )
    RETURN: <QueryString> « e.g. ?key=val&otherKey=otherVal »
  */
  getAsString: function (queryObject) {
    if (queryObject) return utilities.makeQueryStringFromObject(queryObject)
    else return $$.replace(location.search, /^\?/, "") || "";
  },

  /**
    DESCRIPTION: get this url query (or the provided query) as an object
    ARGUMENTS: ( ?queryString « if nothing passed here or it's not a string, will get query from page's url » )
    RETURN: <object>
  */
  getAsObject: function (queryString) {
    if (!_.isString(queryString)) queryString  = window.location.search.substring(1);
    return utilities.makeQueryObjectFromString(queryString);
  },

  /**
    DESCRIPTION: set page query string in url
    ARGUMENTS: (
      !query: <string|object>,
      ?reload <boolean>@default=false « pass true to reload page on query change »,
    )
    RETURN: <QueryString>
  */
  set: function (query, reload) {

    // if query is an object, convert it to a query string
    if (_.isObject(query)) query = utilities.makeQueryStringFromObject(query);

    // set query string in url
    return setQueryFromString(query, reload);

  },

  /**
    DESCRIPTION: modify page query string in url from a query object (only specified keys are modified)
    ARGUMENTS: (
      !object: <object>,
      ?reload <boolean>@default=false « pass true to reload page on query change »,
    )
    RETURN: <QueryString>
  */
  modify: function (newQueryObjectPortion, reload) {
    var newQueryObject = Object.assign($$.url.query.getAsObject(), newQueryObjectPortion);
    return $$.url.query.set(newQueryObject, reload);
  },

  /**
    DESCRIPTION: add the given key (without value) to query
    ARGUMENTS: (
      !key <string>,
      ?reload <boolean>@default=false « pass true to reload page on query change »,
    )
    RETURN: <QueryString>
    EXAMPLES:
      $$.url.query.addKey("someKey");
  */
  addKey: function (key, reload) {
    var objToAddKey = {};
    objToAddKey[key] = "";
    return $$.url.query.modify(objToAddKey, reload);
  },

  /**
    DESCRIPTION: remove a key in url query
    ARGUMENTS: (
      !key: <string>,
      ?reload <boolean>@default=false « pass true to reload page on query change »,
    )
    RETURN: <QueryString>
  */
  removeKey: function (key, reload) {
    var currentQuery = $$.url.query.getAsObject();
    delete currentQuery[key];
    return $$.url.query.set(currentQuery, reload);
  },

  /**
    DESCRIPTION: get a key in url query
    ARGUMENTS: ( !deepKey: <string> )
    RETURN: <any>
  */
  getKey: function (deepKey) {
    var currentQuery = $$.url.query.getAsObject();
    return $$.getValue(currentQuery, deepKey);
  },

  /**
    DESCRIPTION: remove a key in url query
    ARGUMENTS: ( !deepKey: <string> )
    RETURN: <boolean>
  */
  hasKey: function (deepKey) {
    var currentQuery = $$.url.query.getAsObject();
    return $$.hasValue(currentQuery, deepKey)
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
