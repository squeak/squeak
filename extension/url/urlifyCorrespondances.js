module.exports = [

  //
  //                              defaults

  {
    inMatch: /\s/g,
    inReplacer: "%20",
    outMatch: /\%20/g,
    outReplacer: " ",
  },
  {
    inMatch: /\!/g,
    inReplacer: "%21",
    outMatch: /\%21/g,
    outReplacer: "!",
  },
  {
    inMatch: /\"/g,
    inReplacer: "%22",
    outMatch: /\%22/g,
    outReplacer: "\"",
  },
  {
    inMatch: /\#/g,
    inReplacer: "%23",
    outMatch: /\%23/g,
    outReplacer: "#",
  },
  {
    inMatch: /\$/g,
    inReplacer: "%24",
    outMatch: /\%24/g,
    outReplacer: "$",
  },
  {
    inMatch: /\&/g,
    inReplacer: /\%26/g,
    outMatch: "%26",
    outReplacer: "&",
  },
  {
    inMatch: /\</g,
    inReplacer: "%3C",
    outMatch: /\%3C/g,
    outReplacer: "<",
  },
  {
    inMatch: /\>/g,
    inReplacer: "%3E",
    outMatch: /\%3E/g,
    outReplacer: ">",
  },
  {
    inMatch: /\?/g,
    inReplacer: "%3F",
    outMatch: /\%3F/g,
    outReplacer: "?",
  },

  // /\=/: "%3D",
  // /\[/: "%5B",
  // /\\/: "%5C",
  // /\]/: "%5D",
  // /\^/: "%5E",
  // /\{/: "%7B",
  // /\|/: "%7C",
  // /\}/: "%7D",
  // /\~/: "%7E",
  // /\“/: "%22",
  // /\‘/: "%27",
  // /\+/: "%2B",
  // /\,/: "%2C",
  // /\%/: "%25",
  // /\@/: "%40",
  // /\`/: "%60",
  // /\:/: "%3A",
  // /\;/: "%3B",

  //
  //                              slashes

  {
    inMatch: /\//g,
    inReplacer: "%2F",
    outMatch: /\%2F/g,
    outReplacer: "/",
    optional: true,
  },

  //
  //                              letters with accents

  {
    inMatch: /\é/g,
    inReplacer: "%C3%A9",
    outMatch: /\%C3\%A9/g,
    outReplacer: "é",
    optional: true,
  },
  {
    inMatch: /\è/g,
    inReplacer: "%C3%A8",
    outMatch: /\%C3\%A8/g,
    outReplacer: "è",
    optional: true,
  },
  {
    inMatch: /\ç/g,
    inReplacer: "%C3%A7",
    outMatch: /\%C3\%A7/g,
    outReplacer: "ç",
    optional: true,
  },
  {
    inMatch: /\â/g,
    inReplacer: "%C3%A2",
    outMatch: /\%C3\%A2/g,
    outReplacer: "â",
    optional: true,
  },
  {
    inMatch: /\à/g,
    inReplacer: "%C3%A0",
    outMatch: /\%C3\%A0/g,
    outReplacer: "à",
    optional: true,
  },

  //                              ¬
  //
  
];
