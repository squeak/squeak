var _ = require("underscore");
var $$ = require("../core");

$$.string = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: convert the value to the prettiest possible string
    ARGUMENTS: (
      ?string <any>,
      ?options <{
        ?htmlifyObjects: <boolean>@default=false « pass true to htmlify prettified objects »,
        ?keepUndefinedNullNaN: <boolean>@default=false « pass true to not transform null, undefined and NaN into an empty string »,
      }>,
    )
    RETURN: <string>
    EXAMPLES:
      $$.string.make("A string.");      // "A string."
      $$.string.make(9);                // "9"
      $$.string.make();                 // ""
      $$.string.make(null);             // ""
      $$.string.make([1, 2, 3]);        // "1, 2, 3"
      $$.string.make([1, null, 2, 3]);  // "1, 2, 3"
      $$.string.make({ a: 1 });
        // '{
        //  "a": "1"
        // }'
  */
  make: function (string, options) {
    if (!options) options = {};

    // undefined, null, NaN
    if (!options.keepUndefinedNullNaN && (_.isUndefined(string) || _.isNull(string) || _.isNaN(string))) return ""

    // arrays
    else if (_.isArray(string)) {
      // remove all undefined, null and NaN value, because they would be replaced by an empty string anyway
      // the following line is the same as "$$.array.compact(string, [undefined, null, NaN])", the method is not used because $$.string shouldn't depend on $$.array
      if (!options.keepUndefinedNullNaN) string = _.reject(string, function (entry) { var entriesToRemove = [undefined, null, NaN]; for (var key in entriesToRemove) { if (_.isEqual(entry, entriesToRemove[key])) return true; } });
      // pass each array entry through $$.string.make
      return _.map(string, function (childValue) {
        return $$.string.make(childValue, options);
      }).join(", ");
    }

    // objects
    else if (_.isObject(string)) {
      var result = $$.json.pretty(string);
      if (options.htmlifyObjects) result = $$.string.htmlify(result);
      return result;
    }

    // others
    else return string +"";

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADAPT STRING TO SOME CONTEXTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: escape an html string (means replace <, >, ", ' /, =, &, ` by html equivalents)
    ARGUMENTS: ( string <string> )
    RETURN: <string>
    EXAMPLES:
      $$.string.escapeHtml('<div class="hello"><br/></div>'); // "&lt;div class&#x3D;&quot;hello&quot;&gt;&lt;br&#x2F;&gt;&lt;&#x2F;div&gt;"
  */
  escapeHtml: function (string) {
    var entityMap = { '&': '&amp;', '<': '&lt;', '>': '&gt;', '"': '&quot;', "'": '&#39;', '/': '&#x2F;', '`': '&#x60;', '=': '&#x3D;' };
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
      return entityMap[s];
    });
  },

  /**
    DESCRIPTION:
      make a string a safe id string for dom elements, escaping all forbidden characters (list comes from: https://api.jquery.com/category/selectors/)
      this replaces all occurences of special unaccepted characters by "_"
    ARGUMENTS: ( string <string> )
    RETURN: <string>
    EXAMPLES:
      $$.string.safeId("$yo"); // "_yo"
  */
  safeId: function (string) {
    return string.replace(/[\!\"\#\$\%\&\'\(\)\*\+\,\.\/\:\;\<\=\>\?\@\[\\\]\^\`\{\|\}\~]/g, "_");
  },

  /**
    DESCRIPTION: test if a string is safe to use as an id for dom elements
    ARGUMENTS: ( string <string> )
    RETURN: <boolean>
  */
  isSafeId: function (string) {
    return string.match(/[\!\"\#\$\%\&\'\(\)\*\+\,\.\/\:\;\<\=\>\?\@\[\\\]\^\`\{\|\}\~]/) ? false : true;
  },

  /**
    DESCRIPTION: make a string a valid UTF8 string
    ARGUMENTS: ( string <string> )
    RETURN: <string>
  */
  validUtf8: function (string) {
    return string.replace(/[^\x20-\x7E]+/g, '');
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LATINIZATION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: remove all accented and special non latin characters from a string
    ARGUMENTS: ( string <string> )
    RETURN: <string>
    EXAMPLES:
      $$.string.latinize("Hello yay élo îo ìo."); // "Hello yay elo io io."
  */
  latinize: function(string){
    if (!_.isString(string)) return $$.log.returnLog(string, "The element you provided to latinize is not a string");
    return string.replace(/[^A-Za-z0-9\[\] ]/g, function (a) { return $$.string.latinList[a] || a });
  },

  /**
    DESCRIPTION: list of non latin characters and their corresponding latin character (used in $$.string.latinize)
    TYPE: <{ [key]: <string>, }>
  */
  latinList: require("./string.latinize.js"),

  /**
    DESCRIPTION: test if string is latin
    ARGUMENTS: ( !string <string> )
    RETURN: <boolean>
    EXAMPLES:
      $$.string.isLatin("Hello yay élo îo ìo."); // false
      $$.string.isLatin("Hello yay elo io io."); // true
  */
  isLatin: function (string) { return string == $$.string.latinize(string); },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MATCHING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: find matching on any kind of value
    ARGUMENTS: (
      !entry <string|object|array|number>,
      !value <string|regexp>,
      ?caseSensitive <boolean> «if true and value is a string will create a case sensitive regexp »
    )
    RETURN: <boolean>
    EXAMPLES:
      $$.string.matchAnything("Text string to find matches.", /\sfind\s/);                 // [ " find " ]
      $$.string.matchAnything(["Text string to find matches."], /\sfind\s/);               // [ " find " ]
      $$.string.matchAnything({ someKey: "Text string to find matches.", }, /\sfind\s/);   // [ " find " ]
      $$.string.matchAnything({ someKey: "Text string to find matches.", }, /Key/);        // [ "Key" ]
      $$.string.matchAnything({ someKey: "Text string to find matches.", }, "key");        // [ "Key" ]
      $$.string.matchAnything({ someKey: "Text string to find matches.", }, "key", true);  // null
      $$.string.matchAnything(["Text string to find matches, find."], "/find/g");          // [ "find", "find" ]
  */
  matchAnything: function (entry, value, caseSensitive) {

    // simple equal check (easy shortcut not to deal with undefined, null...)
    if (_.isEqual(entry, value)) return true

    // entry and value are defined
    else if (value && entry) {

      // create regexp
      var regExpVal = $$.regexp.make(value, caseSensitive ? "g" : "gi");

      // match
      if (_.isString(entry)) return entry.match(regExpVal) || $$.string.latinize(entry).match(regExpVal)
      else if (_.isFunction(entry)) {
        console.info("Function matching is not supported by $$.string.matchAnything.");
        return false;
      }
      else if (_.isObject(entry)) {
        var stringifiedEntry = $$.json.short(entry);
        return $$.string.latinize(stringifiedEntry).match(regExpVal);
      }
      else if (_.isNumber(entry)) return (entry +"").match(regExpVal)
      else console.error("What is this value you want to match? It doesn't seem supported.\nSupported types are: string, object, array, number.", entry, value, regExpVal);

    }

    // entry or value is missing
    else return false;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REPLACEMENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: replace multiple elements in a string
    ARGUMENTS: (
      !string <string>,
      !options <
        |<matcherAndReplacer>[]
        |{ [key]: <matcherAndReplacer> }
      > «
        every key listed here will be applied to string for replacements
        if the value is not an object, will just ignore it from replacing
      »,
    )
    RETURN: <string>
    TYPES:
      matcherAndReplacer = <{
        !regexp: <regexp|regexp[]> « elements to match »,
        !replacement: <string> « string with which to replace matches »,
      }>
    }
  */
  replaceMulti: function (string, options) {

    // make sure given value is a string
    string = $$.string.make(string);

    // replace all matches
    _.each(options, function (replaceObject, key) {
      if (_.isObject(replaceObject)) {
        var regexps = _.isArray(replaceObject.regexp) ? replaceObject.regexp : [replaceObject.regexp];
        _.each(regexps, function (regexp) {
          string = string.replace(regexp, replaceObject.replacement);
        });
      };
    });

    return string;

  },

  /**
    DESCRIPTION: convert a text string to html
    ARGUMENTS: (
      !string <string>,
      ?options <see $$.string.replaceMulti·options}>@default={
        newLine: {
          _recursiveOption: true,
          regexp: /\n/g,
          replacement: "<br>",
        },
        tab: {
          _recursiveOption: true,
          regexp: [/\t/g, /\\t/g],
          replacement: "&nbsp&nbsp&nbsp&nbsp",
        },
        spaces: {
          _recursiveOption: true,
          regexp: /\s\s+/g,
          replacement: function (d, position) {
            var spaces = "";
            for (i = 0; i < d.length; i++) {
              spaces += "&nbsp";
            };
            return spaces;
          },
        },
      },
    )
    RETURN: <string>
    EXAMPLES:
      $$.string.htmlify("\tText\nto convert."); // "&nbsp&nbsp&nbsp&nbspText<br>to convert."
  */
  htmlify: function (string, options) {

    var defaultOptions = {
      newLine: {
        _recursiveOption: true,
        regexp: /\n/g,
        replacement: "<br>",
      },
      tab: {
        _recursiveOption: true,
        regexp: [/\t/g, /\\t/g],
        replacement: "&nbsp&nbsp&nbsp&nbsp",
      },
      spaces: {
        _recursiveOption: true,
        regexp: /\s\s+/g,
        replacement: function (d, position) {
          var spaces = "";
          for (i = 0; i < d.length; i++) {
            spaces += "&nbsp";
          };
          return spaces;
        },
      },
    };

    return $$.string.replaceMulti(string, $$.defaults(defaultOptions, options))

  },

  /**
    DESCRIPTION: the opposite of $$.string.htmlify to convert back an htmlified string to text
    ARGUMENTS: (
      !string <string>,
      ?options <see $$.string.replaceMulti·options}>@default={
        newLine: {
          _recursiveOption: true,
          regexp: /<br>/g,
          replacement: "\n",
        },
        tab: {
          _recursiveOption: true,
          regexp: /&nbsp;&nbsp;&nbsp;&nbsp;/g,
          replacement: "\t",
        },
        spaces: {
          _recursiveOption: true,
          regexp: /&nbsp;/g,
          replacement: " ",
        },
      },
    )
    RETURN: <string>
    EXAMPLES:
      $$.string.textify("&nbsp&nbsp&nbsp&nbspText<br>to convert."); // "\tText\nto convert."
  */
  textify: function (string, options) {

    var defaultOptions = {
      newLine: {
        _recursiveOption: true,
        regexp: /<br>/g,
        replacement: "\n",
      },
      tab: {
        _recursiveOption: true,
        regexp: /&nbsp&nbsp&nbsp&nbsp/g,
        replacement: "\t",
      },
      spaces: {
        _recursiveOption: true,
        regexp: /&nbsp/g,
        replacement: " ",
      },
    };

    return $$.string.replaceMulti(string, $$.defaults(defaultOptions, options))

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SMALL CONVENIENCE UTILIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: just keep the beginning of a string and add "..." at the end
    ARGUMENTS: (
      !string <string>,
      ?length <number>@default=20 « result string length »,
    )
    RETRUN: <string>
    EXAMPLES:
      $$.string.trim("A string way to looooooooooooooong to display nice in a small space.");     // "A string way to looo..."
      $$.string.trim("A string way to looooooooooooooong to display nice in a small space.", 40); // "A string way to looooooooooooooong to di..."
  */
  trim: function (string, length) {

    // safety net
    string = $$.string.make(string);

    // default length
    if (!length) length = 20;

    // is not longer
    if (string.length <= length) return string
    // is longer
    else return _.first(string, length).join("") +"...";

  },

  /**
    DESCRIPTION: remove at every line of string the number of spaces there is at beginning of the first line
    ARGUMENTS: ( <string> )
    RETURN: <string>
    EXAMPLES:
      $$.string.outdentAuto("\
              DESCRIPTION: some description\n\
              ARGUMENTS: (\n\
                first: <string>,\n\
                second: <boolean>,\n\
              )\n\
              RETURN: <function>\n\
      ");
  */
  outdentAuto: function (string) {
    var tabSize = $$.match(string, /^[\t ]*/).length;
    return string.replace(new RegExp ("^[\\t ]{0,"+ tabSize +"}", "gm"), "");
  },

  /**
    DESCRIPTION: convert a uuid to a unique number
    ARGUMENTS: ( string <uuidString> )
    RETURN: <integer>
    EXAMPLES:
      $$.string.uuidToNumber("5897959d-a499"); // 5897959131610499
  */
  uuidToNumber: function(string){

    var result = "";
    var array = string.split("");

    _.each(array, function(letter,i){
      // get index in alphabet
      result += $$.string.alphabet.uuid.indexOf(letter) +"";
    });

    return +result;

  },

  /**
    DESCRIPTION: alphabet utilities, mainly for internal usage
    KEYS: {
      min: <["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]>,
      maj: <["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]>,
      double: <$$.string.alphabet.min and $$.string.alphabet.max>,
      uuid: <["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d","e","f", "-"]>,
    }
  */
  alphabet: {
    min: ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    maj: ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    double: function () { return _.flatten([$$.string.alphabet.maj, $$.string.alphabet.min]); },
    uuid: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d","e","f", "-"],
  },

  // composition: function (string) {
  //   return {
  //     isMaj: string == string.toUpperCase(),
  //     isMin: string == string.toLowerCase(),
  //     hasMaj: string != string.toLowerCase(),
  //     hasMin: string != string.toUpperCase(),
  //     // hasBoth: string != string.toUpperCase() && string != string.toLowerCase(),
  //   };
  // },

  /**
    DESCRIPTION: make a full key out of a key and an optional rootkey
    ARGUMENTS: (
      !key <string>,
      ?rootKey <string>,
    )
    RETURN: <string>
    EXAMPLES:
      $$.string.makeFullKey("mykey", "my.parentKey"); // "my.parentKey.mykey"
      $$.string.makeFullKey("mykey"); // "mykey"
  */
  makeFullKey: function (key, rootKey) {
    if (!_.isString(key)) key += "";
    return rootKey ? rootKey +"."+ key : key;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INTERPOLATE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      interpolate the given string (= replaces some parts of it by some variables)
      by default the syntax used is mustache-like:
      {{ some_key }} will be replaced by the value saved at some_key in the entry
      {{ . }} is a special syntax to refer to the whole entry
    ARGUMENTS: ({
      !string <string> « the string to interpolate »,
      !entry <object> « an object containing variables to fill in the string »,
      ?fallback <object> « an object containing fallback values, in case some are not defined in "entry" »,
      ?missing: <string>@default=undefined « if a value cannot be found neither in "entry" nor in "fallback", this string will be used, if this is not defined, no interpolation will take place, and the result string will contain the patterns of the string passed »,
      ?style <"%%"|"mustache"|"underscore"|regexp>@default="mustache" «
        the predefined pattern of interpolation to use
        you may also pass here a regexp pattern to use for interpolation
      »,
      ?complexExpressions: <boolean|regexp> «
        if this is enabled, you will be able to pass more complex expresions to be interpolated,
        this allow you to have portions of the string that are predefined but only shown if a value is defined
        see the last examples below for details (if you need to have a quote in your static text, use "", this will be replaced by a single one)
        you may also pass here a regexp pattern to use for complex expressions
      »,
    })
    RETURN: <string>
    EXAMPLES:
      $$.string.interpolate({
        string: "Hello {{ variable }}!",
        entry: { variable: "world" },
      });
      // "Hello world!"
      $$.string.interpolate({
        string: "Hello %%variable%%!",
        entry: { variable: "world" },
        style: "%%",
      });
      // "Hello world!"
      $$.string.interpolate({
        string: "Hello <%= yo %>!",
        entry: { yo: "world" },
        style: "underscore",
      });
      // "Hello world!"
      $$.string.interpolate({
        string: "This is not an entry, {{ . }}!",
        entry: "just a string",
      });
      // "This is not an entry, just a string!"
      $$.string.interpolate({
        string: "Hello {{ va }}!",
        entry: {},
        fallback: { va: "unknwon person"},
      });
      // "Hello unknwon person!"
      $$.string.interpolate({
        string: '{{ city }}{{ " (" country ")" }}',
        entry: { city: "Buenos Aires", country: "Argentina", },
        complexExpressions: true,
      });
      // "Buenos Aires (Argentina)"
      $$.string.interpolate({
        string: '{{ city }}{{ " (" country ")" }}',
        entry: { city: "Buenos Aires", },
        complexExpressions: true,
        missing: "",
      });
      // "Buenos Aires"
      $$.string.interpolate({
        string: '{{ city }}{{ " """ country """" }}',
        entry: { city: "Buenos Aires", country: "Argentina", },
        complexExpressions: true,
      });
      // 'Buenos Aires "Argentina"'
  */
  interpolate: function (options) {

    // default options
    var defaultOptions = {
      style: "mustache",
      fallback: {},
    };
    options = $$.defaults(defaultOptions, options);
    if (!_.isString(options.string)) options.string = "";
    if (!options.variables) options.variables = {};


    // what style to use
    if (options.style == "%%") var matcher = /\%\%\s*(.+?)\s*\%\%/g
    else if (options.style == "underscore") var matcher = /<%=\s*(.+?)\s*%>/g
    else if (_.isRegExp(options.style)) var matcher = options.style
    else var matcher = /\{\{\s*(.+?)\s*\}\}/g;

    // complex expressions matcher
    if (_.isRegExp(options.complexExpressions)) var complexExpressions = options.complexExpressions
    else if (options.complexExpressions) var complexExpressions = /\s*"([^"]|"")*"\s*/g // \s*((?!\\)\")(.*?)([^\\]\")\s*
    else var complexExpressions = false;

    // replace values in string
    return options.string.replace(matcher, function (match, escape) {

      // more advanced expressions
      var key = complexExpressions ? escape.replace(complexExpressions, "") : escape;

      // get value in entry, support . as a special key to parse the whole entry
      if (key === ".") var valueFound = options.entry;
      else var valueFound = $$.getValue(options.entry, key);
      // get value in fallback entry
      if (_.isUndefined(valueFound) || (_.isArray(valueFound) && !valueFound.length)) valueFound = $$.getValue(options.fallback, key);

      // no value, return "missing" string or just the original match
      if (_.isUndefined(valueFound)) return !_.isUndefined(options.missing) ? options.missing : match
      // if complex expression is enabled, remove quotes and replace double quotes by simple quotes
      else if (complexExpressions) return interpretComplexSubstring(escape, complexExpressions, key, $$.string.make(valueFound, { keepUndefinedNullNaN: true }))
      // return the value found
      else return $$.string.make(valueFound, { keepUndefinedNullNaN: true });

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// this function is highly experimental and very very very "workaround"
function interpretComplexSubstring (substring, complexExpressionsMatcher, key, valueFound) {

  var resultMaker = [];

  // make an array with indexes and string already replaced
  resultMaker.push(0);
  substring.replace(complexExpressionsMatcher, function (match, esc, matchIndex) {
    resultMaker.push(matchIndex);
    // remove starting and ending quote and spaces, replace "" by "
    resultMaker.push(match.replace(/^\s*"/, "").replace(/"\s*$/, "").replace(/""/g, "\""));
    resultMaker.push(matchIndex + match.length);
  });
  resultMaker.push(substring.length);

  var resultString = "";
  // if two indexes are consecutive, there should be a string between them
  _.each(resultMaker, function (val, valIndex) {
    if (_.isNumber(val) && _.isNumber(resultMaker[valIndex+1]) && val !== resultMaker[valIndex+1]) {
      var keyFound = substring.substring(val, resultMaker[valIndex+1]).replace(/^\s*/, "").replace(/\s*$/, "");
      if (keyFound == key) resultString += valueFound;
    }
    else if (_.isString(val)) resultString += val;
  });

  return resultString;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = $$.string;
