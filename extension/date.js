var _ = require("underscore");
var $$ = require("../core");
var moment = require("moment");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CUSTOMIZE SLIGHTLY MOMENT LOCALE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

moment.updateLocale("en", { week: {
  dow: 1, // first day of week is Monday
  doy: 4  // first week of year must contain 4 January (7 + 1 - 4) // ISO-8601, Europe
}});

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

$$.date = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: shortcut to `moment` library
    see https://momentjs.com/
  */
  moment: moment,

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DATE QUICK MAKER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: list of date formats accepted by the $$.date.make method
    TYPE: <string[]>
  */
  make_acceptedDateFormats: [
    // -
    "YYYY-MM-DD",
    "YYYY-MM-DD_HH:mm",
    "YYYY-MM-DD_HH:mm:ss",
    "YYYY-MM-DD_HH[h]mm[m]",
    "YYYY-MM-DD_HH[h]mm[m]ss[s]",
    // /
    "YYYY/MM/DD",
    "YYYY/MM/DD_HH:mm",
    "YYYY/MM/DD_HH:mm:ss",
    "YYYY/MM/DD_HH[h]mm[m]",
    "YYYY/MM/DD_HH[h]mm[m]ss[s]",
    // only year-month year-quarter, year
    "YYYY-MM",
    "YYYY/MM",
    "YYYY·Q",
    "YYYY",
    // only time
    "HH[h]mm[m]ss[s]",
    "HH[h]mm[m]",
    "HH:mm:ss",
    "HH:mm",
    // other
    "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]",
  ],

  /**
    DESCRIPTION: a date maker that accepts date in the format YYYY-MM-DD, YYYY/MM/DD where date parts can also be "?" ("secure" option)
    ARGUMENTS: (
      ?date <dateString|Date|momentObject|undefined> «
        - if undefined, will set now's date
        - string should be in the one of the formats listed below (you can customize the list of accepted formats modifying the $$.date.make_acceptedDateFormats array)
      »,
      ?secure <boolean> « if true, allow date to contain "?" characters e.g. "????-12-09" »
    )
    RETURN: <momentObject> « if any failure, returns now's date »
    TYPES:
      dateString = a string that should follow one of the following patterns:
        "YYYY-MM-DD", "YYYY-MM-DD_HH:mm", "YYYY-MM-DD_HH:mm:ss", "YYYY-MM-DD_HH[h]mm[m]", "YYYY-MM-DD_HH[h]mm[m]ss[s]",
        "YYYY/MM/DD", "YYYY/MM/DD_HH:mm", "YYYY/MM/DD_HH:mm:ss", "YYYY/MM/DD_HH[h]mm[m]", "YYYY/MM/DD_HH[h]mm[m]ss[s]",
        "YYYY-MM", "YYYY/MM", "YYYY·Q", "YYYY",
        "HH[h]mm[m]ss[s]", "HH[h]mm[m]", "HH:mm:ss", "HH:mm",
        "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]",
    EXAMPLES:
      $$.date.make("2000-07-09_18h18m18s"); // <momentObject with the given date>
      $$.date.make("18h18m18s");            // <momentObject with the given date>
      $$.date.make("2040·3");               // <momentObject with the given date>
  */
  make: function (date, secure) {

    //
    //                              DATE STRING TESTER

    function tryDatesFormatsAndGetDate (listOfFormats) {
      return _.some(listOfFormats, function (format) {
        momentDate = moment(date, format);
        if (date === momentDate.format(format)) return true
      });
    };

    //
    //                              GET DATE FROM STRING OR OBJECT

    var momentDate;
    if (_.isUndefined(date)) momentDate = moment()
    else if (_.isString(date)) {

      // autofill missing year, month or day // replace all "?" by "1", the only way to not get any error date
      if (secure) date = date.replace(/\?/g, _.isString(secure) ? secure : "1");

      // get date
      var someFormatHasMatched = tryDatesFormatsAndGetDate($$.date.make_acceptedDateFormats);

      // if date string wasn't managed by moment unset it so it raises error after
      if (!someFormatHasMatched) {
        if (!momentDate._isValid) momentDate = null
        else $$.log.info("$$.date.make: moment managed to parse date, but it's not in one of the accepted formats:", $$.date.make_acceptedDateFormats);
      };

    }
    else if (_.isDate(date)) momentDate = moment(date)
    else if (_.isObject(date) && date._isAMomentObject) momentDate = date

    //
    //                              RETURN

    // ERROR
    if (!momentDate) {
      $$.log.detailedError(
        "squeak.date.make",
        "Date format is not recognized",
        date
      );
      return moment();
    }

    // RETURN DATE
    else return momentDate;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DEFAULT DATES FORMATS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the given date in format "YYYY"
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « in the format "YYYY" »
    EXAMPLES:
      $$.date.year("2017-01-01_09:09:09"); // "2017"
  */
  year: function (date, secure) {
    return $$.date.make(date, secure).format("YYYY");
  },

  /**
    DESCRIPTION: get the given date in format "YYYY-MM"
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « in the format "YYYY-MM" »
    EXAMPLES:
      $$.date.month("2017-01-01_09:09:09"); // "2017-01"
  */
  month: function (date, secure) {
    return $$.date.make(date, secure).format("YYYY-MM");
  },

  /**
    DESCRIPTION: get the given date in format "YYYY-MM-DD"
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « in the format "YYYY-MM-DD" »
    EXAMPLES:
      $$.date.day("2017-01-01_09:09:09"); // "2017-01-01"
  */
  day: function (date, secure) {
    return $$.date.make(date, secure).format("YYYY-MM-DD");
  },

  /**
    DESCRIPTION: get the given date in format "YYYY-MM-DD_HH[h]mm[m]ss[s]"
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « in the format "YYYY-MM-DD_HH[h]mm[m]ss[s]" »
    EXAMPLES:
      $$.date.time("2017-01-01_09:09:09"); // "2017-01-01_09h09m09s"
  */
  time: function (date, secure) {
    return $$.date.make(date, secure).format("YYYY-MM-DD_HH[h]mm[m]ss[s]");
  },

  /**
    DESCRIPTION: get the given date in format "YYYY-MM-DD_HH[h]mm[m]ss[s]Z"
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « in the format "YYYY-MM-DD_HH[h]mm[m]ss[s]Z" »
    EXAMPLES:
      $$.date.offset("2017-01-01_09:09:09"); // "2017-01-01_09h09m09s+01:00"
  */
  offset: function (date, secure) {
    return $$.date.make(date, secure).format("YYYY-MM-DD_HH[h]mm[m]ss[s]Z");
  },

  /**
    DESCRIPTION: get the given date's quarter
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « date's quarter number »
    EXAMPLES:
      $$.date.quarter("2017-01-01_09:09:09"); // "1"
      $$.date.quarter("2017-12-01_09:09:09"); // "4"
  */
  quarter: function (date, secure) {
    return $$.date.make(date, secure).format("Q");
  },

  /**
    DESCRIPTION: get the given date's week
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « date's week number »
    EXAMPLES:
      $$.date.week("2017-01-01_09:09:09"); // "01"
      $$.date.week("2017-12-01_09:09:09"); // "48"
  */
  week: function (date, secure) {
    return $$.date.make(date, secure).format("ww");
  },

  /**
    DESCRIPTION: get the given date in default format ("YYYY-MM-DD[T]HH:mm:ssZ")
    ARGUMENTS: ( see $$.date.make )
    RETURN: <dateString> « in the format "YYYY-MM-DD[T]HH:mm:ssZ" »
    EXAMPLES:
      $$.date.print("2017-01-01_09:09:09"); // "2017-01-01T09:09:09+07:00"
  */
  print: function (date, secure) {
    return $$.date.make(date, secure).format();
  },


  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE DATES LIST
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: returns the list of dates between the two given dates
    ARGUMENTS: (
      !startDateString <dateStringHandledByBetween|momentObject>,
      !endDateString <dateStringHandledByBetween|momentObject>,
      ?step <"year"|"month"|"day"|"quarter">@default="day",
    )
    RETURN: <dateStringHandledByBetween[]> « date string type depend on the step you chose »
    TYPES:
      dateStringHandledByBetween = a string that should follow one of the following patterns:
        "YYYY", "YYYY-MM", "YYYY-MM-DD", "YYYY-Q"
    EXAMPLES:
      $$.date.getDatesBetween("2000-01", "2000-12", "month"); // [ "2000-01", "2000-02", "2000-03", "2000-04", "2000-05", "2000-06", "2000-07", "2000-08", "2000-09", "2000-10", "2000-11", "2000-12" ]
  */
  getDatesBetween: function (startDateString, endDateString, step) {

    //
    //                              DATE FORMATS BY STEP

    var formats = {
      year: "YYYY",
      month: "YYYY-MM",
      day: "YYYY-MM-DD",
      quarter: "YYYY·Q",
    };
    var format = formats[step] || formats[day];

    //
    //                              GET START AND END DATE MOMENT OBJECTS

    var startDate = moment(startDateString, format);
    var endDate = moment(endDateString, format);


    // verify that start date is before end date
    if (endDate.isBefore(startDate)) return $$.log.detailedError("$$.date.getDatesBetween", "End date must be greated than start date.");

    //
    //                              GENERATE RETURNED ARRAY AND, LOOP INCREMENTALY TO ADD DATES IN BEWTEEN

    var result = [];
    while (startDate.isSameOrBefore(endDate)) {
      result.push(startDate.format(format));
      startDate.add(1, step);
    };
    return result;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  IS IN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: test if a date is in one other like 2019-12-11 in 2019-12 or 2019-12 in 2019·4
    ARGUMENTS: (
      !date <dateString>,
      !periodDate <dateString>,
    )
    RETURN: <boolean>
    EXAMPLES:
      $$.date.isIn("2019-12-11", "2019-12"); // true
      $$.date.isIn("2019-11-11", "2019-12"); // false
      $$.date.isIn("2019-12-11", "2019·4");  // true
  */
  isIn: function (date, periodDate) {

    // make moment date
    var dateMoment = $$.date.make(date);
    var periodDateMoment = $$.date.make(periodDate);

    return dateMoment.format(periodDateMoment._f) == periodDateMoment.format(periodDateMoment._f);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  NUMBER TO TIME
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: from a number of seconds, make a readable time
    ARGUMENTS: (
      !number <number>,
      ?joinSplit <string> « what kind of space between time parts ("", " ", "<br>")? »,
    )
    RETURN: <string>
    EXAMPLES:
      $$.date.numberToTime(4789);         // "1h19m49s"
      $$.date.numberToTime(4789, "<br>"); // "1h<br>19m<br>49s"
  */
  numberToTime: function (number, joinSplit) {

    var hours = Math.floor(number/3600);
    var hoursRemainder = number % 3600;

    var minutes = Math.floor(hoursRemainder/60);
    var seconds = hoursRemainder % 60;

    return _.compact([
      ( hours   ? hours   +"h" : undefined ),
      ( minutes ? minutes +"m" : undefined ),
      ( seconds ? seconds +"s" : undefined )
    ]).join(joinSplit || "") || "0s";

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$.date;
