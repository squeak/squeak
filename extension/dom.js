var _ = require("underscore");
var $$ = require("../core");
var $ = require("jquery");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var draggable_zIndexCategories = {};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

$$.dom = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FOCUS AND BLUR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: focus asked element
    ARGUMENTS: (
      !target <jqueryProcessable|function():<jqueryProcessable>> « the element to focus »,
      ?options {
        ?timeout <number:default=500> « timeout to set focus in milliseconds »
        ?findInput <boolean|string>@default=true «
          |boolean: if set to true and the given target is not an input or text area, find children textarea(s) or input(s) to focus
          |string: pass the selector of the type of child(ren) you want to find
        »
        ?after: <function> « execute after focus has been made »
      }
    )
    RETURN: <void>
    EXAMPLES:
      $$.dom.focus( $("<input>").appendTo($("body")) ); // this will focus the just created input
  */
  focus: function (target, options) {

    var defaultOptions = {
      timeout: 500,
      findInput: true,
    };
    options = $$.defaults(defaultOptions, options);

    setTimeout(function() {

      // get target
      var $target = $($$.result(target));

      // find child input
      if (options.findInput && !$$.dom.isInputer($target)) $target = $target.find(options.findInput === true ? "input, textarea" : options.findInput);

      // focus
      $target.focus();

      // callback
      if (options.after) options.after();

    }, options.timeout);

  },

  /**
    DESCRIPTION: unfocus the current active element
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  blur: function () {
    $($$.dom.getFocus()).blur();
  },

  /**
    DESCRIPTION: get the current active element
    ARGUMENTS: ( ø )
    RETURN: <jqueryObject>
  */
  getFocus: function () { return $(document.activeElement); },

  /**
    DESCRIPTION: check if the active element is an input
    ARGUMENTS: ( ø )
    RETURN: <boolean>
  */
  isFocusInput: function () {
    return $$.dom.isInputer($$.dom.getFocus());
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INPUT OR TEXTAREA
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: check if a dom element is an input or textarea
    ARGUMENTS: ( target <domElement|jqueryDomElement> )
    RETURN: <boolean>
  */
  isInputer: function (target) {

    var targetTag = target.prop("tagName");
    var isTextEditable = target.attr("contentEditable");

    return !(  (targetTag != "INPUT" && targetTag != "TEXTAREA") && isTextEditable != "true"  );

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SELECTION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: clear selection in page
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  clearSelection: function () {
    if (document.selection && document.selection.empty) document.selection.empty()
    else if(window.getSelection) window.getSelection().removeAllRanges();
  },

  // function getSelectedText () { // from general
  //   var text = "";
  //   if (window.getSelection) {
  //     text = window.getSelection().toString();
  //   } else if (document.selection && document.selection.type != "Control") {
  //     text = document.selection.createRange().text;
  //   };
  //   return text;
  // },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SCROLL TO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: scroll to the given elemtn
    ARGUMENTS: (
      !target <jqueryProcessable>,
      ?parent <jqueryProcessable> « containing target, if undefined, will use body, but it will only work if the scroll bar is on body, so it's better to specify a value here »,
      ?options <{
        ?margin: <integer>@default=30 « how many pixels to leave on top of element »,
        ?animate: <integer|null>@default=100 « how many milliseconds will the scroll take »,
      }>,
    )
    RETURN: <void>
    EXAMPLES:
      $$.dom.scrollTo("#my-div", "#parent");
  */
  scrollTo: function (target, parent, options) {

    //
    //                              DEFAULTS
    var defaultOptions = {
      margin: 30,
      animate: 100,
    };
    options = $$.defaults(defaultOptions, options);

    //
    //                              MAKE SURE TARGET AND PARENT EXIST

    // return if target doesn't exist
    target = $(target);
    if (!target[0]) return $$.log.detailedError(
      "$$.dom.scrollTo",
      "no target found to scroll",
      target
    );

    // if no parent, take all as parent
    parent = $(parent);
    if (!parent[0]) parent = $("body");

    //
    //                              SCROLL

    // scroll amount
    var scrollAmount = target[0].offsetTop - parent[0].offsetTop - options.margin;
    // check if object is visible
    var targetAbsoluteOffset = target.position().top + target.height();
    var isVisible = targetAbsoluteOffset <= window.innerHeight && targetAbsoluteOffset >= 0;
    // if is not visible, scroll
    if (!isVisible) {
      if (options.animate) parent.animate({ scrollTop: scrollAmount, }, options.animate)
      else parent[0].scrollTop(scrollAmount)
    };

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  WINDOW, LOAD/UNLOAD EVENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


  /**
    DESCRIPTION:
      let you define a function that will be executed when page has loaded
      similar to defining window.onload function but you can run $$.dom.onWindowLoad as many time as you want and all the functions you pass will be executed in order
    ARGUMENTS: ( !func <function> )
    RETURN: <void>
    EXAMPLES:
      $$.dom.onWindowLoad(function(){ console.log(1); });
      $$.dom.onWindowLoad(function(){ console.log(2); });
      $$.dom.onWindowLoad(function(){ console.log(3); });
      // it will log 1, 2, 3 when page is loaded
  */
  onWindowLoad: function (func) {
    window.onload = $$.queue(window.onload, func);
  },

  /**
    DESCRIPTION:
      let you define a function that will be executed when page has been resized
      similar to defining window.onresize function but you can run $$.dom.onWindowResize as many time as you want and all the functions you pass will be executed in order
    ARGUMENTS: ( !func <function> )
    RETURN: <void>
    EXAMPLES:
      $$.dom.onWindowResize(function(){ console.log(1); });
      $$.dom.onWindowResize(function(){ console.log(2); });
      // now if page is resized, it will log 1, 2
      $$.dom.onWindowResize(function(){ console.log(3); });
      // now if page is resized, it will log 1, 2, 3
  */
  onWindowResize: function (func) {
    window.onresize = $$.queue(window.onresize, func);
  },

  /**
    DESCRIPTION:
      let you define a function that will be executed when page will be left
      similar to defining window.onbeforeunload function but you can run $$.dom.onBeforeunload as many time as you want and all the functions you pass will be executed in order
    ARGUMENTS: ( !func <function> )
    RETURN: <void>
    EXAMPLES:
      $$.dom.onBeforeunload(function(){ console.log(1); });
      // now if page is left, it will log 1
      $$.dom.onBeforeunload(function(){ console.log(2); });
      $$.dom.onBeforeunload(function(){ console.log(3); });
      // now if page is left, it will log 1, 2, 3 before leaving
  */
  onBeforeunload: function (func) {
    window.onbeforeunload = $$.queue(window.onbeforeunload, func);
  },

  /**
    DESCRIPTION: execute a callback when previous-page/next-page button have been hit
    ARGUMENTS: ( callback <function(ø)> )
    RETURN: <void>
  */
  onPopstate: function (callback) {
    $(window).on("popstate", function () {
      callback();
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADD TAB SUPPORT TO TEXTAREA OR INPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: prevent switching input when tab key is entered and input a tab character (\t) instead
    ARGUMENTS: ( !target <jqueryProcessable> )
    RETURN: <void>
  */
  addTabSupport: function (target) {

    function insertTab ($el) {
      var pos = $$.dom.getCaretPosition($el);
      var textContent = $el.val();
      var preText = textContent.substring(0, pos);
      var postText = textContent.substring(pos, textContent.length);

      $el.val(preText + "\t" + postText);

      $$.dom.setCaretPosition($el, pos + 1);
    };

    $(target).on("keydown", function (e) {
      if (e.which == 9) {
        insertTab($(this))
        e.preventDefault();
      };
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET/SET CARET POSITION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: set caret position in an input or textarea
    ARGUMENTS: (
      !$el: <jqueryObject>,
      !pos: <number>,
    )
    RETURN: <void>
  */
  setCaretPosition: function ($el, pos) {
    $el[0].focus();
    $el[0].setSelectionRange(pos, pos);
  },

  /**
    DESCRIPTION: get caret position in an input or textarea
    ARGUMENTS: ( !$el: <jqueryObject> )
    RETURN: <void>
  */
  getCaretPosition: function ($el) {
    var caretPosition = 0;
    caretPosition = $el[0].selectionStart;
    return caretPosition;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE ELEMENT DRAGGABLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make an element draggable
    ARGUMENTS: ({

      // dom element and optional handle
      !$target <jqueryObject> « element(s) to make draggable »,
      ?$handle: <jqueryObject> « if specified, the element will only be draggable by this handle »,

      // conditions (if any of the following condition option is set and doesn't match, drag won't take effect)
      ?keyToPress: <"ctrlKey"|"shiftKey"|"altKey"> « keys to be pushed for dragging to take effect »,
      ?keyNotToPress: <"ctrlKey"|"shiftKey"|"altKey"> « keys that shouldn't be pushed for dragging to take effect »,
      ?condition: <function(<event>)<boolean>> « if defined, the result of running this function will decide if dragging should take effect or not »,

      // z-index
      ?zAlso: <string> « if this is set, will always put in the front the last dragged target of this category »,
      ?zInitialMax: <integer> « if using zAlso, set here the z value of the highest element in list »,

      // drag event actions
      ?onDrag: <function(
        <{ x:<number>, y:<number>, z:<number>, }>,
        event
      )> « callback executed while dragging is ongoing »,
      ?onDragStart: <function(event)> « callback executed when dragging starts »,
      ?onDragEnd: <function(event)> « callback executed when dragging stops »,
      ?onDrop: <function(
        $dropTarget <jqueryObject> « the element on which it was dropped »,
        $element <jqueryObject> « the element that was dropped »,
      )> « executed when the element has been dropped on to another one »,

      // other options
      ?cloneDrag: <boolean>@default=false « if true, will drag a clone of the element, that is removed on mouse relase »,
      ?$dropGroup: <jqueryObject> «
        this is only relevant if onDrop is set
        a list of entries on which the draggable element can be dropped on
        if you specified multiple targets, they will automatically be used as dropGroup
      »,

    })
    RETURN: <void>
  */
  makeDraggable: function (options) {

    // verify that there is a target
    if (!options.$target || !options.$target.length) return $$.log.detailedError(
      "squeak.dom.makeDraggable",
      "You didn't specify any target element to make draggable."
    )
    // if multiple targets
    else if (options.$target.length > 1) options.$target.each(function () {
      var eachTargetOptions = $$.defaults({ $dropGroup: options.$target, }, options, { $target: $(this), });
      $$.dom.makeDraggable(eachTargetOptions);
    });

    // global variables
    var initMouseX, initMouseY, finalMouseX, finalMouseY, $targetElement, $dropTarget;

    // identify target: handle or element itself
    var dragTarget = options.$handle ? options.$handle[0] : options.$target[0];
    // attach drag event to target (queue it to other one it could have)
    dragTarget.onmousedown = $$.queue(dragTarget.onmousedown, dragInit);

    // initialize: attach mousemove and mouseup events to target
    function dragInit (ev) {

      // make sure to get the event
      var event = ev || window.event;

      // stop if dragging condition hasn't been met
      if (options.keyToPress && !event[options.keyToPress]) return;
      if (options.keyNotToPress && event[options.keyNotToPress]) return;
      if (_.isFunction(options.condition) && !options.condition(event)) return;

      event.preventDefault();

      // save initial mouse cursor position
      initMouseY = event.clientY;
      initMouseX = event.clientX;

      // identify target element that should be dragged (if droppable, create a new one that will be deleted when drag stops)
      if (options.cloneDrag) $targetElement = options.$target.clone().css({
        position: "absolute",
        top: options.$target[0].offsetTop,
        left: options.$target[0].offsetLeft,
        pointerEvents: "none",
      }).appendTo(options.$target.parent());
      else $targetElement = options.$target;

      // detect hovering of other entries to drop on
      if (options.onDrop && options.$dropGroup) options.$dropGroup.hover(function () {
        $dropTarget = $(this);
      }, function () {
        $dropTarget = undefined;
      });

      // functions to execute when mouse move and is up
      document.onmouseup = stopDrag;
      document.onmousemove = drag;

      // onDragStart method
      if (options.onDragStart) options.onDragStart(ev);

    };

    // remove drag events attached
    function stopDrag (ev) {
      document.onmouseup = null;
      document.onmousemove = null;
      if (options.cloneDrag) $targetElement.remove();
      if (options.onDrop && $dropTarget) options.onDrop($dropTarget, $targetElement);
      // onDragEnd method
      if (options.onDragEnd) options.onDragEnd(ev);
    };

    // drag element on page on mouse move
    function drag (ev) {

      var event = ev || window.event;
      event.preventDefault();

      // calculate displacement
      finalMouseY = initMouseY - event.clientY;
      finalMouseX = initMouseX - event.clientX;
      // save new initial mouse cursor position for next execution
      initMouseY = event.clientY;
      initMouseX = event.clientX;

      // handle element's margin (if there are some, if there are none, this is not necessary, but it doesn't harm either)
      var marginTop = +$targetElement.css("margin-top").replace(/px$/, "");
      var marginBottom = +$targetElement.css("margin-bottom").replace(/px$/, "");
      var marginLeft = +$targetElement.css("margin-left").replace(/px$/, "");
      var marginRight = +$targetElement.css("margin-right").replace(/px$/, "");
      var verticalMargins = (marginTop+marginBottom)/2;
      var horizontalMargins = (marginLeft+marginRight)/2;

      // set element new position
      var x = $targetElement[0].offsetLeft - finalMouseX - horizontalMargins;
      var y = $targetElement[0].offsetTop - finalMouseY - verticalMargins;
      $targetElement[0].style.top = y +"px";
      $targetElement[0].style.left = x +"px";
      $targetElement[0].style.bottom = "unset"; // make sure there is no bottom value defined
      $targetElement[0].style.right = "unset"; // make sure there is no right value defined

      // if asked, also sets z-index
      if (options.zAlso) {
        // options.zAlsoINITIAL VALUE
        var z = draggable_zIndexCategories[options.zAlso] || (options.zInitialMax || 7);
        if (z != +$targetElement.css("z-index")) z = z +1;
        draggable_zIndexCategories[options.zAlso] = z;
        $targetElement.css("z-index", z);
      };

      // execute drag callback
      if (options.onDrag) options.onDrag({ x: x, y: y, z: z, }, event);

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$.dom;
