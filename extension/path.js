var _ = require("underscore");
var $$ = require("../core");

$$.path = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: returns path's extension if there is one (extension cannot be longer than ten characters or contain spaces)
    ARGUMENTS: ( path <string> )
    RETURN: <string>
    EXAMPLES:
      $$.path.getExtension("/path/to/file.ext"); // "ext"
      $$.path.getExtension("image.jpg");         // "jpg"
  */
  getExtension: function (path) {
    return path.match(/\.[^\.\s\/]{1,10}$/) ? path.match(/[^\.]*$/)[0] || "" : "";
  },

  /**
    DESCRIPTION: decompose the given path and return an object (see examples below)
    ARGUMENTS: (
      !path <string>,
      ?recurse <integer> « if positive, will execute $$.path.decompose on parentPath as many times as this value »,
    )
    RETURN: <pathObject>
    TYPES:
      pathObject = {
        !isDirectory: <boolean>, // just test if path has a trailing slash
        !parentPath: <string>,
        !path: <string>,
        !nameAndExtension: <string>,
        !name: <string>,
        !extension: <string>, // (see $$.path.getExtension)
        ?parent: <pathObject>, // defined only if recurse option is true
      }
    EXAMPLES:
      $$.path.decompose("/path/to/image.jpg");
      // > {
      // >   isDirectory: false,
      // >   parentPath: "/path/to/",
      // >   path: "/path/to/image.jpg",
      // >   nameAndExtension: "image.jpg",
      // >   name: "image",
      // >   extension: "jpg",
      // > }
      $$.path.decompose("image.jpg");
      // > {
      // >   isDirectory: false,
      // >   parentPath: "",
      // >   path: "image.jpg",
      // >   nameAndExtension: "image.jpg",
      // >   name: "image",
      // >   extension: "jpg",
      // > }
      $$.path.decompose("/path/to/directory/");
      // > {
      // >   isDirectory: true,
      // >   parentPath: "/path/to/",
      // >   path: "/path/to/directory/",
      // >   nameAndExtension: "directory",
      // >   name: "directory",
      // >   extension: "",
      // > }
      $$.path.decompose("/path/to/directory/", 2);
      // > {
      // >   isDirectory: true,
      // >   parentPath: "/path/to/",
      // >   path: "/path/to/directory/",
      // >   nameAndExtension: "directory",
      // >   name: "directory",
      // >   extension: "",
      // >   parent: {
      // >     isDirectory: true,
      // >     parentPath: "/path/",
      // >     path: "/path/to/",
      // >     nameAndExtension: "to",
      // >     name: "to",
      // >     extension: "",
      // >     parent: {
      // >       isDirectory: true,
      // >       parentPath: "/",
      // >       path: "/path/",
      // >       nameAndExtension: "path",
      // >       name: "path",
      // >       extension: "",
      // >     },
      // >   },
      // > }
  */
  decompose: function (passedPath, recurse) {

    // make sure path is a string
    if (_.isUndefined(passedPath)) passedPath = ""
    else if (!_.isString(passedPath)) passedPath = passedPath +"";

    // make sure there is no trailing slash at the end (even if it is a directory)
    var fullPath = passedPath.replace(/\/$/, "");

    // get parts
    var parentPath = fullPath.replace(/[^\/]*$/, "");
    var nameAndExtension = fullPath.match(/[^\/]*$/)[0] || "";
    var extension = $$.path.getExtension(nameAndExtension);

    var result = {
      isDirectory: passedPath.match(/\/$/) ? true : false,
      parentPath: parentPath,
      path: passedPath,
      nameAndExtension: nameAndExtension,
      name: nameAndExtension.replace(new RegExp ("\\."+ extension +"$"), ""),
      extension: extension,
    };

    // recurse parent if asked
    if (recurse && parentPath) result.parent = $$.path.decompose(parentPath, recurse -1);

    // return object
    return result;

  },

  /**
    DESCRIPTION: get a safe path for file, to use in terminal, path will be surrounded by quotes and all quotes in file name are escaped
    ARGUMENTS: ( path <string> )
    RETURN: <string>
    EXAMPLES:
      $$.path.safeForTerminal('/path with spaces and "quotes"/to/some image.jpg');
      // "\"/path with spaces and \\\"quotes\\\"/to/some image.jpg\""
  */
  safeForTerminal: function (path) {
    return '"'+ path.replace(/"/g, "\\\"") +'"';
  },

  /**
    DESCRIPTION: check if the path of a file descibes a hidden file (if it's name starts with a dot)
    ARGUMENTS: ( !fullPath <!string> )
    RETURN: <boolean>
  */
  isHiddenFile: function (fullPath) {
    return fullPath.match(/\/\.[^\/]*$/) ? true : false;
  },

  /**
    DESCRIPTION: get all parents of an entry (see examples below)
    ARGUMENTS: (
      !fullPath <string>,
      ?includeRoot <boolean>@default=false « pass true to include "/" path in list of parents paths »,
    )
    RETURN: <strings[]> « array of all parents paths »
    EXAMPLES:
      $$.path.getAllParentsPaths("/my/entry/isHere/ENTRY");  // ["/my/", "/my/entry/", "/my/entry/isHere/"]
      $$.path.getAllParentsPaths("/my/entry/isHere/");       // ["/my/", "/my/entry/", "/my/entry/isHere/"]
      $$.path.getAllParentsPaths("/my/entry/isHere/", true); // ["/", "/my/", "/my/entry/", "/my/entry/isHere/"]
  */
  getAllParentsPaths: function (fullPath, includeRoot) {

    var allParentsPaths = [];
    fullPath = fullPath.replace(/[^\/]*$/, "") // make sure there is no name in the path
    var allPathPieces = _.compact(fullPath.split("/"))

    for (i=0; i<allPathPieces.length; i++) { // i=1 to skip name
      allParentsPaths.unshift("/"+ _.initial(allPathPieces, i).join("/") +"/");
    }

    if (includeRoot) allParentsPaths.unshift("/");

    return allParentsPaths

  },

  /**
    DESCRIPTION:
      same as $$.path.getAllParentsPaths, but for an array of paths
      will return only one instance of each parent, even if many children in same parent
    ARGUMENTS: (
      !fullPaths <string[]>,
      ?includeRoot <boolean>@default=false « pass true to include "/" path in list of parents paths »,
    )
    RETURN: <strings[]> « array of all parents paths »
    EXAMPLES:
      $$.path.batchGetAllParentsPaths(["/my/entry/isHere/ENTRY", "/my/other/entry/isThere.ext", "/my/entry/isHere/otherEntry"]);
      // [ "/my/", "/my/entry/", "/my/entry/isHere/", "/my/other/", "/my/other/entry/" ]
  */
  batchGetAllParentsPaths: function (arrayOfPaths, includeRoot) {
    return _.chain(arrayOfPaths)
      .map(function (path) {
        return $$.path.getAllParentsPaths(path, includeRoot);
      })
      .flatten()
      .uniq()
      .value()
    ;
  },

  /**
    DESCRIPTION: returns only the part of the path from a root folder
    ARGUMENTS: (
      !path <string>,
      !rootPath <string>,
    )
    RETURN: <string>
    EXAMPLES:
      $$.path.getRelativePath("/path/to/my/file.ext", "/path/to/"); // "my/file.ext"
  */
  getRelativePath: function (path, rootPath) {
    var rootPathRegExp = new RegExp ("^"+ rootPath)
    if (_.isString(path)) return path.replace(rootPathRegExp, "")
    else $$.log.detailedError(
      "$$.path.getRelativePath",
      "path is not a string",
      path
    );
  },

  /**
    DESCRIPTION: same as getRelativePath but for an array of paths
    ARGUMENTS: (
      !path <string[]>,
      !rootPath <string>,
    )
    RETURN: <string[]>
    EXAMPLES:
      $$.path.batchGetRelativePaths(["/path/to/my/file.ext", "/path/to/my/otherFile.ext"], "/path/to/");
      // [ "my/file.ext", "my/otherFile.ext" ]
  */
  batchGetRelativePaths: function (pathsArray, rootPath) {
    return _.map(pathsArray, function (path) {
      return $$.path.getRelativePath(path, rootPath);
    })
  },

  /**
    DESCRIPTION: replace the beginning of a path string with another parent folder value
    ARGUMENTS: (
      !path <string>,
      !originParent <string>,
      !targetParent <string>,
    )
    RETURN: <string>
    EXAMPLES:
      $$.path.changeParent("/first/path/to/my/files/", "/first/", "/second/other/"); // "/second/other/path/to/my/files/"
  */
  changeParent: function (path, originPath, targetPath) {
    return $$.replace(path, "^"+ $$.regexp.escape(originPath), targetPath);
  },

  /**
    DESCRIPTION: replace windows non compatible characters in a path by the given character
    ARGUMENTS: (
      !path <string>,
      ?replacer <string>@default="_",
    )
    RETURN: <string>
    EXAMPLES:
      $$.path.makeSafeFilePath('/path with spaces and "quotes"/to/some image.jpg');
      // "/path with spaces and _quotes_/to/some image.jpg"
      $$.path.makeSafeFilePath('/path with spaces and "quotes"/to/some image.jpg', "Δ");
      // "/path with spaces and ΔquotesΔ/to/some image.jpg"
  */
  makeSafeFilePath: function (path, replacer) {
    if (!replacer) replacer = "_";
    if (!_.isString(path)) return path
    else return path.replace(/[\?\<\>\:\"\|\*]/g, replacer); //"|\\?*<\":>+[]/'"
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$.path;
