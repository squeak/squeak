var $ = require("jquery");
var _ = require("underscore");
var $$ = require("../../core");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: performs an ajax request
  ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
  RETURN: <ajaxRequest>
*/
$$.ajax = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // o            >            +            #            °            ·            ^            :            |
  //                                           ASYNC AJAX

  /**
    alias of $$.ajax
  */
  main: function (options) {
    //----------------------------------------------- DEFAULT OPTIONS
    var defaultOptions = {
      error: function(response){ console.log(options); console.log(response); },
    };
    options = $$.defaults(defaultOptions, options);
    //----------------------------------------------- DEFAULT OPTIONS END
    // // correct url with custom characters ///////////////////////////////////////////////////// problem when file name contains #
    // if (options.url && _.isString(options.url)) options.url.replace(/\#/g, "%23");
    return $.ajax(options);
  },

  /**
    DESCRIPTION: performs a GET ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  get: function (options) {
    options.type = "GET";
    return $$.ajax(options);
  },

  /**
    DESCRIPTION: performs a POST ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  post: function (options) {
    options.type = "POST";
    return $$.ajax(options);
  },

  /**
    DESCRIPTION: performs a PUT ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  put: function (options) {
    options.type = "PUT";
    return $$.ajax(options);
  },

  /**
    DESCRIPTION: performs a GET ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  delete: function (options) {
    options.type = "DELETE";
    return $$.ajax(options);
  },


  //
  // o            >            +            #            °            ·            ^            :            |

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
