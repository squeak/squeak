var $ = require("jquery");
var _ = require("underscore");
var $$ = require("./async");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DETECT SENT AJAX REQUESTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: will run the callback every time an ajax request has been sent
  ARGUMENTS: ( callback <function(event, request, settings)> « see jquery ajaxSend » )
  RETURN: <jquery ajaxSend return>
*/
$$.ajax.sendListener = function (callback) {
  return $(document).ajaxSend(callback);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FILE UPLOADING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: upload the given files to the chosen url
  ARGUMENTS: ({
    !files <file|file[]>
    !url <string> « url to upload to »,
    ?formData: <{
      [ formKey <string> ]: <any|function(<any>)>
    }>,
    !success: <function> « ajax success callback »,
    ?error: <function> « ajax error callback »,
  })
  RETURN: <ajaxRequest>
  TODO:
    - ability to easily associate with a progress bar
*/
$$.ajax.uploadFiles = function (options) {

  var files = $$.isFile(options.files) ? [options.files] : options.files;
  var formData = new FormData();

  // append files to form
  _.each(files, function(file,i){ formData.append("file", file, file.name); });

  // optional additional form datas
  _.each(options.formData, function (d,i) {
    var data = $$.result(d, files);
    if (_.isObject(data)) data = JSON.stringify(data);
    formData.append(i, data);
    // formData.append(i, _.result(options.formData, i));
  });

  return $.ajax({
    url: options.url,
    type: "POST",
    contentType: false,
    processData: false,
    data: formData,
    // TODO: progress bar
    // xhr: function(){ return toolbar.progressbar(files); },
    // // xhr: toolbar.progressbar,
    success: options.success,
    error: options.error || function(response){ $$.log.error(response); },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  HIDDEN INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a hidden file input to upload files on request (useful if you want a custom button to upload files)
  ARGUMENTS: (
    ?$container: <jqueryProcessable>@default="body",
    ?acceptedFileTypes: <string[]|undefined> « set the list of accepted file types »,
    !onSelect: <$$.ajax.uploadFiles·options|function(file)> «
      will be executed on each selected file
        | if onSelect is a function, will execute the function
        | if onSelect is an object, will run $$.ajax.uploadFiles(onSelect)
    »,
  )
  RETURN: <jqueryObject>
  EXAMPLES:
    $$.ajax.createHiddenInput({
      onSelect: "/upload/",
      success: function () { alert("done"); },
    });
    $$.ajax.createHiddenInput({
      acceptedFileTypes: [ "" ],
      onSelect: function (file) {
        console.log(file);
      },
    });
*/
$$.ajax.createHiddenInput = function (options) {

  //
  //                              GET ALL OPTIONS

  var defaultOptions = {
    $container: "body",
    name: "file",
    multiple: "",
    acceptedFileTypes: undefined,
  };
  options = $$.defaults(defaultOptions, options);

  var $container = $(options.$container);

  //
  //                              MAKE INPUT'S OPTIONS

  var inputOptions = {
    type: "file",
    name: options.name,
  };
  // allow multiple files unless options.multiple is set to false or null
  if (options.multiple !== false || options.multiple !== null) inputOptions.multiple = "";
  if (options.acceptedFileTypes) inputOptions.accept = options.acceptedFileTypes.join(", ");

  //
  //                              CREATE INPUT

  var $hiddenFileInput = $("<input>", inputOptions).appendTo($container).css({
    display: "none",
  }).change(function(){

    // execute callback for each chosen file
    var files = $(this).prop("files");
    _.each(files, function (file) {

      // check that file is right type if necessary
      if (!options.acceptedFileTypes || _.indexOf(options.acceptedFileTypes, file.type) != -1) {

        // EXECUTE CALLBACK
        if (_.isFunction(options.onSelect)) options.onSelect(file)

        // EXECUTE AJAX UPLOAD
        else {
          var ajaxOptions = _.clone(options.onSelect);
          ajaxOptions.files = [file];
          $$.ajax.uploadFiles(ajaxOptions);
        };

      }
      else alert("invalid file type");

    });

  });

  return $hiddenFileInput;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
