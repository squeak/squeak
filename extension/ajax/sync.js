var $ = require("jquery");
var _ = require("underscore");
var $$ = require("./async");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: performs a synchronous ajax request
  ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
  RETURN: <ajaxRequest>
*/
$$.ajax.sync = $$.scopeFunction({

  /**
    see $$.ajax.sync
  */
  main: function(options){

    var defaultOptions = {
      async: false,
    };
    options = $$.defaults(defaultOptions, options);

    return $.ajax(options);

  },

  /**
    DESCRIPTION: performs a synchronous GET ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  get: function (options) {
    options.type = "GET";
    return $$.ajax.sync(options);
  },

  /**
    DESCRIPTION: performs a synchronous POST ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  post: function (options) {
    options.type = "POST";
    return $$.ajax.sync(options);
  },

  /**
    DESCRIPTION: performs a synchronous PUT ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  put: function (options) {
    options.type = "PUT";
    return $$.ajax.sync(options);
  },

  /**
    DESCRIPTION: performs a synchronous DELETE ajax request
    ARGUMENTS: ( !options « see jquery.ajax (https://api.jquery.com/jQuery.ajax/) » )
    RETURN: <ajaxRequest>
  */
  delete: function (options) {
    options.type = "DELETE";
    return $$.ajax.sync(options);
  },

}),

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
