var _ = require("underscore");
var $$ = require("../core");

$$.object = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: flatten an object (see the example below)
    ARGUMENTS: (
      !obj <object> « object to flatten »,
      ¡result <object> « the partially filled return object, FOR INTERNAL USAGE DON'T SET THIS »,
      ¡parentKeyAndPoint <string> « the key of the current recure place, FOR INTERNAL USAGE DON'T SET THIS »,
    )
    RETURN: <object>
    EXAMPLES:
      $$.object.flatten({
        a: 1,
        b: {
          A: 9,
          B: 4
        }
      });
      // > {
      // >   "a": 1,
      // >   "b.A": 9,
      // >   "b.B": 4
      // > }
  */
  flatten: function (obj, result, parentKeyAndPoint) {
    if (!parentKeyAndPoint) parentKeyAndPoint = "";
    if (!result) result = {};
    _.each(obj, function(val,key){
      // if value is an object, recurse in flatten
      if ($$.isObjectLiteral(val)) $$.object.flatten(val, result, parentKeyAndPoint + key +".")
      // else add key-value pair to result
      else result[parentKeyAndPoint + key] = val;
    });
    return result;
  },

  /**
    DESCRIPTION: generate a key in object that doesn't exist yet
    ARGUMENTS: (
      !obj <object>,
      !key <string> «base key»,
      ?suffix <integer> « don't need to be specified, it'll be created automatically, starting with 1, but you can specify a number to start with if you want »
    )
    RETURN: <string>
    EXAMPLES:
      let yo = { myTakenKey: "hello", };
      let getUntakenKey = $$.object.findUnspecifiedKey(yo, "myTakenKey"); // "myTakenKey-1"
      yo[getUntakenKey] = "hi";
      $$.object.findUnspecifiedKey(yo, "myTakenKey"); // "myTakenKey-2"
  */
  findUnspecifiedKey: function (obj, key, suffix) {
    // determine suffix
    if (!suffix) suffix = 1
    else suffix++;
    // determine key
    if (_.has(obj, key +"-"+ suffix)) return $$.object.findUnspecifiedKey(obj, key, suffix)
    else return key +"-"+ suffix;
  },

  /**
    DESCRIPTION:
      remove list of given keys in an object
      doesn't modify the passed object, creates a new object
    ARGUMENTS: (
      !object <object>,
      !keysToRemove <string[]> « list of keys that will be removed in object »,
      ?recursive <number> « pass a number if you want to do recurse this ammounts of time and remove keys recursively »,
    )
    RETURN: <object>
    EXAMPLES:
      $$.object.removeKeys({ a: 1, b: 2, c: 3, d: 4, }, ["a", "c"]); // { b: 2, d: 4 }
      let deepObject = {
        a: 1, b: 2, c: 3,
        d: {
          a: 1, b: 2, c: 3,
          d: {
            a: 1, b: 2, c: 3,
            d: 4,
          },
        },
      };
      $$.object.removeKeys(deepObject, ["a", "c"]);    // { b: 2, d: { a: 1, b: 2, c: 3, d: { a: 1, b: 2, c: 3, d: 4 } } }
      $$.object.removeKeys(deepObject, ["a", "c"], 1); // { b: 2, d: {       b: 2,       d: { a: 1, b: 2, c: 3, d: 4 } } }
      $$.object.removeKeys(deepObject, ["a", "c"], 9); // { b: 2, d: {       b: 2,       d: {       b: 2,       d: 4 } } }
  */
  removeKeys: function (object, keysToRemove, recursive) {
    var result = _.clone(object);
    _.each(result, function (entry, key) {
      // remove requested keys
      _.each(keysToRemove, function (keyToRemove) { delete result[keyToRemove]; });
      // recurse if asked
      if (recursive && $$.isObjectOrArray(entry)) result[key] = $$.object.removeKeys(entry, keysToRemove, recursive -1);
    });
    return result;
  },

  /**
    DESCRIPTION:
      remove list of given values in an object
      doesn't modify the passed object, creates a new object
      uses _.isEqual to compare values
    ARGUMENTS: (
      !object <object>,
      !valuesToRemove <any[]> «
        list of values that will be removed in object
        if function, it will be executed to test equality with it's result
      »,
      ?recursive <number> « pass a number if you want to do recurse this ammounts of time and remove keys recursively »,

      object,
      valuesToRemove <any[]> « if value is a function, will be run »,
      recursive « pass true to do it recursively », // TODO: improve, pass a number to decide how much recursion
    )
    RETURN: <object>
    EXAMPLES:
      let deepObject = {
        a: 1, b: 2, c: 3,
        d: {
          a: 1, b: 2, c: 3,
          d: {
            a: 1, b: 2, c: 3,
            d: 4,
          },
        },
      };
      $$.object.removeValues(deepObject, [1, 3]);    // { b: 2, d: { a: 1, b: 2, c: 3, d: { a: 1, b: 2, c: 3, d: 4 } } }
      $$.object.removeValues(deepObject, [1, 3], 1); // { b: 2, d: {       b: 2,       d: { a: 1, b: 2, c: 3, d: 4 } } }
      $$.object.removeValues(deepObject, [1, 3], 9); // { b: 2, d: {       b: 2,       d: {       b: 2,       d: 4 } } }
  */
  removeValues: function (object, valuesToRemove, recursive) {
    var result = _.clone(object);
    _.each(result, function (entry, key) {
      // remove requested keys
      _.each(valuesToRemove, function (valueToRemove) {
        if (_.isEqual(entry, $$.result(valueToRemove))) delete result[key];
      });
      // recurse if asked
      if (recursive && $$.isObjectOrArray(entry)) result[key] = $$.object.removeValues(entry, valuesToRemove, recursive -1);
    });
    return result;
  },

  /**
    DESCRIPTION:
      remove values from an object if they are the same as the provided default object
      if there are some subobjects, and they or the default one contains the key _recursiveOption, will recurse the object and remove default in subobjects as well
      this is somehow the opposite of $$.defaults (see $$.defaults)
    ARGUMENTS: (
      !object <object>,
      !defaults <object>,
    )
    RETURN: <object>
    EXAMPLES:
      $$.object.removeDefaultValues(
        { a: 1, b: 1, c: 3, d: 8 },
        { a: 1, b: 2, c: 3, d: 4 }
      ); // { b: 1, d: 8 }
      $$.object.removeDefaultValues(
        {
          a: 1, b: 1, c: 3,
          d: { _recursiveOption: true, a: "hello", b: "you", },
        },
        {
          a: 1, b: 2, c: 3,
          d: { _recursiveOption: true, a: "a", b: "b", },
        },
      ); // { b: 1, d: { a: "hello", b: "you" } }
  */
  removeDefaultValues: function (object, defaults) {
    // clone object before doing something on it
    var objectClone = _.clone(object);
    _.each(objectClone, function (value, key) {
      // remove value if is equal to default one
      if (_.isEqual(defaults[key], value)) delete objectClone[key];
      // recurse if object and _recursiveOption is set in it
      if ($$.isObjectLiteral(objectClone[key]) && $$.isObjectLiteral(defaults[key]) && (defaults[key]._recursiveOption || objectClone[key]._recursiveOption)) objectClone[key] = $$.object.removeDefaultValues(objectClone[key], defaults[key]);
    });
    return objectClone;
  },

  /**
    DESCRIPTION: return a new version of the passed object with it's keys sorted in alphabetical order
    ARGUMENTS: ( unorderedObject <object> )
    RETURN: orderedObject <object>
    EXAMPLES:
      $$.object.sort({ c: 3, a: 1, d: 4, b: 2, }); // { a: 1, b: 2, c: 3, d: 4 }
  */
  sort: function (unorderedObject) {
    var orderedObject = _.isArray(unorderedObject) ? [] : {};
    Object.keys(unorderedObject).sort().forEach(function(key) {
      orderedObject[key] = $$.isObjectOrArray(unorderedObject[key]) ? $$.object.sort(unorderedObject[key]) : unorderedObject[key];
    });
    return orderedObject;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$.object;
