var _ = require("underscore");
var $$ = require("../core");

$$.array = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: wrap any element in an array, or return it if it's already an array
    ARGUMENTS: ( val <any> )
    RETURN: <array>
  */
  make: function (val) {
    if (_.isArray(val)) return val
    else return [val];
  },

  /**
    DESCRIPTION:
      everytime this is activated, it will return the next element in the array
      when reaching the end of the array, go back to first index
    ARGUMENTS: (
      !array <array> « array to iterate values of »,
      !arrayIdentification <string> « unique name to identify the array »,
    )
    RETURN: <any>
    EXAMPLES:
      $$.array.toggle([1, 2, 3], "yo"); // 1
      $$.array.toggle([1, 2, 3], "yo"); // 2
      $$.array.toggle([1, 2, 3], "yo"); // 3
      $$.array.toggle([1, 2, 3], "yo"); // 1
  */
  toggle: function (array, arrayIdentification) {

    // create saving of number of time toggled
    if (!_.has($$.array.toggle, arrayIdentification)) $$.array.toggle[arrayIdentification] = 0
    // increase toggling time (increment index)
    else $$.array.toggle[arrayIdentification]++;

    // if reached limit of array, restart from beginning
    if ($$.array.toggle[arrayIdentification] >= array.length) $$.array.toggle[arrayIdentification] = 0;

    // return selected element
    return array[$$.array.toggle[arrayIdentification]];

  },

  /**
    DESCRIPTION: check if an array is undefined, empty or filled with only invalid values (the last is checked with _.compact)
    ARGUMENTS: ( array )
    RETURN: <boolean>
    EXAMPLES:
      $$.array.isEmpty([]);                // true
      $$.array.isEmpty([1, 2]);            // false
      $$.array.isEmpty([null, undefined]); // true
  */
  isEmpty: function (array) {
    return !array || _.isEqual(_.compact(array), []);
  },

  /**
    DESCRIPTION: merge any ammount of arrays
    ARGUMENTS: ( ...args <any[]> « if arg is not an array, it will be wrapped in an array (or ignored if undefined) » )
    RETURN: <array>
    EXAMPLES:
      $$.array.merge([ "first", "array"], "and", { object: "", }, [ "and", "other array", ]);
      // [ "first", "array", "and", { object: "", }, "and", "other array" ]
  */
  merge: function(){
    var object = [];
    _.each(arguments, function(array,i){

      // create array if undefined
      if (array === undefined) array = []
      // arrayify any non array argument
      else if (!_.isArray(array)) array = [array];

      object.push(array);

    });
    return _.flatten(object, true);
  },

  /**
    DESCRIPTION:
      remove a value from an array (value is matched with _.isEqual function)
      will modify the given array
    ARGUMENTS: (
      !array <array>,
      !val <any>,
      ?unexistantCallback <function> « if passed, will be executed if given value is not found in array »,
    )
    RETURN: <array>
    EXAMPLES:
      $$.array.removeByValue([1, 2, 3, { a: 1 }], { a: 1 }); // [1, 2, 3]
      $$.array.removeByValue([1, 2, 3, { a: 1 }], 1); // [2, 3, { a: 1 }]
  */
  removeByValue: function (array, val, unexistantCallback) {

    var index = $$.array.indexOf(array, val);

    // remove entry from array
    if (index != -1) array.splice(index, 1)
    else if (unexistantCallback) unexistantCallback();
    // else if (noAlertIfUnexistant) return
    // else console.log("Error removing entry from array!", " entry: ", val, " array: ", array)

    return array;

  },

  /**
    DESCRIPTION: returns the index of a value in an array (value is matched with _.isEqual function so it works on object entries (unlike _.indexOf))
    ARGUMENTS: (
      !array <array>,
      ?value <any>,
    )
    RETURN: <number>
    EXAMPLES:
      $$.array.indexOf([{a: 1}, {b: 2}], {b: 2}); // 1
      $$.array.indexOf([{a: 1}, {b: 2}], {b: 3}); // -1
  */
  indexOf: function (array, val) {

    var index = -1;

    _.find(array, function(value, i){
      if (_.isEqual(value, val)) {
        index = i;
        return true;
      };
    })

    return index;

  },

  /**
    DESCRIPTION: similar to _.compact, but can freely choose which entries should be removed (default is only strict undefined values)
    ARGUMENTS: (
      !array <any[]>,
      ?entriesToRemove <any[]>@default=<[undefined]>,
    )
    RETURN: <any[]>
  */
  compact: function (array, entriesToRemove) {
    // default entries to remove
    if (!entriesToRemove) entriesToRemove = [ undefined ];
    // remove
    return _.reject(array, function (entry) {
      for (var key in entriesToRemove) {
        if (_.isEqual(entry, entriesToRemove[key])) return true;
      }
    });
  },

  /**
    DESCRIPTION: move an element in an array (modifies the array and return it)
    ARGUMENTS: (
      !array <array>,
      !fromTo <{ from: <integer>, to: <integer>, }>,
      ?extendArrayIfHigher <boolean>@default=false « if true and the new asked index is higher than table length, will create new elements in between »
    )
    RETURN: <array>
    EXAMPLES:
      $$.array.move([1,2,3,4], {from: 1, to: 2});       // [ 1, 3, 2, 4 ]
      $$.array.move([1,2,3,4], {from: 1, to: 5});       // [ 1, 3, 4, 2 ]
      $$.array.move([1,2,3,4], {from: 1, to: 5}, true); // [ 1, 3, 4, undefined, undefined, 2 ]
  */
  move: function (array, fromTo, extendArrayIfHigher) {

    if (!_.isArray(array)) {
      $$.alert.detailedError(
        "$$.array.move",
        "The provided value is not an array:",
        array,
      );
      return array;
    };
    var old_index = fromTo.from;
    var new_index = fromTo.to;

    // make new index max index if it is higher than array.length
    if (new_index >= array.length && !extendArrayIfHigher) new_index = array.length - 1;
    // create undefined entries if new index is higher than array.length
    else if (new_index >= array.length) {
      var k = new_index - array.length;
      while ((k--) + 1) {
        array.push(undefined);
      }
    };

    // move
    array.splice(new_index, 0, array.splice(old_index, 1)[0]);

    // return modified array
    return array;

  },

  /**
    DESCRIPTION: convert an array which first column are keyTitles to a collection
    ARGUMENTS: ( <array> )
    RETURN: <collection>
    EXAMPLES:
      $$.array.toCollection([
        ["names", "age"],
        ["Jorge", 67],
        ["Julie", 59],
        ["Micha", 18],
      ]);
      // > [
      // >   { name: "Jorge", age: 67, },
      // >   { name: "Julie", age: 59, },
      // >   { name: "Micha", age: 18, },
      // > ]
  */
  toCollection: function (array) {
    return _.map(_.rest(array), function(entry,i){
      return _.object(array[0], entry);
    });
  },

  /**
    DESCRIPTION: replace matching element in array by given value (element is matched with _.isEqual function)
    ARGUMENTS: (
      array <array>,
      replace <any>,
      replaceBy <any>,
      callbackIfAbsent <function> « will be executed if replace is absent, if no callback defined, replaceBy is pushed to array »
    )
    RETURN: <void> « given array is modified »
  */
  replace: function (array, replace, replaceBy, callbackIfAbsent) {
    var index = $$.array.indexOf(array, val);
    // var indexOfReplaceValue = array.indexOf(replace);
    if (indexOfReplaceValue != -1) array[indexOfReplaceValue] = replaceBy
    else if (callbackIfAbsent) callbackIfAbsent()
    else array.push(replaceBy);
  },


  // /*
  //   DESCRIPTION: returns the values from array that are not present in the other arrays
  //   ARGUMENTS: (
  //     array <array>
  //     *others <any>
  //   )
  // */
  // difference: function (array) {
  //   var result = _.clone(array);
  //   if (!_.isArray(array)) return $$.log.returnLog(null, "Type error for $$.array.difference, array is not an array: ", array);
  //   // for each other array or object
  //   _.each(_.rest(arguments), function(other,i){
  //     $$.each(other, function(val,key){
  //       $$.array.removeByValue(result, val)
  //     });
  //   });
  //   return result;
  // },
  // /*
  //   DESCRIPTION: but returns the values from array that are not present in the other arrays or objects ///// MAYBE NOT USEFUL TO SUPPORT OBEJCTS
  //   ARGUMENTS: (
  //     array <array|object>
  //     *others <array|object> «but must be same type as array»
  //   )
  // */
  // // difference: function(array){
  // //   var result;
  // //   var type = $$.typeof(array);
  // //   if (type !== "array" || type !== "object") return $$.log.returnLog(null, "Type error for $$.array.difference, array is not an array or object: ", array);
  // //   // for each other array or object
  // //   _.each(_.rest(arguments), function(other,i){
  // //     if (type == "array") {
  // //       if (!_.isArray(other)) other = [other];
  // //       result = _.difference(result, other);
  // //     }
  // //     else {
  // //       if (!$$.isObjectLiteral(other)) return $$.log.returnLog(null, "Type error for $$.array.difference, array is object but other is not: ", array, other);
  // //       _.each
  // //     };
  // //   });
  // //   return result;
  // // },

  // /** //// NOT EVER USED BUT COULD BE ACTIVATED IF NEEDED
  //   DESCRIPTION: shift all the elements of an array, in other words it moves all the n first elements at the end of the array
  //   ARGUMENTS: (
  //     !originArray <any[]> « the array to shift (will not be modified) »,
  //     !startingIndex <integer> « the index where the first entry should be »,
  //   )
  //   RETURN: <any[]>
  //   EXAMPLES:
  //     $$.array.shift([0, 1, 2, 3, 4, 5, 6], 4); // [4, 5, 6, 0, 1, 2, 3]
  //     $$.array.shift(["in", "order", ".", "A", "sentence"], 3); // ["A", "sentence", "in", "order", "."]
  // */
  // shift: function (originArray, startingIndex) {
  //
  //   // first clone the array before editing it
  //   var resultArray = _.clone(originArray);
  //
  //   // get all the first elements that should be moved
  //   var portionToMove = resultArray.splice(0, startingIndex);
  //
  //   // add those elements at the end of the array
  //   resultArray.push.apply(resultArray, portionToMove);
  //
  //   return resultArray;
  //
  // },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$.array;
