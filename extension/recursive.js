var _ = require("underscore");
var $$ = require("../core");

/**
  DESCRIPTION:
    iterate recursively in an object and execute a function on each entry
    can also be used for mapping because it returns the result of iterating functions
  ARGUMENTS: (
    !object <object>,
    !func <function(
      value <any>            « the currently iterated value »,
      currentKey <string>    « the currently iterated key »,
      currentObject <object> « the currently iterated object »,
      deepKey <string>       « the deepKey of currently iterated value (key in relation to rootObject) »,
      rootObject <object>    « the object you passed to $$.recursive() »,
    ):<any>> « the return of this function will be the value for that key in the finally returned object »,
    ?options <{
      ?root: <string>@default="" « string that will be prepended to deep keys »,
      ?delimitor: <string>@default="." « what delimitor is used in keys to describe deep keys »,
      ?condition: <function(
        value <any>            « the currently iterated value »,
        currentKey <string>    « the currently iterated key »,
        currentObject <object> « the currently iterated object »,
        deepKey <string>       « the deepKey of currently iterated value (key in relation to rootObject) »,
        rootObject <object>    « the object you passed to $$.recursive() »,
      )> « condition to continue to recurse, default is that subentry should be an object, and not have "_recurse: false" set »,
      ¡deepness: <integer> « FOR INTERNAL USE ONLY, describes the current deepness of the recursion »,
      ¡rootObject: <object> « FOR INTERNAL USE ONLY, the object you passed to $$.recursive »,
    },
  )
  RETURN: <object> « object containing recursively the values returned by iterate function »
  EXAMPLES:
    $$.recursive({
      a: 1,
      b: 2,
      c: {
        some: "deeper value",
        another: "one",
        and: {
          something: "even deeper",
        },
      },
    }, function (val) {
      console.log(val);
      return val + 9;
    });
    // will log all objects and subobjects values and return the following:
    // > {
    // >   a: 10,
    // >   b: 11,
    // >   c: {
    // >     some: "deeper value9",
    // >     another: "one9",
    // >     and: {
    // >       something: "even deeper9",
    // >     },
    // >   },
    // > }
    let flatList = [];
    $$.recursive({
      a: 1,
      b: 2,
      c: {
        some: "deeper value",
        another: "one",
        and: {
          something: "even deeper",
        },
      },
    }, function (val, key, currentObject, deepKey, rootObject) {
      flatList.push({
        key: deepKey,
        value: val,
      });
    }, {
      delimitor: "/",
      root: "/",
    });
    // flatlist value is now:
    // > [
    // >   { key: "/a", value: 1 },
    // >   ​{ key: "/b", value: 2 },
    // >   ​{ key: "/c/some", value: "deeper value" },
    // >   ​{ key: "/c/another", value: "one" },
    // >   ​{ key: "/c/and/something", value: "even deeper" },
    // > ]
*/
$$.recursive = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see $$.recursive
  */
  main: function (object, func, options, deepness) {

    //
    //                              DEFAULTS

    var defaultOptions = {
      root: "/",
      delimitor: "/",
      deepness: -1,
      condition: function (entry) { return $$.isObjectLiteral(entry) && entry._recurse !== false;  },
    };
    options = $$.defaults(defaultOptions, options);

    // set internal options parameters
    options.deepness = options.deepness + 1;
    if (options.deepness == 0) options.rootObject = object;

    //
    //                              ITERATE AND RETURN

    var target = _.isArray(object) ? [] : {};

    // iterate over object
    _.each(object, function (val, key) {

      // set options to pass
      var opts = _.clone(options);
      opts.root += opts.deepness ? opts.delimitor + key : key;

      // recurse
      if (options.condition(val, key, object, opts.root, opts.rootObject)) target[key] = $$.recursive(val, func, opts)
      // or execute func
      else target[key] = func(val, key, object, opts.root, opts.rootObject);

    });

    return target;

    //                              ¬
    //

  },

  /**
    DESCRIPTION: a recursive version of _.size()
    ARGUMENTS: (
      !obj <object>,
      !countOnlySubValues <boolean> « if true, all object or array entries won't be counted, only they're children will be »
    )
    RETURN: <integer>
    EXAMPLES:
      $$.recursive.size({ a: 1, b: 2, c: { d: 3, e: { f: 4, }, }, }); // 6
      $$.recursive.size({ a: 1, b: 2, c: { d: 3, e: { f: 4, }, }, }, true); // 4
  */
  size: function (obj, countOnlySubValues) {
    // get size of obj
    var size = _.size(obj);
    // add size of subobjects
    _.each(obj, function(val,key){
      if ($$.isObjectOrArray(val)) {
        // do not count this object if asked
        if (countOnlySubValues) size--;
        // count subvalues
        size += $$.recursive.size(val, countOnlySubValues);
      }
    });
    // return
    return size;
  },


  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = $$.recursive;
