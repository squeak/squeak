var _ = require("underscore");
var $$ = require("../core");

/**
  DESCRIPTION: get value of a key in a ["key1:value1", "key2:value2", "key3:value3"] like array
  ARGUMENTS: (
    !object <object|array|comaStringifiedArray>,
    !key <string>,
  )
  RETURN: <string>
*/
$$.tag = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  main: function (object, key) {
    var asked = [];
    // key match
    var match = new RegExp ("^"+ key +"\\s*\\:\\s*");
    $$.each(object, function(d,i){
      // if there is matching key in list, add to return array
      if (d.match(match)) asked.push(d.replace(match, ""));
    }, { split: /\s*,\s*/, });
    // return array of values matching key
    return asked;
  },

  list: function (object) {
    var tagsList = [];
    // keys matcher
    var match = new RegExp ("^[^\\:]*\\s*\\:\\s*");
    $$.each(object, function (d,i) {
      // find key
      var key = object.match(match);
      // if any key, add to list
      if (key) tagsList.push({
        key: key[0].replace(/\s*\:\s*$/, ""),
        value: object.replace(match, ""),
      })
      else console.log("unable to find key in string", d);
    }, { split: /\s*,\s*/, });
    // return array of keys and values
    return tagsList;
  },

  /**
    DESCRIPTION: get an object with key/value pairs from a ["key1:value1", "key2:value2", "key3:value3"] like array
    ARGUMENTS: ( !object <object|array|comaStringifiedArray> )
    RETURN: <object> « { key1: value1, key2: value2, key3: value3 } »
  */
  // TODO

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = $$.tag;
