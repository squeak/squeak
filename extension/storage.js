var _ = require("underscore");
var $$ = require("../core");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  STORAGE INTERNAL FUNCTION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var storage = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get, set, edit an item from local or session storage
    ARGUMENTS: (
      !storageType <localStorage|sessionStorage>,
      ?key <string>,
      ?value <any stringifiable to JSON>,
      ?processing <function(item, key, value)> « function to process on the value »,
      ?url <string> « if you want to set it to a custom location »
    )
    RETURN: <any>
  */
  main: function (STORAGE, key, value, processing, url) {

    //
    //                              FETCH PAGE ITEM FROM STORAGE

    var fetched = storage.getItem(STORAGE, url);
    var item = fetched.item;

    //
    //                              PROCESS VALUE (set, push...)

    if (processing) var setOrNotSet = processing(item, key, value)
    else var setOrNotSet = false;

    //
    //                              SET ITEM IN STORAGE IF ASKED

    if (setOrNotSet) STORAGE.setItem(fetched.storageItemName, JSON.stringify(item));

    //
    //                              RETURN ASKED VALUE

    if (key) return item[key]
    else return item;

    //                              ¬
    //

  },

  // o            >            +            #            °            ·            ^            :            |
  //                                           GET ITEM

  /**
    DESCRIPTION: fetch an item from the local or session storage, relative to the current page url
    ARGUMENTS: (
      STORAGE <localStorage|sessionStorage>,
      ?url <string>,
    )
    RETURN: <object>
  */
  getItem: function (STORAGE, url) {

    // GET PAGE URL
    var storageItemName = url || window.location.pathname;

    // FETCH ITEM
    var obj = JSON.parse(STORAGE.getItem(storageItemName));

    // SET ITEM EMPTY VALUE IF UNDEFINED
    if (!obj) {
      STORAGE.setItem(storageItemName, "{}");
      obj = {};
    };

    // RETURN
    return {
      item: obj,
      storageItemName: storageItemName,
    };

    // ¬

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROCESSINGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  processings: {

    //
    //                              SET

    set: function (item, key, value) {
      // do not set if no value defined
      if (_.isUndefined(value)) return false
      // set value in item
      else {
        item[key] = value;
        // yes set value to storage
        return true;
      };
    },

    //
    //                              PUSH

    push: function (item, key, value) {
      if (!_.isArray(item[key])) item[key] = [value]
      else item[key].push(value);
      // yes set value to storage
      return true;
    },

    //
    //                              IF

    if: function (item, key, value) {
      if (_.isUndefined(item[key])) {
        item[key] = value;
        // yes set value to storage
        return true;
      }
      // no, do not set value to storage
      else return false;
    },

    //
    //                              PUSH IF DIFFERENT

    pushIfDifferent: function (item, key, value) {
      if (!_.isArray(item[key])) item[key] = [value]
      else if (item[key][item[key].length-1] != value) item[key].push(value);
      // yes set value to storage
      return true;
    },


    //
    //                              DELETE

    delete: function (item, key, value) {
      delete item[key];
      return true;
    },

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  $$.storage.local AND $$.storage.session GENERATION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

$$.storage = {};

_.each({ local: localStorage, session: sessionStorage, }, function (STORAGE, storageKey) {

  $$.storage[storageKey] = $$.scopeFunction({
    main: function (key, value, url) { return storage(STORAGE, key, value, storage.processings.set, url); },
  });

  _.each(storage.processings, function (processingFunction, processingKey) {
    $$.storage[storageKey][processingKey] = function (key, value, url) { return storage(STORAGE, key, value, processingFunction, url); };
  });

});

/**
  DESCRIPTION: get, set, edit an item from local storage
  ARGUMENTS: (
    ?key <string>,
    ?value <any stringifiable to JSON>,
    ?processing <function(item, key, value)> « function to process on the value »,
    ?url <string> « if you want to set it to a custom location »
  )
  RETURN: <any>
*/
// $$.storage.local = see upper

/**
  see $$.storage.local (it's the same but for session storage)
*/
// $$.storage.session = see upper

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$.storage;
