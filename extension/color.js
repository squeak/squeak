var _ = require("underscore");
var $$ = require("../core");
var color = require("color");

/**
  DESCRIPTION: shortcut to `color` library
  see https://github.com/Qix-/color#readme
*/
$$.color = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see $$.color
  */
  main: color,

  /**
    DESCRIPTION: get an rgba string from a color
    ARGUMENTS: ( ?col <colorString> « if undefined, will be black, if invalid will throw error » )
    RETURN: <rgba string>
  */
  rgba: function (col) {
    var colorObject = $$.color(col).rgb();
    return "rgba("+ colorObject.red() +","+ colorObject.green() +","+ colorObject.blue() +","+ colorObject.alpha() +")";
  },

  /**
    DESCRIPTION: change the opacity of a color
    ARGUMENTS: (
      ?col <colorString>,
      ?newOpacity <number between 0 and 1>,
    )
    RETURN: <rgba string>
  */
  opacity: function (col, newOpacity) {
    var colorObject = $$.color(col);
    return $$.color.rgba(colorObject.alpha(newOpacity));
  },

  /**
    DESCRIPTION:
      get Highly Sensitive Poo (HSP) for the given hex color
      HSP is an indicator of the lightness of a color
      light/dark threshold should be around 127.5 since range is between 0 and 255
    ARGUMENTS: ( !color <hexString> )
    RETURN: <number>
    EXAMPLES:
      $$.color.hsp("#00f"); // 86.09790938228407
      $$.color.hsp("#FF0F0F"); // 140.00071428389214
  */
  hsp: function (color) {

    // CONVERT HEX COLOR TO RGB (http://gist.github.com/983661)
    color = +("0x" + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));

    var red = color >> 16;
    var green = color >> 8 & 255;
    var blue = color & 255;

    // RETURN HSP (CALCULATION INSPIRED BY: http://alienryderflex.com/hsp.html)
    return Math.sqrt(
      0.299 * (red * red) +
      0.587 * (green * green) +
      0.114 * (blue * blue)
    );

  },

  /**
    DESCRIPTION: get contrast color of the given color
    ARGUMENTS: (
      !col <colorString> « color to find contrast of »,
      ?options <{
        ?light <colorString>@default=white « if contrast should be light, this color will be chosen »,
        ?dark <colorString>@default=white « if contrast should be dark, this color will be chosen »,
        ?null <colorString>@default=black « if no col specified or color is transparent, this color will be chosen »,
      }>,
    )
    RETURN: <colorString>
    EXAMPLES:
      $$.color.contrast("#00F"); // "white"
      $$.color.contrast("#0F0"); // black
  */
  contrast: function (col, options) {
    var defaultOptions = {
      light: "white",
      dark: "black",
      null: "black",
    };
    options = $$.defaults(defaultOptions, options);
    if (!col || col == "transparent") return options.null
    else {
      var color = $$.color(col);
      return color.luminosity() > 0.5 ? options.dark : options.light;
    };
  },

  /**
    DESCRIPTION: get style with backgroundColor and color (by default color passed is background and contrast front, to invert that, use the invert option)
    ARGUMENTS: (
      ?col <colorString> « color to find contrast style of, if this is missing, will return all empty strings (convenient to unset previously set css styles with jquery) »,
      ?options <{

        ?invert: <boolean> « invert background and front »,

        // default light and dark colors
        ?light: <colorString>@default=white « if contrast should be light, this color will be chosen »,
        ?dark: <colorString>@default=white « if contrast should be dark, this color will be chosen »,

        // secondary colors are usefull for subtitle texts that should not be black or white but grey
        ?secondaryLight <colorString>@default=#BBB « if contrast should be light, this color will be chosen as secondary »,
        ?secondaryDark <colorString>@default=#666 « if contrast should be dark, this color will be chosen as secondary »,

      }>,
    )
    RETURN: { color: <colorString>, backgroundColor: <colorString>, secondaryColor: <colorString>, }
    EXAMPLES:
      $$.color.contrastStyle("#00F"); // { backgroundColor: "#00F", color: "white", secondaryColor: "#BBB" }
  */
  contrastStyle: function (col, options) {

    // IF NO COLOR WAS ASKED RETURN AN UNSET OBJECT
    if (!col) return { backgroundColor: "", color: "", secondaryColor: "", };

    // DEFAULT OPTIONS
    var defaultOptions = {
      light: "white",
      dark: "black",
      invert: false,
      secondaryLight: "#BBB",
      secondaryDark: "#666",
    };
    options = $$.defaults(defaultOptions, options);

    // GET CONTRAST COLOR
    var contrastColor = $$.color.contrast(col, options) || "";

    // CHOSE COLOR AND BACKGROUND COLOR
    if (options.invert) {
      var color = col;
      var backgroundColor = contrastColor;
    }
    else {
      var color = contrastColor;
      var backgroundColor = col;
    };

    // RETURN COLORS OBJECT
    return {
      backgroundColor: backgroundColor,
      color: color,
      secondaryColor: backgroundColor == options.light ? options.secondaryLight : options.secondaryDark,
    };

  },

  /**
    DESCRIPTION: invert the passed rgb color
    ARGUMENTS: (
      |-- !rgbColor <string>,
      |-- !red <number|string> « between 0 and 255 »,
          !green <number|string> « between 0 and 255 »,
          !blue <number|string> « between 0 and 255 »,
          ?transparency <number|string> « between 0 and 1 »,
    )
    RETURN: <string> « resulting rgb color string »
  */
  invert: function (rgbColor) {

    // make array from rgb string or rgb numbers
    var rgbArray = [].slice.call(arguments).join(",").replace(/rgba?\(|\)|\s/gi, '').split(',');

    // invert colors array and return
    for (var i = 0; i < rgbArray.length; i++) rgbArray[i] = (i === 3 ? 1 : 255) - rgbArray[i];
    if (rgbArray.length == 4) return "rgba("+ rgbArray.join(", ") +")"
    else return "rgb("+ rgbArray.join(", ") +")";

  },

  /**
    DESCRIPTION: convert rgb and rgba color to hex
    ARGUMENTS: ( ?color <string> « the string to convert to rgb » )
    RETURN: <string> « the resulting hex string (if you passed a string that is not a rgb or rgba string, your string will be returned unchanged) »
  */
  rgb2hex: function (color) {

    // missing string or not rgb like
    if (!_.isString(color)) return "#000000";
    if (!color.match(/^rgba?\(.*\)$/)) return color;

    // make rgb array
    var rgbArray = color.replace(/^rgba?\(/, "").replace(/\)$/, "").split(/\s*,\s*/);
    if (rgbArray.length < 3) return "#000000";

    // make final hex string
    return "#" +
      ("0" + parseInt(rgbArray[0],10).toString(16)).slice(-2) +
      ("0" + parseInt(rgbArray[1],10).toString(16)).slice(-2) +
      ("0" + parseInt(rgbArray[2],10).toString(16)).slice(-2) +
      (rgbArray.length > 3 ? ("0" + parseInt(255 * rgbArray[3],10).toString(16)).slice(-2) : "")
    ;

  },

  /**
    DESCRIPTION:
      convert the given string into a random color
      a same string will always return the same color
      (colors will always be generated without transparency)
    ARGUMENTS: (
      !stringToConvertIntoColor <string>,
      ?possibleColors <hexColorString[]> « pass here a list of colors that can be used, if you don't want to have any kind of random color »,
      ?defaultColor <hexColorString>@default="#000000" « the color to return if the string to convert is empty »,
    )
    RETURN: <string> « hex color string »
    SOURCE: this function is heavily inspired from the following gist: https://gist.github.com/0x263b/2bdd90886c2036a1ad5bcf06d6e6fb37
  */
  anyString2HexColor: function (stringToConvertIntoColor, possibleColors, defaultColor) {
    var hash = 0;
    if (!defaultColor) defaultColor = "#000000";
    if (stringToConvertIntoColor.length === 0) return defaultColor;

    // generate random hash
    for (var i = 0; i < stringToConvertIntoColor.length; i++) {
      hash = stringToConvertIntoColor.charCodeAt(i) + ((hash << 5) - hash);
      hash = hash & hash;
    };

    // extract random color in given array
    if (_.isArray(possibleColors)) {
      var indexToExtract = ((hash % possibleColors.length) + possibleColors.length) % possibleColors.length;
      return possibleColors[indexToExtract];
    }
    // generate random color from string
    else {
      var color = '#';
      // make a hex color from hash
      for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 255;
        color += ('00' + value.toString(16)).substr(-2);
      };
      // return resulting color
      return color;
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = $$.color;
