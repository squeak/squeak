var _ = require("underscore");
var $$ = require("../core");
var $ = require("jquery");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// check if two rectangle overlap each other
function isCollide (a, b) {
  // var ay = a.y || a.top;
  // var by = a
  return !(
    ((a.y + a.height) < (b.y)) ||
    (a.y > (b.y + b.height)) ||
    ((a.x + a.width) < b.x) ||
    (a.x > (b.x + b.width))
  );
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  POSITION ONE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: position randomly one element in the given container
  ARGUMENTS: (
    !$target <jqueryProcessable>,
    !$container <jqueryProcessable>,
    ?options <{
      ?size: <"cover"|"contain">@default="cover" «
        similar to the behaviour of the background-size css options with those names
        with "contain" the $target element will have to be fully contained in $container
        with "cover" the $target element can overflow the $container, it just has to have part of itself in the $container
      »,
      ?h: <boolean>@default=true « if false, do not randomly set horizontal position »,
      ?v: <boolean>@default=true « if false, do not randomly set vertical position »,
      ?amplitudeMax: <number>@default=1 « value between 0 and 1 defining how far from the center of the $container the $target position can be set »,
    }>,
  )
  RETURN: <void>
  NOTE: to work you'll need target postition to absolute and container to relative
*/
$$.random.positionOne = function ($target, $container, options) {
  //----------------------------------------------- DEFAULT OPTIONS
  var defaultOptions = {
    size: "cover",
    h: true,
    v: true,
    amplitudeMax: 1,
  };
  options = $$.defaults(defaultOptions, options);
  //----------------------------------------------- DEFAULT OPTIONS END
  if (options.size == "cover") {
    var maxH = $container.width();
    var maxV = $container.height();
  }
  else {
    var maxH = $container.width() - $target.width();
    var maxV = $container.height() - $target.height();
  };
  if (options.h && maxH) $target.css($$.random.direction().h, $$.random.number(0, maxH * options.amplitudeMax));
  if (options.v && maxV) $target.css($$.random.direction().v, $$.random.number(0, maxV * options.amplitudeMax));
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  POSITION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    change dom element to random position, without overlapping other specified elements
    (will try 30 times randomly to find a position not overlapping, if it still overlaps will just set position to last position tried)
  ARGUMENTS: (
    !$container <yquerjObject>,
    !element <yquerjObject> « element to move »,
    !elements <yquerjObject> « elements not to overlap with »,
  )
  RETURN: <void> « change dom element's position »
*/
$$.random.position = function ($container, element, elements) {

  //
  //                              MAKE SURE ELEMENTS HAVE IDS

  var callindId = $$.uuid();
  elements.each(function(i,d){
    var element = $(d);
    if (!element.attr("id")) element.attr("id", "random-position-id-"+ callindId +"-"+ i);
  });

  //
  //                              NOT OVERLAPING

  if (elements.length > 1) {
    // store boxes positions
    var boxDims = [];
    var thisBox;
    function getBoxDim (elem) {
      return {
        // top: elem.css("top"),
        x: parseInt(window.getComputedStyle(elem).left),
        y: parseInt(window.getComputedStyle(elem).top),
        width: parseInt(window.getComputedStyle(elem).width),
        height: parseInt(window.getComputedStyle(elem).height),
      }
    };
    elements.each(function(i,d){
      // get dimensions
      var boxDimensions = getBoxDim($(d)[0]);
      // store dimensions for current box and others in different places
      if ($(d).attr("id") != element.attr("id")) boxDims.push(boxDimensions)
      else thisBox = boxDimensions;
    });
    //
    var conflict = true;
    var amountsOfConflicts = 0;
    while (conflict && amountsOfConflicts < 30) {
      // generate new position
      var box = {
        x: $$.random.number(0, $container.width() - element.width()),
        y: $$.random.number(0, $container.height() - element.height()),
        width: thisBox.width,
        height: thisBox.height,
      };
      // check if new position overlap other boxes
      for (var i=0; i<boxDims.length; i++) {
        if (isCollide(box, boxDims[i])) { conflict = true; break; }
        else conflict = false;
      };
      amountsOfConflicts ++;
    };
    // set new position now that it's sure there is no overlap
    $(element).css({
      left: box.x,
      top: box.y,
    });
  }

  //
  //                              OVERLAPING

  else {
    element.css({
      left: $$.random.number(0, $container.width() - element.width()),
      top: $$.random.number(0, $container.height() - element.height()),
    });
  };

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
