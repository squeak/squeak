var _ = require("underscore");
var $$ = require("../core");

$$.log.node = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHALK
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: reference to chalk library
      see https://www.npmjs.com/package/chalk
    CHILDREN:
      $$.log.node.chalk.colors = <[
        "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white", "gray",
        "redBright", "greenBright", "yellowBright", "blueBright", "magentaBright", "cyanBright", "whiteBright"
      ]> « list of colors supported by `chalk` library »,

      $$.log.node.chalk.bgColors = <[
        "bgBlack", "bgRed", "bgGreen", "bgYellow", "bgBlue", "bgMagenta", "bgCyan", "bgWhite",
        "bgBlackBright", "bgRedBright", "bgGreenBright", "bgYellowBright", "bgBlueBright", "bgMagentaBright", "bgCyanBright", "bgWhiteBright"
      ]> « list of background colors supported by `chalk` library »,

      $$.log.node.chalk.isChalkColorName: <function(color<string>):<boolean>> « test if a color is a valid `chalk` color or background color »
  */
  chalk: $$.scopeFunction({

    main: require("chalk"),

    colors: [
      "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white", "gray",
      "redBright", "greenBright", "yellowBright", "blueBright", "magentaBright", "cyanBright", "whiteBright"
    ],

    bgColors: [
      "bgBlack", "bgRed", "bgGreen", "bgYellow", "bgBlue", "bgMagenta", "bgCyan", "bgWhite",
      "bgBlackBright", "bgRedBright", "bgGreenBright", "bgYellowBright", "bgBlueBright", "bgMagentaBright", "bgCyanBright", "bgWhiteBright"
    ],

    isChalkColorName: function (color, onlyColors) {
      return _.indexOf($$.log.node.chalk.colors, color) != -1 || (!onlyColors && _.indexOf($$.log.node.chalk.bgColors, color) != -1);
    },

  }),

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RAINBOWS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: log multiple strings with different colors
    ARGUMENTS: ( ...args <[ see $$.log.node.style's arguments ][]> )
    RETURN: <void>
    EXAMPLES:
      $$.log.node.multiColor(
        [ "First part of string "],
        [ "second part in red ", "redBright" ],
        [ "third part is more important.", "redBright", { bold: true, underline: true, } ],
      );
  */
  multiColor: function () {

    var resultString = "";

    _.each(arguments, function (group) {
      resultString += $$.log.node.style(group[0], group[1], group[2]);
    });

    return console.log(resultString);

  },

  /**
    DESCRIPTION: log the given string with random rainbowy colors
    ARGUMENTS: ( see $$.log.node.rainbowString )
    RETURN: <void>
  */
  randomRainbow: function (string, bold, availableColors) {
    return $$.log($$.log.node.rainbowString(string, bold, availableColors))
  },

  /**
    DESCRIPTION: return the given string rainbow colored and if asked bolded for logging in node terminal
    ARGUMENTS: (
      !string <string> « the string to log »,
      ?options <see $$.log.node.style·options> « style options for string »,
      ?availableColors <hexOrChalkColorString[]> «
        list of colors to use, if not defined, all colors from rainbow will be used
        see $$.log.node.style for hexOrChalkColorString type
      »,
    )
    RETURN: <string>
  */
  rainbowString: function (string, options, availableColors) {

    var resultString = "";
    if (!availableColors) availableColors = _.reject($$.log.node.chalk.colors, function(d){ return d == "black"});

    _.each(string, function (letter) {
      resultString += $$.log.node.style(letter, $$.random.entry(availableColors), options);
    });

    return resultString;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  STYLING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: return the given string colored and if asked bolded for logging in node terminal
    ARGUMENTS: (
      !string <string>,
      ?color <hexOrChalkColorString|hexOrChalkColorString[]>,
      ?options <{
        ?bold: <boolean>,
        ?underline: <boolean>,
        ?strikethrough: <boolean>,
        ?italic: <boolean>,
        ?dim: <boolean>,
      }>,
    )
    RETURN: <string>
    TYPES:
      hexOrChalkColorString =
        | any hex color (e.g. #0FF0DE)
        | a color supported by chalk (e.g. yellowBright) (see $$.log.node.chalk.colors and see $$.log.node.chalk.bgColors for full list of supported ones)
    EXAMPLES:
      $$.log.node.style("Some styled message.", "green", { bold: true, });
      $$.log.node.style("A rainbow message.", "rainbow");
      $$.log.node.style("A randomly green and red message.", ["green", "red"]);
    TODO:
      - add support for multicoloredness with patterns like $0, $1, $2, $3 in string
  */
  style: function (string, color, options) {

    if (!options) options = {};

    //
    //                              RAINBOW STRING

    if (color == "rainbow") return $$.log.node.rainbowString(string, options)

    //
    //                              MULTICOLOR STRING

    else if (_.isArray(color)) return $$.log.node.rainbowString(string, options, color)

    //
    //                              COLORED STRING

    else {

      // default chalk
      chalkToUse = $$.log.node.chalk;

      // bold, underline, italic, strikethrough, dim
      if (options.bold) chalkToUse = chalkToUse.bold;
      if (options.underline) chalkToUse = chalkToUse.underline;
      if (options.italic) chalkToUse = chalkToUse.italic;
      if (options.strikethrough) chalkToUse = chalkToUse.strikethrough;
      if (options.dim) chalkToUse = chalkToUse.dim;

      // color
      if (color) {
        if ($$.match(color, /^#/)) chalkToUse = chalkToUse.hex(color)
        else if ($$.log.node.chalk.isChalkColorName(color)) chalkToUse = chalkToUse[color]
        else $$.log.detailedError(
          "squeak.log.style",
          "Color should be a hex string, or a valid chalk color name.",
          "the color you asked: ", color,
          "chalk available colors: ",
          _.map(
            _.flatten([$$.log.node.chalk.colors, $$.log.node.chalk.bgColors], true),
            function (col) { return $$.log.node.style(col, col); }
          ).join("\n")
        );
      };

      // return styled string
      return chalkToUse(string);

    };

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LINE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: log a line
    ARGUMENTS: (
      !color <same as $$.log.node.style·hexOrChalkColorString>,
      ?options <same as $$.log.node.style·options>,
    )
    RETURN: <void>
    EXAMPLES:
      $$.log.node.line("redBright", { bold: true, });
  */
  line: function (color, options) {
    var toLog = "";
    for (i=0; i<process.stdout.columns; i++) { toLog += "—" };
    return $$.log.style(toLog, color, options);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SAME LINE LOG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      log in same line (a log that overwrite itself)
      useful for example, to present a progress bar
    ARGUMENTS: ( ... same as $$.log.node.style )
    RETURN: <void>
    EXAMPLES:
      $$.log.node.write("|->");
      $$.log.node.write("|----->");
      $$.log.node.write("|------------->");
      $$.log.node.write("|--------------------|");
  */
  write: function (message, color, options) {
    process.stdout.write( $$.log.node.style(message, color, options) +"\r");
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TERMINAL SPINNER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a spinner line (with text message) in terminal
    ARGUMENTS: (messageOrOptions <string|{
      !message: <string> « message to display by the spinner »,
      ?color: <colorString>,
      ?spinner: <"simple"|"big"|"inout"|"random"|"randomNarrow"|"spiral">@default="simple"
      ?interval: <integer>@default=100 « spinning speed interval (in milliseconds) »
      ?finish: <string>@default="✔" « what to display in place of spinner when finished »,
    }> « if only a string, should be the message »)
    RETURN: <{
      clear: <function(ø):<void>> « clear the current outpur of the spinner (until next interval) »,
      destroy: <function(
        ?clear <boolean> « if true will clear log of spinner instead of displaying finish message »
      ):<void>> « stop the spinner »,
    }>
  */
  spinner: function (messageOrOptions) {
    var i = 0;

    // HANDLE PASSING ONLY MESSAGE OR FULL OPTIONS
    if (_.isString(messageOrOptions)) var options = { message: messageOrOptions, }
    else var options = messageOrOptions;

    // DEFAULT OPTIONS
    var defaultOptions = {
      interval: 100,
      spinner: "simple",
      finish: "✔",
    };
    options = $$.defaults(defaultOptions, options);

    // ALL TYPES OF SPINNERS
    var brailleAlphabet = "⠁ ⠂ ⠃ ⠄ ⠅ ⠆ ⠇ ⠈ ⠉ ⠊ ⠋ ⠌ ⠍ ⠎ ⠏ ⠐ ⠑ ⠒ ⠓ ⠔ ⠕ ⠖ ⠗ ⠘ ⠙ ⠚ ⠛ ⠜ ⠝ ⠞ ⠟ ⠠ ⠡ ⠢ ⠣ ⠤ ⠥ ⠦ ⠧ ⠨ ⠩ ⠪ ⠫ ⠬ ⠭ ⠮ ⠯ ⠰ ⠱ ⠲ ⠳ ⠴ ⠵ ⠶ ⠷ ⠸ ⠹ ⠺ ⠻ ⠼ ⠽ ⠾ ⠿ ⡀ ⡁ ⡂ ⡃ ⡄ ⡅ ⡆ ⡇ ⡈ ⡉ ⡊ ⡋ ⡌ ⡍ ⡎ ⡏ ⡐ ⡑ ⡒ ⡓ ⡔ ⡕ ⡖ ⡗ ⡘ ⡙ ⡚ ⡛ ⡜ ⡝ ⡞ ⡟ ⡠ ⡡ ⡢ ⡣ ⡤ ⡥ ⡦ ⡧ ⡨ ⡩ ⡪ ⡫ ⡬ ⡭ ⡮ ⡯ ⡰ ⡱ ⡲ ⡳ ⡴ ⡵ ⡶ ⡷ ⡸ ⡹ ⡺ ⡻ ⡼ ⡽ ⡾ ⡿ ⢀ ⢁ ⢂ ⢃ ⢄ ⢅ ⢆ ⢇ ⢈ ⢉ ⢊ ⢋ ⢌ ⢍ ⢎ ⢏ ⢐ ⢑ ⢒ ⢓ ⢔ ⢕ ⢖ ⢗ ⢘ ⢙ ⢚ ⢛ ⢜ ⢝ ⢞ ⢟ ⢠ ⢡ ⢢ ⢣ ⢤ ⢥ ⢦ ⢧ ⢨ ⢩ ⢪ ⢫ ⢬ ⢭ ⢮ ⢯ ⢰ ⢱ ⢲ ⢳ ⢴ ⢵ ⢶ ⢷ ⢸ ⢹ ⢺ ⢻ ⢼ ⢽ ⢾ ⢿ ⣀ ⣁ ⣂ ⣃ ⣄ ⣅ ⣆ ⣇ ⣈ ⣉ ⣊ ⣋ ⣌ ⣍ ⣎ ⣏ ⣐ ⣑ ⣒ ⣓ ⣔ ⣕ ⣖ ⣗ ⣘ ⣙ ⣚ ⣛ ⣜ ⣝ ⣞ ⣟ ⣠ ⣡ ⣢ ⣣ ⣤ ⣥ ⣦ ⣧ ⣨ ⣩ ⣪ ⣫ ⣬ ⣭ ⣮ ⣯ ⣰ ⣱ ⣲ ⣳ ⣴ ⣵ ⣶ ⣷ ⣸ ⣹ ⣺ ⣻ ⣼ ⣽ ⣾ ⣿".split(" ");
    var spiralHalf = "⣾⣿ ⣽⣿ ⣻⣿ ⢿⣿ ⡿⣿ ⣿⢿ ⣿⡿ ⣿⣟ ⣿⣯ ⣿⣷ ⣿⣾ ⣷⣿ ⣯⣿ ⣟⣿ ⣿⣻ ⣿⣽".split(" "); // ⣼ ⣛  ⠷ ⠿
    var spinners = {
      simple: "⠋⠙⠹⠸⠼⠴⠦⠧⠇⠏", // ⣰
      inout: "⠁⠃⠇⠇⡇⣇⣧⣷⣿⣾⣼⣸⢸⠸⠘⠈",
      randomNarrow: _.shuffle(brailleAlphabet),
      random: _.map(_.zip(_.shuffle(brailleAlphabet), _.shuffle(brailleAlphabet)), function (arr) { return arr.join(""); }),
      spiral: _.flatten([spiralHalf, _.clone(spiralHalf).reverse()]),
      big: "⣾⣿ ⣽⣿ ⣻⣿ ⢿⣿ ⡿⣿ ⣿⢿ ⣿⡿ ⣿⣟ ⣿⣯ ⣿⣷ ⣿⣾ ⣷⣿".split(" "), // ⣼ ⣛  ⠷ ⠿    ⣯⣿ ⣟⣿ ⣿⣻ ⣿⣽
    };

    // FIGURE OUT WHICH SPINNER TO USE
    var spinnerToUse = spinners[options.spinner] || spinners.simple;

    // WRITE FUNCTION
    function write (spinnerToUse) {
      i = (i+1) % spinnerToUse.length;
      // clear current text
      process.stdout.clearLine();
      // move cursor to beginning of line
      process.stdout.cursorTo(0);
      // write message with spinner
      process.stdout.write($$.log.node.style(" "+ spinnerToUse[i] +" "+ options.message, options.color));
    };

    // CREATE SPINNER LOG AT INTERVAL
    var interval = setInterval(_.partial(write, spinnerToUse), options.interval);

    // RETURN DESTROY METHOD TO BE ABLE TO STOP THE SPINNER
    var spinnerElement = {
      clear: function () {
        // clear current text
        process.stdout.clearLine();
        // move cursor to beginning of line
        process.stdout.cursorTo(0);
      },
      destroy: function (clear) {
        clearInterval(interval);
        if (clear) spinnerElement.clear()
        else {
          write([options.finish]);
          $$.log();
        };
      },
    };
    return spinnerElement;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
}

module.exports = $$.log.node;
