var _ = require("underscore");
var $$ = require("../core");
require("../extension/path");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    get the file type for the given file path
    extensions verification is not case sensitive
  ARGUMENTS: ( path <string> )
  RETURN:
    <keyof $$.fileType.extensions|"unknown">
    « with the default value of $$.fileType.extensions, type of return is <"image"|"video"|"audio"|"unknown"> »
  EXAMPLES:
    $$.fileType("/path/to/my/song.ogg");  // "audio"
    $$.fileType("/path/to/my/image.JPG"); // "image"
*/
$$.fileType = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see $$.fileType
  */
  main: function (path) {
    var extension = $$.path.getExtension(path);
    var fileType = "unknown";
    $$.breakableEach($$.fileType.extensions, function (extsList, type) {
      return $$.breakableEach(extsList, function (ext) {
        if (extension.toLowerCase() == ext.toLowerCase()) {
          fileType = type;
          return "break";
        };
      });
    });
    return fileType;
  },

  /**
    DESCRIPTION:
      list of known extensions
      this list is used by $$.fileType method to identify the file type
      customize these objects to extend $$.fileType support for more file types
    TYPE: <{
      [type<string>]: listOfExtensionsForThisType<string[]>
    }>@default={
      video: [ "mp4", "avi", "mkv", "flv", "mov", "m4v" ],
      audio: [ "m4a", "mp3", "ogg", "wav", "flac", "aif", "aiff", "wma" ],
      image: [ "jpg", "jpeg", "png", "svg", "tif", "tiff" ],
    }
  */
  extensions: {
    video: [ "mp4", "avi", "mkv", "flv", "mov", "m4v" ],
    audio: [ "m4a", "mp3", "ogg", "wav", "flac", "aif", "aiff", "wma" ],
    image: [ "jpg", "jpeg", "png", "svg", "tif", "tiff" ],
  },

  /**
    DESCRIPTION: test if the given file's path matches the given extensions
    ARGUMENTS: (
      !path <string>,
      !arrayOfExtensions <string[]>,
    )
    RETURN: <boolean>
    EXAMPLES:
      $$.fileType.is("/path/to/film.avi", $$.fileType.extensions.image); // false
  */
  is: function (path, arrayOfExtensions) {
    var extension = $$.path.getExtension(path).toLowerCase();
    return _.indexOf(arrayOfExtensions, extension) != -1;
  },

  /**
    DESCRIPTION: test if the given file path corresponds to an audio file
    ARGUMENTS: ( !path <string> )
    RETURN: <boolean>
    EXAMPLES:
      $$.fileType.isAudio("sound.mp3"); // true
  */
  isAudio: function (path) {
    return $$.fileType.is(path, $$.fileType.extensions.audio);
  },

  /**
    DESCRIPTION: test if the given file path corresponds to a video file
    ARGUMENTS: ( !path <string> )
    RETURN: <boolean>
    EXAMPLES:
      $$.fileType.isVideo("sound.mp3"); // false
  */
  isVideo: function (path) {
    return $$.fileType.is(path, $$.fileType.extensions.video);
  },

  /**
    DESCRIPTION: test if the given file path corresponds to an image file
    ARGUMENTS: ( !path <string> )
    RETURN: <boolean>
    EXAMPLES:
      $$.fileType.isImage("image.png"); // true
  */
  isImage: function (path) {
    return $$.fileType.is(path, $$.fileType.extensions.image);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = $$;
