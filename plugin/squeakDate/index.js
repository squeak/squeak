var _ = require("underscore");
var $$ = require("../../core");
var fromString = require("./fromString");
var fromMoments = require("./fromMoments");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a squeakDate from the given string or moment object
  ARGUMENTS: (
    !momentObjectsOrString <
      |squeakDateOrRange_string
      |squeakDateOrRange_justMoment
    >,
    ?customSettings <squeakDateSettings> « custom settings to process date »,
  )
  RETURN: <squeakDate|squeakDateSingle|squeakDateRange>
  EXAMPLES:
    // in all those examples, squeakDate1 represents the same date as squeakDate2

    // SIMPLE EXAMPLE

    // from string
    var squeakDate1 = $$.squeakDate("2019-12-15");
    // from moment object
    var squeakDate2 = $$.squeakDate(moment("2019-12-15"));

    // MORE ADVANCED USAGE
    // from string
    var squeakDate1 = $$.squeakDates("2019-12-15_12:00#2019-12-17_12:00,2020-11-11_12:30,2030-01-19_04:39#2030-03-03_04:35");
    // from moment objects
    var squeakDate2 = $$.squeakDates(
      // the list of dates
      [
        {
          start: moment("2019-12-15_12:00", "YYYY-MM-DD_HH:mm"),
          end: moment("2019-12-17_12:00", "YYYY-MM-DD_HH:mm"),
        },
        moment("2020-11-11_12:30", "YYYY-MM-DD_HH:mm"),
        {
          start: moment("2030-01-19_04:39", "YYYY-MM-DD_HH:mm");
          end: moment("2030-03-03_04:35", "YYYY-MM-DD_HH:mm");
        },
      ],
      // the list of elements to get from the moment objects
      { momentParseParts: ["year", "month", "day", "hours", "minutes"], }
    );

    // YOU CAN OMIT SOME DATES PARTS IF THEY ARE SHARED AMONG ALL DATES IN THE LIST
    // from string
    var squeakDate1 = $$.squeakDates("2019-12-15#19,21,28#30");
    // from moment objects
    var squeakDate2 = $$.squeakDates(
      [
        {
          start: moment("2019-12-15", "YYYY-MM-DD"),
          end: moment("2019-12-19", "YYYY-MM-DD"),
        },
        moment("2019-12-19", "YYYY-MM-DD"),
        moment("2019-12-21", "YYYY-MM-DD"),
        {
          start: moment("2019-12-28", "YYYY-MM-DD"),
          end: moment("2019-12-30", "YYYY-MM-DD"),
        },
      ]
    );
  TYPES:
    //
    //                              SQUEAK DATE

    squeakDate = <{

      isSqueakDate: true,
      dates: <squeakDateOrRange[]>,
      getString: <function(ø):<squeakDate_string>>,
      getDatesCommonParts: <function(ø)<datePartsToOmit>>,

    }>
    squeakDateOrRange = <squeakDateSingle|squeakDateRange>
    squeakDateRange = <{

      isSqueakDateRange: true,

      start: <squeakDateSingle>,
      end: <squeakDateSingle>,

      getDatesCommonParts: <function(ø)>,
      getDatesFormats: <function(ø)>,
      getString: <function(ø):<string>>,
      getSorting: <function(ø):<Date>> « get a sorting value to know how to sort this date in a list »,

    }>
    squeakDateSingle = <{

      isSqueakDateSingle: true,

      ?year: <number>,
      ?month: <number>,
      ?day: <number>,
      ?hours: <number>,
      ?minutes: <number>,
      ?seconds: <number>,

      ?hasTime: <boolean>,
      internal: <any> « elements that are used internally but may not always be present in a squeak date »,

      getFormat: <function(componentsToOmit <string[]>):<string>>,
      getString: <function(ø):<string>>,
      getMoment: <function(ø):<momentObject>>,
      getSorting: <function(ø):<Date>> « get a sorting value to know how to sort this date in a list »,

    }>

    //
    //                              SQUEAK DATE STRING

    squeakDate_string = <squeakDateOrRange_string|squeakDateOrRange_string[]> «
      DESCRIPTION: string in the form, for example:
        2019-12-09#27,2019-12-30_19:30:24,2020-01-02#02-03
        2019-12-09#27,28,30#31,
        It's a list of dates or date ranges that are in the form YYYY-MM-DD_HH:mm:ss and YYYY-MM-DD_HH:mm:ss#YYYY-MM-DD_HH:mm:ss
        if there is no seconds precision (or minutes, hours, day, month), they can just be omitted, e.g. YYYY-MM or YYYY-MM-DD_HH:mm
        if both dates in a date range have same year, or year and month, year and month and day..., this part will be omitted in the end date e.g. 2019-10-10#2019-11-12 will in fact be written 2019-10-10#11-12
        same between the different dates in the list, 2019-10-11,2019-10-14 is written 2019-10-11,14
        for dateRanges in the list, it will compare similar beginning in dates with start and end date and everything should match,
          e.g. 2019-11-11#2019-11-13,2019-11-17            becomes 2019-11-11#13,17
               2019-11-11#2019-11-13,2019-12-11            becomes 2019-11-11#11-13,12-11
               2019-11-11#2019-11-13,2019-12-11,2020-01-02  stays  2019-11-11#2019-11-13,2019-12-11,2020-01-02 because there is no part that is common to all dates
      NOTE: for now, this only supports multiple dates or ranges that have the same precision, anyway I'm not sure it'd be nice to support others because it's a bit strange and not so understandable
    »,
    squeakDateOrRange_string = <squeakDateRange_string|squeakDateSingle_string>,
    squeakDateRange_string = <"YYYY-MM-DD_HH:mm:ss#YYYY-MM-DD_HH:mm:ss"> « not all parts are necessary, see squeakDate_string explanation »,
    squeakDateSingle_string = <"YYYY-MM-DD_HH:mm:ss"> « not all parts are necessary, see squeakDate_string explanation »,

    //
    //                              SQUEAK DATE MOMENT OBJECTS

    squeakDate_justMoments = <squeakDateOrRange_justMoments|squeakDateOrRange_justMoments[]>,
    squeakDateOrRange_justMoments = <squeakDateRange_justMoments|squeakDateSingle_justMoment>,
    squeakDateRange_justMoments = <{
      start: <momentObject>,
      end: <momentObject>,
    }>,
    squeakDateSingle_justMoment = <momentObject>,

    //
    //                              SETTINGS AND OTHERS

    squeakDateSettings = <see $$.squeakDate.settings>
    datePartsToOmit = <{ [datePart]: <number> }>

    //                              ¬
    //
  TODO:
    Could this be useful to improve squeakDate: "http://isaaccambron.com/twix.js/#home"?
*/
$$.squeakDate = function (momentObjectsOrString, customSettings) {
  var fullSettings = $$.defaults($$.squeakDate.settings, customSettings);
  if (_.isString(momentObjectsOrString)) return fromString.makeSqueakDate(momentObjectsOrString, fullSettings)
  else return fromMoments.makeSqueakDate(momentObjectsOrString, fullSettings);
};

/**
  DESCRIPTION: create a squeakDate from the given string, moment object, or array of the previous
    see $$.squeakDate
    same but for multiple dates
  ARGUMENTS: (
    !momentObjectsOrString <
      |squeakDate_string
      |squeakDate_string[]
      |squeakDate_justMoments
    >,
    ?customSettings <squeakDateSettings> « custom settings to process date »,
  )
  RETURN: <squeakDate>
*/
$$.squeakDates = function (momentObjectsOrString, customSettings) {
  var fullSettings = $$.defaults($$.squeakDate.settings, customSettings);
  // moment objects
  if (_.isArray(momentObjectsOrString) && momentObjectsOrString[0] && momentObjectsOrString[0]._isAMomentObject) return fromMoments.makeSqueakDates(momentObjectsOrString, fullSettings)
  // string or list of strings
  else return fromString.makeSqueakDates(momentObjectsOrString, fullSettings);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SETTINGS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    default settings for $$.squeakDate
    you can modify these settings if you want all calls to $$.squeakDate and $$.squeakDates to have custom settings
    if you just want to modify settings for one run, use the customSettings option to pass the settings you want
  TYPE: <squeakDateSettings>
  TYPES:
    squeakDateSettings = <{

      dateComponents: <squeakDateComponents> « the parts of a date »,

      separators: <squeakDateSeparators> « separators between date parts, time parts, and between date and time »,

      partsOrder:
        <<"date"|"time">[]>
        @default=["date", "time"]
        « order of elements in a date (changing this allow to change dates formats) »,

      parseOrder:
        <datePart[]>
        @default=[ "year", "month", "day", "hours", "minutes", "seconds" ]
        « order in which to add parts of date in string »,

      momentParseParts:
        <datePart[]>
        @default=[ "year", "month", "day" ]
        « what parts should be parsed from a moment object »,

    }>

    squeakDateComponents = <squeakDateComponent[]>
    squeakDateComponent = <{
      !format: <string> « format of the date part in moment syntax (e.g. "YYYY" if you want full year) »,
      !isPartOf: <"date"|"time">,
      ?_recursiveOption: true,
    }>

    squeakDateSeparators = <{
      ["date"|"time"|"range"|"betweenParts"|"betweenDates"]: <string>
    }>

    datePart = <key of $$.squeakDate.settings.dateComponents>@default="year"|"month"|"day"|"hours"|"minutes"|"seconds"

*/
$$.squeakDate.settings = {

  dateComponents: {
    _recursiveOption: true,
    year: {
      _recursiveOption: true,
      format: "YYYY",
      isPartOf: "date",
    },
    month: {
      _recursiveOption: true,
      format: "MM",
      isPartOf: "date",
    },
    day: {
      _recursiveOption: true,
      format: "DD",
      isPartOf: "date",
    },
    hours: {
      _recursiveOption: true,
      format: "HH",
      isPartOf: "time",
    },
    minutes: {
      _recursiveOption: true,
      format: "mm",
      isPartOf: "time",
    },
    seconds: {
      _recursiveOption: true,
      format: "ss",
      isPartOf: "time",
    },
  },

  separators: {
    _recursiveOption: true,
    date: "-",
    time: ":",
    range: "#",
    betweenParts: "_",
    betweenDates: ",",
  },

  partsOrder: ["date", "time"],
  parseOrder: [ "year", "month", "day", "hours", "minutes", "seconds" ],

  momentParseParts: [ "year", "month", "day" ],

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$.squeakDate;
