var _ = require("underscore");
var $$ = require("../../core");
var moment = require("moment");
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE SQUEAK DATE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
  ARGUMENTS: (
    !dates <squeakDateOrRange_string[]>,
    !settings<squeakDateSettings>,
  )
  RETURN: <squeakDate>
*/
function makeSqueakDate (dates, settings) {

  return {

    //
    //                              CONSTANTS

    isSqueakDate: true,
    dates: dates,
    settings: settings,

    //
    //                              DATES COMMON PARTS

    /**
      DESCRIPTION: parts that all dates have in common
      ARGUMENTS: ( ø )
      RETURN: <datePartsToOmit>
    */
    getDatesCommonParts: function () {

      var commonParts = {};
      var dates = this.dates;
      var self = this;

      $$.breakableEach(self.settings.parseOrder, function (dateComponentName) {
        var dateComponentObject = self.settings.dateComponents[dateComponentName];
        return $$.breakableEach(dates, function (squeakDateOrRange) {
          var squeakDateOrRangeCommonValues = squeakDateOrRange.getDatesCommonParts();
          if (_.isUndefined(commonParts[dateComponentName]) && !_.isUndefined(squeakDateOrRangeCommonValues[dateComponentName])) commonParts[dateComponentName] = squeakDateOrRangeCommonValues[dateComponentName]
          else if (_.isUndefined(squeakDateOrRangeCommonValues[dateComponentName]) || commonParts[dateComponentName] != squeakDateOrRangeCommonValues[dateComponentName]) {
            delete commonParts[dateComponentName];
            return "break";
          };
        });
      });

      return commonParts;

    },

    //
    //                              GET STRING

    /**
      DESCRIPTION:
      ARGUMENTS: ( ø )
      RETURN: <squeakDate_string>
    */
    getString: function () {
      var datesCommonParts = this.getDatesCommonParts();
      return _.map(this.dates, function (squeakDateOrRange, i) {

        // for first date, do not omit parts (or just do not omit parts of the start date)
        if (i == 0) {
          if (squeakDateOrRange.isSqueakDateRange) return squeakDateOrRange.getString(datesCommonParts)
          else return squeakDateOrRange.getString();
        }
        // for other dates, omit all parts that are in common
        else return squeakDateOrRange.getString(datesCommonParts, true);

      }).join(this.settings.separators.betweenDates);
    },

    //                              ¬
    //

  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE SQUEAK DATE RANGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
  ARGUMENTS: (
    !start <squeakDateSingle>,
    !end <squeakDateSingle>,
    !settings<squeakDateSettings>,
  )
  RETURN: <squeakDateRange>
*/
function makeSqueakDateRange (start, end, settings) {
  return {

    //
    //                              CONSTANTS

    isSqueakDateRange: true,
    start: start,
    end: end,
    settings: settings,

    //
    //                              COMMON PARTS

    /**
      DESCRIPTION: get list of date components that are the same in start and end
      ARGUMENTS: ( ø )
      RETURN: <datePartsToOmit>
    */
    getDatesCommonParts: function () {
      var datesCommonParts = {};
      var self = this;
      $$.breakableEach(self.settings.parseOrder, function (dateComponentName) {
        var dateComponentObject = self.settings.dateComponents[dateComponentName];
        var isEqualInStartAndEnd = !_.isUndefined(self.start) && self.end && self.start[dateComponentName] == self.end[dateComponentName];
        if (isEqualInStartAndEnd) datesCommonParts[dateComponentName] = self.start[dateComponentName];
        else return "break";
      });
      return datesCommonParts;
    },

    //
    //                              DATES FORMATS

    /**
      DESCRIPTION: get start and end date formats
      ARGUMENTS: (
        ?datesCommonParts <datePartsToOmit> « if not defined, will be automatically gotten for this range's two dates »,
        ?alsoCustomFormatStart <boolean>@default=false « pass this if the first date in range should also have some parts omitted »
      )
      RETURN: <{ start: <string>, end: <string>, }>
    */
    getDatesFormats: function (datesCommonParts, alsoCustomFormatStart) {
      if (!datesCommonParts) datesCommonParts = this.getDatesCommonParts();
      return {
        start: this.start.getFormat(alsoCustomFormatStart ? datesCommonParts : undefined),
        end: this.end ? this.end.getFormat(datesCommonParts) : "",
      };
    },

    //
    //                              GET STRING

    /**
      DESCRIPTION:
      ARGUMENTS: (
        ?partsToOmit <datePartsToOmit>,
        ?alsoCustomFormatStart <boolean>@default=false « pass this if the first date in range should also have some parts omitted »
      )
      RETURN: <squeakDateRange_string>
    */
    getString: function (partsToOmit, alsoCustomFormatStart) {
      // figure out dates formats to use
      var datesFormats = this.getDatesFormats(partsToOmit, alsoCustomFormatStart);
      // make sure that end date is not empty (use last part of formats of first date if end format is empty)
      if (!datesFormats.end) datesFormats.end = $$.match(datesFormats.start, /[a-zA-Z0-9]*$/);
      // get start and end date strings
      var startDateString = this.start.getMoment().format(datesFormats.start);
      var endDateString = datesFormats.end ? this.end.getMoment().format(datesFormats.end) : "";
      // make full date strings
      return startDateString + this.settings.separators.range + endDateString;
    },

    //
    //                              GET SORTING

    getSorting: function () {
      if (this.start.getMoment()._isValid) return this.start.getMoment()._d
      else return new Date("9999-01-01"); // make sure to return a valid date, otherwise if sorting using _.sortBy, for example, it messes up the whole sorting
    },

    //                              ¬
    //

  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE SQUEAK DATE SINGLE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
  ARGUMENTS: (
    !squeakDateSinglePartial <{
      ?[datePart]: <number>,
      !hasTime: <boolean>
    }>,
    !settings<squeakDateSettings>,
  )
  RETURN: <squeakDateSingle>
*/
function makeSqueakDateSingle (squeakDateSinglePartial, settings) {
  return $$.defaults(squeakDateSinglePartial, {

    isSqueakDateSingle: true,
    settings: settings,
    hasDate: squeakDateSinglePartial.year || squeakDateSinglePartial.month || squeakDateSinglePartial.day ? true : false,
    hasTime: !_.isUndefined(squeakDateSinglePartial.hours) || !_.isUndefined(squeakDateSinglePartial.minutes) || !_.isUndefined(squeakDateSinglePartial.seconds) ? true : false,

    //
    //                              GET DATE FORMAT

    /**
      DESCRIPTION:
      ARGUMENTS: (
        !dateComponentObject <dateComponent>,
        ?previousDateComponentObject <dateComponent>,
      )
      RETURN: <string>
    */
    getSeparator: function (dateComponentObject, previousDateComponentObject) {
      if (!previousDateComponentObject) return ""
      else if (previousDateComponentObject.isPartOf != dateComponentObject.isPartOf) return this.settings.separators.betweenParts
      else return this.settings.separators[dateComponentObject.isPartOf];
    },

    /**
      DESCRIPTION: get the format of the date
      ARGUMENTS: ( ?componentsToOmit <datePartsToOmit> « if passed, will not iterate those components » )
      RETURN: <string>
    */
    getFormat: function (componentsToOmit) {
      var self = this;
      var format = "";

      // get list of date components to iterate to make the format (if asked omit some parts of the date)
      if (componentsToOmit) var components = _.difference(self.settings.parseOrder, _.keys(componentsToOmit))
      else var components = self.settings.parseOrder;

      var previousPart_dateComponentObject;
      $$.breakableEach(components, function (dateComponentName) {
        var dateComponentObject = self.settings.dateComponents[dateComponentName];
        if (!_.isUndefined(self[dateComponentName])) {
          // add this date component's format to format to return (with or without separator)
          var separator = self.getSeparator(dateComponentObject, previousPart_dateComponentObject);
          format += separator + dateComponentObject.format
          previousPart_dateComponentObject = dateComponentObject;
        }
        else "break";
      });

      return format;
    },

    //
    //                              COMMON PARTS

    /**
      DESCRIPTION: get list of date components that are the same in start and end
      ARGUMENTS: ( ø )
      RETURN: <datePartsToOmit>
    */
    getDatesCommonParts: function () {
      return _.clone(this);
    },

    //
    //                              GET DATE STRING

    /**
      DESCRIPTION:
      ARGUMENTS: ( ?partsToOmit <datePartsToOmit> )
      RETURN: <squeakDateSingle_string>
    */
    getString: function (partsToOmit) {
      return this.getMoment().format(this.getFormat(partsToOmit));
    },

    //
    //                              GET DATE MOMENT OBJECT

    /**
      DESCRIPTION:
      ARGUMENTS: ( ø )
      RETURN: <momentObject>
    */
    getMoment: function () {
      var dateObjectForMoment = _.clone(this);
      if (!_.isUndefined(dateObjectForMoment.month)) dateObjectForMoment.month--; // remove one to month because first month is 0
      return moment(dateObjectForMoment);
    },

    //
    //                              GET SORTING

    getSorting: function () {
      if (this.getMoment()._isValid) return this.getMoment()._d
      else return new Date("9999-01-01"); // make sure to return a valid date, otherwise if sorting using _.sortBy, for example, it messes up the whole sorting
    },

    //                              ¬
    //

  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {
  squeakDate: makeSqueakDate,
  squeakDateRange: makeSqueakDateRange,
  squeakDateSingle: makeSqueakDateSingle,
};
