var _ = require("underscore");
var $$ = require("../../core");
var maker = require("./maker");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var fromString = {

  /**
    DESCRIPTION:
    ARGUMENTS: (
      !squeakDateOrRangeString<squeakDateOrRange_string>,
      !settings<squeakDateSettings>,
    )
    RETURN: <squeakDateOrRange>
  */
  makeSqueakDate: function (squeakDateOrRangeString, settings) {
    this.settings = settings;
    return this.makeSqueakDateOrRange(squeakDateOrRangeString);
  },

  /**
    DESCRIPTION:
    ARGUMENTS: (
      !squeakDateStringOrArrayOfStrings<squeakDate_string|squeakDate_string[]>,
      !settings<squeakDateSettings>,
    )
    RETURN: <squeakDate>
  */
  makeSqueakDates: function (squeakDateStringOrArrayOfStrings, settings) {
    var self = this;
    self.settings = settings;
    var datesArray = _.isString(squeakDateStringOrArrayOfStrings) ? squeakDateStringOrArrayOfStrings.split(self.settings.separators.betweenDates) : squeakDateStringOrArrayOfStrings;

    // safety net
    datesArray = _.compact(datesArray);
    if (!datesArray.length) return undefined;

    // get first date object (useful later to fill missing parts of dates)
    var firstSqueakDateOrRange = self.makeSqueakDateOrRange(datesArray[0]);
    var firstSqueakDateSingle = firstSqueakDateOrRange.isSqueakDateRange ? firstSqueakDateOrRange.start : firstSqueakDateOrRange;

    // make dates
    var dates = _.map(datesArray, function (squeakDateOrRange, i) {
      return self.makeSqueakDateOrRange(squeakDateOrRange, firstSqueakDateSingle);
    });

    // make date object
    return maker.squeakDate(dates, this.settings);
  },

  /**
    DESCRIPTION:
    ARGUMENTS: (
      !squeakDateOrRangeString <squeakDateOrRange_string>,
      ?firstSqueakDateSingle <squeakDateSingle> « passed to fill parts that may be missing in string because common with the first date »,
    )
    RETURN: <squeakDateOrRange>
  */
  makeSqueakDateOrRange: function (squeakDateOrRangeString, firstSqueakDateSingle) {
    if (_.isUndefined(squeakDateOrRangeString)) return undefined
    else if (squeakDateOrRangeString.match(this.settings.separators.range)) return this.makeSqueakDateRange(squeakDateOrRangeString, firstSqueakDateSingle)
    else return this.makeSqueakDateSingle(squeakDateOrRangeString, firstSqueakDateSingle);
  },

  /**
    DESCRIPTION:
    ARGUMENTS: (
      !squeakDateRangeString<squeakDateRange_string>,
      ?firstSqueakDateSingle <squeakDateSingle> « passed to fill parts that may be missing in string because common with the first date »,
    )
    RETURN: <squeakDateRange>
  */
  makeSqueakDateRange: function (squeakDateRangeString, firstSqueakDateSingle) {

    // get range parts
    var startAndEnd = squeakDateRangeString.split(this.settings.separators.range);

    // make start date
    var start = this.makeSqueakDateSingle(startAndEnd[0], firstSqueakDateSingle);

    // make end date passing first date (start, or first date in whole dates list) as well in case there are omitted parts
    if (!firstSqueakDateSingle) firstSqueakDateSingle = start; // if no first date coming from other previous date, use this range start date
    var end = startAndEnd[1] == "" ? null : this.makeSqueakDateSingle(startAndEnd[1], firstSqueakDateSingle);

    // return squeakDateRange
    return maker.squeakDateRange(start, end, this.settings);

  },

  /**
    DESCRIPTION:
    ARGUMENTS: (
      !squeakDateSingleString<squeakDateSingle_string>,
      ?firstSqueakDateSingle <squeakDateSingle> « passed to fill parts that may be missing in string because common with the first date »,
    )
    RETURN: <squeakDateSingle>
  */
  makeSqueakDateSingle: function (squeakDateSingleString, firstSqueakDateSingle) {
    var s = this.settings;

    // make sure to be able to interpret properly dates like: 2012-12-27_12:01#15, the final 15 is part of time, and parsing would not figure this out without the following
    if (_.indexOf(partsOrder, "time") && firstSqueakDateSingle && firstSqueakDateSingle.hasTime && !squeakDateSingleString.match(s.separators.betweenParts)) var partsOrder = _.without(s.partsOrder, "date")
    // same than previous, covering the case where date would be writte  after time in the string
    else if (_.indexOf(partsOrder, "date") && firstSqueakDateSingle && firstSqueakDateSingle.hasDate && !squeakDateSingleString.match(s.separators.betweenParts)) var partsOrder = _.without(s.partsOrder, "time")
    else var partsOrder = s.partsOrder;

    // figure out date parts
    var dateIndex = _.indexOf(partsOrder, "date");
    var timeIndex = _.indexOf(partsOrder, "time");
    var dateANDtime = squeakDateSingleString.split(s.separators.betweenParts);
    var parts = {
      date: dateANDtime[dateIndex] ? dateANDtime[dateIndex].split(s.separators.date) : [],
      time: dateANDtime[timeIndex] ? dateANDtime[timeIndex].split(s.separators.time) : [],
    };

    // prepend firstSqueakDate's parts that are missing in current date
    var dateDecalage = firstSqueakDateSingle ? firstSqueakDateSingle.internal.dateParts.length - parts.date.length : 0;
    for (i = dateDecalage-1; i >= 0; i--) { parts.date.unshift(firstSqueakDateSingle.internal.dateParts[i]) };

    // prepend firstSqueakDate's parts that are missing in current date
    var timeDecalage = firstSqueakDateSingle ? firstSqueakDateSingle.internal.timeParts.length - parts.time.length : 0;
    for (i = timeDecalage-1; i >= 0; i--) { parts.time.unshift(firstSqueakDateSingle.internal.timeParts[i]) };

    // prepare date object
    var resultObjectDateParts = {
      internal: {
        dateParts: parts.date,
        timeParts: parts.time,
      },
    };

    // GET VALUES OF DATES PARTS
    // get arrays of names of date and time parts that should be gotten
    // TYPE: { date: <string>[], time: <string[]> }
    var parsePartsOrder = _.groupBy(s.parseOrder, function (partName) { return s.dateComponents[partName].isPartOf; });
    // for each date part (year|month|day|hours...), get the value in parts["date"|"time"]
    _.each(s.parseOrder, function (partName) {
      var dateOrTime = s.dateComponents[partName].isPartOf;
      resultObjectDateParts[partName] = +parts[dateOrTime][_.indexOf(parsePartsOrder[dateOrTime], partName)];
    });

    // remove keys with missing values
    var resultObject = _.omit(resultObjectDateParts, function (val) { return _.isNaN(val); });

    // return full squeakDateSingle object with methods
    return maker.squeakDateSingle(resultObject, this.settings);

  },

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// bind all methods context to object
_.each(fromString, function (methodFunc, methodName) {
  fromString[methodName] = _.bind(methodFunc, fromString);
});

module.exports = fromString;
