var _ = require("underscore");
var $$ = require("../../core");
var moment = require("moment");
var maker = require("./maker");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var fromMoments = {

  /**
    DESCRIPTION:
    ARGUMENTS: (
      !squeakDateOrRangeJustMoments <squeakDateOrRange_justMoments>,
      !settings<squeakDateSettings>,
    )
    RETURN: <squeakDateOrRange>
  */
  makeSqueakDate: function (squeakDateOrRangeJustMoments, settings) {
    this.settings = settings;
    return this.makeSqueakDateOrRange(squeakDateOrRangeJustMoments);
  },

  /**
    DESCRIPTION:
    ARGUMENTS: (
      !squeakDateJustMoments <squeakDate_justMoments>,
      !settings<squeakDateSettings>,
    )
    RETURN: <squeakDate>
  */
  makeSqueakDates: function (squeakDateJustMoments, settings) {
    var self = this;
    self.settings = settings;
    // make squeak dates
    var dates = _.map(_.isArray(squeakDateJustMoments) ? squeakDateJustMoments : [squeakDateJustMoments], function (squeakDateOrRange) {
      return self.makeSqueakDateOrRange(squeakDateOrRange);
    });
    // make date object
    return maker.squeakDate(dates, self.settings);
  },

  /**
    DESCRIPTION:
    ARGUMENTS: ( !squeakDateOrRangeJustMoments <squeakDateOrRange_justMoments>, )
    RETURN: <squeakDateOrRange>
  */
  makeSqueakDateOrRange: function (squeakDateOrRangeJustMoments) {
    if (_.isUndefined(squeakDateOrRangeJustMoments)) return undefined
    else if (squeakDateOrRangeJustMoments._isAMomentObject) return this.makeSqueakDateSingle(squeakDateOrRangeJustMoments)
    else return this.makeSqueakDateRange(squeakDateOrRangeJustMoments);
  },

  /**
    DESCRIPTION:
    ARGUMENTS: ( !squeakDateRangeJustMoments<squeakDateRange_justMoments>, )
    RETURN: <squeakDateRange>
  */
  makeSqueakDateRange: function (squeakDateRangeJustMoments) {

    // make start date
    var start = this.makeSqueakDateSingle(squeakDateRangeJustMoments.start);
    var end = _.isNull(squeakDateRangeJustMoments.end) ? null : this.makeSqueakDateSingle(squeakDateRangeJustMoments.end);

    // return squeakDateRange
    return maker.squeakDateRange(start, end, this.settings);

  },

  /**
    DESCRIPTION:
    ARGUMENTS: ( !squeakDateSingleJustMoment<momentObject>, )
    RETURN: <squeakDateSingle>
  */
  makeSqueakDateSingle: function (squeakDateSingleJustMoment) {

    var resultObjectDateParts = {};
    var self = this;

    // fill asked parts
    _.each(self.settings.momentParseParts, function (dateComponentName) {
      var dateComponentObject = self.settings.dateComponents[dateComponentName];
      resultObjectDateParts[dateComponentName] = +squeakDateSingleJustMoment.format(dateComponentObject.format);
    });

    // return full squeakDateSingle object with methods
    return maker.squeakDateSingle(resultObjectDateParts, this.settings);

  },

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// bind all methods context to object
_.each(fromMoments, function (methodFunc, methodName) {
  fromMoments[methodName] = _.bind(methodFunc, fromMoments);
});

module.exports = fromMoments;
