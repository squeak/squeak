var _ = require("underscore");
var $$ = require("../core");
require("../extension/array");
require("../extension/date");
require("../extension/object");
require("../extension/string");

$$.collection = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // return function result or entry[key]
  resultOrKey: function (entry, funcOrKey) {
    if (_.isString(funcOrKey)) return $$.getValue(entry, funcOrKey)
    else if (_.isFunction(funcOrKey)) return funcOrKey(entry)
    else return entry;
    // else console.log("error, func or key wrong type: ", funcOrKey, entry);
  },

  // get id from id, or from entry
  id: function (entryOrId, key) {
    return _.isObject(entryOrId) ? $$.getValue(entryOrId, key || "_id") : entryOrId;
  },

  // get revision number (just the number, not the whole string)
  revNumber: function (entry) {
    if (!entry._rev) console.log("can't find rev number")
    else return +entry._rev.split("-")[0];
  },

  // find entry in collection
  get: function (collection, entryOrId) { return _.findWhere(collection, { _id: $$.collection.id(entryOrId)  }) },

  /**
    DESCRIPTION: just like _.pluck, but with deep key ability
    ARGUMENTS: (
      collection <collection>,
      deepKey <string>,
    )
    RETURN: array
  */
  pluck: function(collection, deepKey){

    var array = [];

    _.each(collection, function(entry, entryIndex){
      array.push($$.getValue(entry, deepKey));
    });

    return array;

  },

  mapCouchGet: function (response) { return _.map(response, function(entry){ return entry.doc }) },

  /*
    DESCRIPTION: stringify an entry
    ARGUMENTS: (
      entry <object>,
      excludeKeys <null|array>,
    )
  */
  entryStringify: function (entry, excludeKeys) {
    // exclude keys
    if (_.isUndefined(excludeKeys)) excludeKeys = ["system"];
    entry = $$.object.removeKeys(entry, excludeKeys);
    // stringify
    return $$.json.pretty(entry);
  },

  //--------------------------------------------------------------------------------------------------------------------------------------------------
  //                                                  COMPARE DIFFERENCES BETWEEN TWO COLLECTIONS
  compare: function (col1, col2, nameOf1, nameOf2) {

    if (!nameOf1) nameOf1 = "1";
    if (!nameOf2) nameOf2 = "2";

    // result
    var result = {
      sameInBoth: [],
      conflict: [],
    }
    var onlyIn1 = result["onlyIn"+ nameOf1] = [];
    var onlyIn2 = result["onlyIn"+ nameOf2] = _.clone(col2); // starts with full collection 2
    var newerIn1 = result["newerIn"+ nameOf1] = [];
    var newerIn2 = result["newerIn"+ nameOf2] = [];

    // iterate first collection
    _.each(col1, function(entry){
      var entryInCol2 = $$.collection.get(col2, entry)
      if (!entryInCol2) onlyIn1.push(entry)
      else {
        if (entry._rev == entryInCol2._rev) result.sameInBoth.push(entry)
        else if ($$.collection.revNumber(entry) > $$.collection.revNumber(entryInCol2)) newerIn1.push(entry)
        else if ($$.collection.revNumber(entry) < $$.collection.revNumber(entryInCol2)) newerIn2.push(entry)
        else result.conflict.push(entry);
        // remove from result.onlyIn2 so only the one that are not in col1 will remain in it
        $$.collection.remove(onlyIn2, entry)
      };
    });

    return result;

  },
  //--------------------------------------------------------------------------------------------------------------------------------------------------
  //                                                  REMOVE ENTRY
  remove: function (array, entryOrEntryId, noAlertIfUnexistant) {
    var index = $$.collection.indexOf(array, entryOrEntryId)
    // remove entry from array
    if (index != -1) array.splice(index, 1)
    else if (noAlertIfUnexistant) return
    else console.log("Error removing entry from array!", " entry: ", entryOrEntryId, " array: ", array)
  },
  //--------------------------------------------------------------------------------------------------------------------------------------------------
  //                                                  GET ENTRY INDEX
  indexOf: function (array, entryOrEntryId) {
    var index = -1
    _.each(array, function (ent, entIndex) { if (ent._id == $$.collection.id(entryOrEntryId)) index = entIndex; })
    return index
  },
  //--------------------------------------------------------------------------------------------------------------------------------------------------
  //                                                  REFRESH ENTRY IN COLLECTION PLACING NEW ONE THERE
  entryRefresh: function (collection, entry) {
    var index = $$.collection.indexOf(collection, entry);
    if (index != -1) collection[index] = entry
    else console.log("Entry is not in collection, nothing done to collection.", entry, collection);
  },

  //--------------------------------------------------------------------------------------------------------------------------------------------------
  //                                                  GET ALL POSSIBLE ENTRIES FOR ONE KEY IN COLLECTION (THE ONE THAT HAVE BEEN INPUTED)
  getSuggestions: function(collection, keyKeysOrFuncs, entryValue){
    var possibleEntries = {};
    // iterate collection
    _.each(collection, function (entry) {
      // if multiple keys to lookup
      $$.each(keyKeysOrFuncs, function(keyOrFunc){
        // if not key but function provided
        if (_.isFunction(keyOrFunc)) $$.each(keyOrFunc(entry), function(result){ possibleEntries[result] = true });
        // iterate (or do on) entry[key]
        else {
          // change path.to.key.with.array.0.in.the.middle to path.to.key.with.array.[..].in.the.middle to match all array entries
          var keyChain = keyOrFunc.replace(/\.\d+/g, "[..]")
          // get possible entries finding values in entry
          $$.each($$.getValue(entry, keyChain), function (val) { // it was $$.each, with split, but if any coma it would cut entry, should not support comma sepparated but turn them into arrays
            // store values as keys on possibleEntries object
            possibleEntries[val] = true; // could do it with array and _.uniq() also
          });
        };
      });
    })
    // return all possible values
    var possibleEntriesArray = _.keys(possibleEntries).sort();
    // remove values already chosen
    if (entryValue) $$.array.removeByValue(possibleEntriesArray, entryValue);
    // return
    return possibleEntriesArray;
  },

  /**
    DESCRIPTION: transform an array or collection in an object
    ARGUMENTS: (
      collection <array>,
      key <string>
    )
    RETURN: <object>
  */
  objectify: function (collection, key) {
    var result = {};
    _.each(collection, function (entry){ result[entry[key]] = entry; });
    return result;
  },

  /**
    DESCRIPTION: filter entries from a collection that match searched pattern
    ARGUMENTS: (
      collection <collection>,
      search <string|regexp>,
      caseSensitive <boolean>
    )
    RETURN: collection « list of given matching entries »
  */
  search: function(collection, search, caseSensitive){
    return _.filter(collection, function(entry){ return $$.string.matchAnything(entry, search, caseSensitive) });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  HEADER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  header: {
    // sort by entry content
    entry: function (record, recordIndex, records, funcOrKey) {
      if (recordIndex && _.isEqual( $$.collection.resultOrKey(record, funcOrKey), $$.collection.resultOrKey(records[recordIndex-1], funcOrKey) )) return null
      else return $$.collection.resultOrKey(record, funcOrKey);
    },
    // sort by first letters
    initiale: function (record, recordIndex, records, funcOrKey) {
      return $$.collection.header.entry(record, recordIndex, records, function(entry){
        var result = $$.collection.resultOrKey(entry, funcOrKey);
        if (result) return result[0]
        else return null;
      })
    },
    // sort by months
    dateMonth: function (record, recordIndex, records) {
      if (!record) return record = {};
      var recordMonth = $$.date.getYM(record.date)
      if (recordIndex === 0) return recordMonth
      else {
        if (!records[recordIndex-1]) records[recordIndex-1] = {};
        var previousRecordMonth = $$.date.getYM(records[recordIndex-1].date)
        if (recordMonth != previousRecordMonth) return recordMonth
        else return null;
      };
    },
    // sort first by temp then by months
    tempAndDateMonth: function (record, recordIndex, records) {
      // first put apart temp entries
      if (record.temp) return $$.collection.header.entry(record, recordIndex, records, "temp") ? "temp" : null
      // first non temp entry, ignore previous entries
      else if (recordIndex && records[recordIndex-1].temp) return $$.collection.header.dateMonth(record, 0, records)
      // after cut by months
      else return $$.collection.header.dateMonth(record, recordIndex, records);
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SORT BY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: sort an array or collection by key or provided function (can provide as many keys or functions as desired)
      behaviour comment: ignores case
    ARGUMENTS: (
      collection <array|collection>,
      *<string|array|function> «string must be key of collection entry to compare | array of keys also works»
    )
    TODO: handle special characters (eg. greek letters, é à...)
  */
  sortBy: function(collection){
    var args = _.flatten(_.rest(arguments))
    var result = _.clone(collection);
    // make sure element asked to be sorted is an array
    if (!_.isArray(result)) return $$.log.returnLog(result, "You asked to sort a non array element.", result)
    else return result.sort(function(a,b){
      var sorter = {};
      var sorterString = {};
      _.each({a:a,b:b}, function(val,key){

        // get elements to sort by
        sorter[key] = _.map(args, function(arg){
          if (_.isFunction(arg)) return $$.result(arg, val)
          else if (_.isString(arg) && $$.isObjectOrArray(val)) return val[arg]
          else $$.log.error("$$.collection.sortBy only accept keyStrings and functions to sort array, what is this?: ", arg, " or maybe the entry analysed now is not an object or array, so it cannot have the asked key, is this an object or array?: ", val);
        });

        // to avoid capitales avant minuscules in sorting, everything in lower case
        sorter[key] = _.map(sorter[key], function(arg){
          // turn object into string
          if ($$.isObjectOrArray(arg)) arg = $$.json.short(arg);
          // everything in lower case
          if (_.isString(arg)) return arg.toLowerCase()
          else return arg;
        })

        // prepare sort strings to sort whole array of keys to compare at once
        sorterString[key] = _.compact(sorter[key]).join(",");
      });
      // sort whole array of keys to compare at once
      if (sorterString.a > sorterString.b) { return 1 }
      else if (sorterString.a < sorterString.b) { return -1 };
      // // sort itterating each individually
      // for (i in sorter.a) { // could as well be sorter.b, they are simmetrical
      //   if (sorter.a[i] > sorter.b[i]) { return 1 }
      //   else if (sorter.a[i] < sorter.b[i]) { return -1 };
      //   // else if (sorter.a[i] === sorter.b[i]) // continue to itterate over next comaprison
      // };
    })
  },

  /**
    DESCRIPTION: sort a collection by date
    ARGUMENTS: (
      !collection,
      ?keyOrFunc <undefined|string|function(entry):<dateString|unefined>> «
        - if undefined, entry.date should contain a dateString
        - if keyOrFunc is string, entry[keyOrFunc] should contain a dateString
        - if it's a function, the function should return a dateString
      »,
    TYPES:
      dateString: see $$.date.make
    )
  */
  sortByDate: function (collection, keyOrFunc) {
    return _.sortBy(collection, function (entry) {

      var date = $$.getValueOrResult(entry, keyOrFunc || "date");
      date = $$.date.make(date, true);

      // use the resulting Date object to sort by
      return date._d;

    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$.collection;
