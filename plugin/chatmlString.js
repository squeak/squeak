var _ = require("underscore");
var $$ = require("../core");
require("../extension/string");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    convert to classic html a chatml string
    it makes the following replacements:
      "\n"                            =>  '<br>'
      "\t", "\\t", "<tab>", "<tab/>"  =>  '<span class="chatml-tab"></span>'
      "<important>"                   =>  '<span class="chatml-important">'
      "</important>"                  =>  '<span>'
      "<secondary>"                   =>  '<span class="chatml-secondary">'
      "</secondary>"                  =>  '<span>'
  ARGUMENTS: (
    !string <string>,
    ?options <see $$.string.replaceMulti·options}>@default={
      newLine: {
        _recursiveOption: true,
        regexp: [/\n/g],
        replacement: "<br>",
      },
      tab: {
        _recursiveOption: true,
        regexp: [/\t/g, /\\t/g, /\<tab\>/g, /\<tab\/\>/g],
        replacement: '<span class="chatml-tab"></span>',
        // replacement: "&nbsp&nbsp&nbsp&nbsp",
      },
      important_in: {
        _recursiveOption: true,
        regexp: [/\<important\>/g],
        replacement: '<span class="chatml-important">',
      },
      important_out: {
        _recursiveOption: true,
        regexp: [/\<\/important\>/g],
        replacement: '</span>',
      },
      secondary_in: {
        _recursiveOption: true,
        regexp: [/\<secondary\>/g],
        replacement: '<span class="chatml-secondary">',
      },
      secondary_out: {
        _recursiveOption: true,
        regexp: [/\<\/secondary\>/g],
        replacement: '</span>',
      },
    }
  )
  RETURN: <string>
*/
$$.chatmlString = function (string, options) {

  var defaultOptions = {
    newLine: {
      _recursiveOption: true,
      regexp: [/\n/g],
      replacement: "<br>",
    },
    tab: {
      _recursiveOption: true,
      regexp: [/\t/g, /\\t/g, /\<tab\>/g, /\<tab\/\>/g],
      replacement: '<span class="chatml-tab"></span>',
      // replacement: "&nbsp&nbsp&nbsp&nbsp",
    },
    important_in: {
      _recursiveOption: true,
      regexp: [/\<important\>/g],
      replacement: '<span class="chatml-important">',
    },
    important_out: {
      _recursiveOption: true,
      regexp: [/\<\/important\>/g],
      replacement: '</span>',
    },
    secondary_in: {
      _recursiveOption: true,
      regexp: [/\<secondary\>/g],
      replacement: '<span class="chatml-secondary">',
    },
    secondary_out: {
      _recursiveOption: true,
      regexp: [/\<\/secondary\>/g],
      replacement: '</span>',
    },
  }

  return $$.string.replaceMulti(string, $$.defaults(defaultOptions, options))

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = $$.chatmlString;
