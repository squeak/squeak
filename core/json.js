var _ = require("underscore");
var $$ = require("./_");

$$.json = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: just same as JSON.stringify(value)
    ARGUMENTS: ( value <any> )
    RETURN: json string
  */
  short: function (value) {
    return JSON.stringify(value);
  },

  /**
    DESCRIPTION: improved version of JSON.stringify(object, null, "\t")
    ARGUMENTS: (
      object <any>,
      prettier <boolean> « if true arrays without children arrays or objects won't be internally indented, so it will take less useless space »
    )
    RETURN: json string
    EXAMPLES:
      $$.json.pretty({
        subObject: {
          a: 1,
          b: 2,
        },
        subArray: [1, 2, 3],
        text: "some text",
      }, true);
      // > {
      // >   "subObject": {
      // >     "a": 1,
      // >     "b": 2
      // >   },
      // >   "subArray": [1, 2, 3], // notice that the array is on a single line, this is because "prettier" option is activated
      // >   "text": "some text"
      // > }
  */
  pretty: function (object, prettier) {
    // var string = JSON.stringify(object, null, 2); // 2 spaces instead of tab
    var string = JSON.stringify(object, null, "\t");
    // contract arrays containing no children array or objects, which would otherwise take much space
    if (prettier) string = string.replace(/\[\n[^\]\[\{\}]*\]/g, function (match, offset, fullString) {
      // wrapped in try/catch in case it was matching something inside a string, so it does not fail
      try {
        return JSON.stringify(JSON.parse(match));
      }
      catch (err) {
        $$.log.error("[$$.json.pretty] Failed to parse matched array as JSON object. The matched string was not modified.", err, match, object);
        return match;
      };
    });
    return string;
  },

  /**
    DESCRIPTION: copy the given object stringified in JSON
    ARGUMENTS: ( object <any compatible with JSON.stringify> )
    RETURN: <void>
    EXAMPLES:
      $$.json.copy([ "some", "array", "to", "copy" ]); // will propose to copy '[ "some", "array", "to", "copy" ]' to clipboard
  */
  copy: function (object) {
    prompt("Copy object json?", json.pretty(object))
  },

  /**
    DESCRIPTION: if provided value is JSON string, parse it, otherwise just return it
    ARGUMENTS: (
      ?string <string>,
      ?callbackOnError <function(err):<any>> «
        if defined, this callback will be executed if the passed string is not proper json,
        if this callback returns a value, this value will be returned by $$.json.parseIfJson
      »,
    )
    RETURN: any
    EXAMPLES:
      $$.json.parseIfJson('{ "key1": 1, "key2": [1, 2, 3] }'); // { key1: 1, key2: [1, 2, 3] }
      $$.json.parseIfJson('{ "key1": 1, "this is not a valid json string" }'); // '{ "key1": 1, "this is not a valid json string" }'
      $$.json.parseIfJson('{ "key1": 1, "this is not a valid json string" }', function (err) { return "invalid string"; }); // "invalid string"
  */
  parseIfJson: function (val, callbackOnError) {
    try { return JSON.parse(val); }
    catch (e) {
      if (callbackOnError) return callbackOnError(e) || val
      else return val;
    }
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$;
