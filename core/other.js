var _ = require("underscore");
var $$ = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


/**
  DESCRIPTION: create a random uuid
  ARGUMENTS: ( ø )
  RETURN: <string>
*/
$$.uuid = function () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
};


// eachEverything: function (anything, func) {
//   if (!anything) var iterate = []
//   else if (_.isString(anything)) var iterate = [anything]
//   else if (_.isObject(anything)) var iterate = anything // beware, this is also valid for functions so if functions become important to manage, change this
//   else if (_.isBoolean(anything)) var iterate = [anything]
//   else var iterate = anything;
//   return _.each(iterate, function(d,i){ func(d,i); });
// },

/**
  DESCRIPTION: _.each that will wrap non array or object in an array to iterate
  ARGUMENTS: (
    target <any>,
    func <function>,
    options {
      split: <false|regexp>@default=false « if target is string, what to use to split it »,
      iterateObjects: <boolean>@default=true,
      iterateArrays: <boolean>@default=true,
    }
  )
  RETURN: <array>
  EXAMPLES:
    $$.each("my, list, of, things, as string", function (val) { console.log(val) }, { split: /\s,\s/, });
    // "my"
    // "list"
    // "of"
    // "things"
    // "as string"
*/
$$.each = function (target, func, options) {

  if (typeof target == "undefined") target = [];

  //
  //                              SET DEFAULT OPTIONS

  var defaultOptions = {
    split: false,
    iterateObjects: true,
    iterateArrays: true,
    // split: /\s*,\s*/,
    // split: /\s?,\s?/,
    // forceArray: false,
  };
  options = $$.defaults(defaultOptions, options);

  //
  //                              MAKE ELEMENT THE PROPER TYPE

  if ((options.iterateObjects && $$.isObjectLiteral(target)) || (options.iterateArrays && _.isArray(target))) var iterate = target
  else {
    if (options.split && _.isString(target)) var iterate = target.split(options.split) // split string if asked (by default no)
    else var iterate = [target];
  };

  //
  //                              ITERATE

  return _.each(iterate, function(d,i){
    // don't do it for recursiveOption
    if (i != "_recursiveOption") func(d,i);
  });

};


/**
  DESCRIPTION: just an iteration system like _.each, that can be stopped in the middle
  ARGUMENTS: (
    array <any[]> « array to iterate over »,
    func <function(<any>, <number|string>):<void|"break">> « function to execute on every iterated value, if function returns "break", will stop iterating »
  )
  RETURN: <any[]|"break">
  EXAMPLES:
    $$.breakableEach(
      ["will", "stop", "iterating", "!!! HERE !!!", "and", "this", "won't", "be logged"],
      function (val) {
        console.log(val)
        if (val == "!!! HERE !!!") return "break";
      }
    );
    // "will"
    // "stop"
    // "iterating"
    // "!!! HERE !!!"
*/
$$.breakableEach = function (array, func) {
  var brokenOrNot = false;
  for (key in array) {
    breakOrNot = func(array[key], key);
    if (breakOrNot == "break") return "break";
  };
  return array;
};


/**
  DESCRIPTION:
    similar to _.compact, but can freely choose which entries should be removed, and it can be recursive
    comparison is done with _.isEqual
    if the object passed is neither an object nor an array, nothing will be done and the passed value will be returned
  ARGUMENTS: (
    !object <object|array|any> « if any, will just do nothing »,
    ?entriesToRemove <array>@default=[ undefined, null, {}, [], NaN, "" ],
    ?recursive <boolean>,
  )
  RETURN: <array|object>
  EXAMPLES:
    $$.compact(["my", {}, "cleaned", undefined, null, [], {}, "array", NaN, ""]); // ["my", "cleaned", "array"]
*/
$$.compact = function (object, entriesToRemove, recursive) {
  if (!$$.isObjectLiteral(object) && !_.isArray(object)) return object;
  if (!entriesToRemove) entriesToRemove = [ undefined, null, {}, [], NaN, "" ];
  // make clone result
  var result = _.isArray(object) ? [] : {};
  // check if keys
  _.each(object, function (entry, key) {
    // for each entries to remove, test if equal and set it to not be added if it is
    var shouldBeRemoved = false;
    _.each(entriesToRemove, function (entryToRemove) { if (_.isEqual(entry, entryToRemove)) shouldBeRemoved = true; });
    if (!shouldBeRemoved) {
      if (_.isArray(object)) {
        result.push(entry)
        // make sure current key is right for recursive
        key = result.length-1;
      }
      else result[key] = entry;
    };
    // recurse if asked
    if (recursive && $$.isObjectOrArray(result[key])) result[key] = $$.compact(result[key], entriesToRemove, recursive);
  });
  return result;
};


/**
  DESCRIPTION:
    mixing _.uniq with JSON.stringify to compare objects also
    ⚠ limitation: two entries can be different, but stringified, they could become equal (like an array and stringified version of it)
  ARGUMENTS: ( array <array> )
  RESULT: <array>
*/
$$.uniq = function (array) {
  return _.uniq(array, function (entry) {
    if (_.isObject(entry)) return $$.json.short(entry)
    else return entry;
  });
};


/**
  DESCRIPTION:
    match something in a string, and return it, or null
    this is to avoid having to do `string.match(/myregexp/)[0]` which would fail if there is no match
  ARGUMENTS: (
    !string <string|any> « if not a string, will return null »,
    !matcher <regexp|string|[string<match>, string«flags»]>,
  )
  RETURN: <null|string>
  EXAMPLES:
    $$.match("string to find matches in", /in$/); // "in"
*/
$$.match = function (string, matcher) {

  if (!_.isString(string)) return null
  else {

    // make matcher a regexp if it's not
    if (_.isString(matcher)) matcher = new RegExp (matcher)
    else if (_.isArray(matcher)) matcher = new RegExp (matcher[0], matcher[1]);
    // match
    var isMatch = string.match(matcher);

    // return null if no match
    if (!isMatch) return null
    // otherwise return firt match
    else return isMatch[0];

  };

};


/**
  DESCRIPTION: same as string.replace but with easy matcher regexp creation and security if passed value is not a string
  ARGUMENTS: (
    !string <string|any> « if any will be stringified with 'any +""' »,
    !matcher <regexp|string|[string<match>, string«flags e.g. "gm" "g"»]>,
    !replaceBy <string>,
  )
  RETURN: <string>
*/
$$.replace = function (string, matcher, replaceBy) {

  // make sure string is a string
  if (!_.isString(string)) string = string + "";

  // make matcher a regexp if it's not
  if (_.isString(matcher)) matcher = new RegExp (matcher)
  else if (_.isArray(matcher)) matcher = new RegExp (matcher[0], matcher[1]);

  // replace
  return string.replace(matcher, replaceBy)

};


/**
  DESCRIPTION: open link in current or new window or tab
  ARGUMENTS: (
    !url <string> «
      the url to open
      you may also pass an object here, to build a url from it, but for that you will have to import $$.url extension
    »,
    ?newTab <boolean> « NOTE: with a click event, you can pass evt.ctrlKey, it will open in new tab only if ctrl key was down »,
  )
  RETURN: <void>
*/
$$.open = function (url, newTab) {
  if (_.isObject(url)) {
    if ($$.url) url = $$.url.make(url)
    else return $$.log.error("You must import the extension $$.url as well to pass an object as url.");
  };
  // this will open in new window (or in external browser for ionic app)
  if (newTab) window.open(url)
  // this will open in same window
  else window.open(url, '_self');
};


/**
  DESCRIPTION: like _.findWhere but uses _.isEqual to compare so it can handle finding objects
  ARGUMENTS: (
    !collection <collection>,
    !searchedEntry <object>,
  )
  RETURN: <array>
  EXAMPLES:
    $$.findWhere(
      [
        { name: "Miki", plant: { name: "basil", color: "green" }},
        { name: "Ike", plant: { name: "mint", color: "green" }}
      ],
      { plant: { name: "basil", color: "green" }
    });
    // { name: "Miki", plant: { name: "basil", color: "green" }}
*/
$$.findWhere = function (collection, searchedEntry) {
  return _.find(collection, function (entry) {
    var match = true;
    var searchedKeys = _.keys(searchedEntry);
    if (searchedKeys.length) {
      _.each(searchedKeys, function (key) {
        if (!_.isEqual(entry[key], searchedEntry[key])) match = false;
      });
      return match;
    }
    else return _.isEqual(entry, searchedEntry);
  });
};


/**
  DESCRIPTION: make it easy to roung a number, with a method that actualy works, without weird floating point bugs
  ARGUMENTS: (
    ?number <number> « the number to round »,
    ?precision <integer|string> «
      if precision is an integer, the higher this integer is, the more precise the number will be (see examples for details)
      if precision is a string, the number will be rounded the same way than the number in the precision string look like (if you wonder why using a string for this case, the main reason is that if you passed 1.00 as precision, javascript would directly turn it into 1, and you'd lose totally the purpose of this function, so the string approach is a good way to avoid confusion)
    »,
  )
  RETURN: <number>
  EXAMPLES:
    $$.round(1.23297489, 2); // 1.23
    $$.round(1.235, 2); // 1.24
    $$.round(1235, -1); // 1240
    $$.round(1235, "10"); // 1240
    $$.round(1.2356, "0.001"); // 1.236 passing 0.001 and true is like passing 3 as precision since there is three numbers after the point in given precision value
    $$.round(1.2356, "0.008"); // 1.236 exactly the same as previous one
    $$.round(1.2356, "0.000"); // 1.236 exactly the same as previous one
    $$.round(1.2356, "X.XX"); // 1.24 exactly the same as previous one
*/
$$.round = function (number, precision) {

  if (_.isUndefined(precision)) precision = 1;

  // precision is just an example of appropriately precised number
  if (_.isString(precision)) {
    var precisionMaker = precision.split(".");
    // precision is smaller than 0, count how many decimals
    if (!_.isUndefined(precisionMaker[1])) precision = precisionMaker[1].length
    // precision is higher than 0, count how many zeros at end of the number
    else precision = -$$.match(precisionMaker[0], /0*$/).length;
  };

  return Math.round(
    Math.round( number * Math.pow(10, precision) * 10 ) / 10
  ) / Math.pow(10, precision);

};


/**
  DESCRIPTION: copy the passed text to clipboard
  ARGUMENTS: ( string <string> )
  RETURN: <void>
*/
$$.copyToClipboard = function (string) {
  var elem = document.createElement("textarea");
  elem.value = string;
  elem.setAttribute("readonly", "");
  elem.style.position = "absolute";
  elem.style.left = "-99999px";
  document.body.appendChild(elem);
  elem.select();
  document.execCommand("copy");
  document.body.removeChild(elem);
};


/**
  DESCRIPTION:
    return the first passed argument that is not undefined
    this is useful if you want to do something like var x = a || b || c; but you want null, 0, "", NaN, false values to be considered as proper values to be returned
  ARGUMENTS: ( ... <any> )
  RETURN: <any>
  EXAMPLES:
    $$.any(1, 2, 3);                    // 1
    $$.any(0, 2, 3);                    // 0
    $$.any("", "something");            // ""
    $$.any(undefined, "", "something"); // ""
    $$.any(undefined, undefined);       // undefined
*/
$$.any = function () {
  for (var i = 0; i < arguments.length; i++) {
    if (!_.isUndefined(arguments[i])) return arguments[i];
  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
