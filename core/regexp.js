var _ = require("underscore");
var $$ = require("./_");

$$.regexp = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


  /**
    DESCRIPTION: escape characters that are special in a regexp, so the string can be passed to new RegExp
    ARGUMENTS: ( <string> )
    RETURN: <string>
    EXAMPLES:
      $$.regexp.escape("^hello.*$"); // "\\^hello\\.\\*\\$"
  */
  escape: function (string) {
    if (!string) return "";
    // list of characters to escape
    var escapeCharacters = ["*", "+", ".", "$", "^", "[", "]", "(", ")", "{", "}", "?", "!", "=", "|", "<", ">", "&"]; // I think there is pretty much everything, maybe even some are not necessary
    // for each of them check if string contains them
    _.each(escapeCharacters, function(char){
      var charMatch = new RegExp ("\\"+ char, "g"); // global flag for multi match
      // escape custom character if it's present in string
      string = string.replace(charMatch, "\\"+ char);
    });
    // return escaped string
    return string;
  },


  /**
    DESCRIPTION: make a new regular expression from a string, escaping special characters
    ARGUMENTS: (
      string <string> «
        this can be any kind of string or
        a string looking like a regepx (e.g."/match/gm")
      »,
      opts <"g"|"m"|"i"|"gi"|"gm"|"gmi"> « regexp options »,
    )
    RETURN: <regexp>
    EXAMPLES:
      $$.regexp.make("^hello.*$");       // /\^hello\.\*\$/
      $$.regexp.make("^hello.*$", "gi"); // /\^hello\.\*\$/gi
      $$.regexp.make("/^hello.*$/");     // /^hello.*$/
  */
  make: function (string, opts) {

    // is a regexp like string, turn it into a real regexp
    if ($$.isRegExpString(string)) {
      var searchRegexp = string.replace(/^\//, "");
      opts = $$.match(searchRegexp, /[^\/]*$/);
      searchRegexp = searchRegexp.replace(/[^\/]*$/, "").replace(/\/$/, "");
      return new RegExp(searchRegexp, opts || "");
    }

    // is already a regexp
    else if (_.isRegExp(string)) return string

    // make regexp from string
    else return new RegExp ($$.regexp.escape(string), opts || "");

  },


  /**
    DESCRIPTION:
      make sure that a string used to replace a patter with .replace() has enought dolars ;)
      the point is that "$$" in a replace is interpreted as "$" so you need "$$$" to actually insert "$"
    ARGUMENTS: ( !string <string> )
    RETURN: <string>
  */
  dolarEscape: function (string) {
    return string.replace(/\$\$/g, "$$$$$");
  },

  /**
    DESCRIPTION: return a regexp to match a url
    ARGUMENTS: ( flags <string> « g, i, m regexp flags » )
    RETURN: <regexp>
  */
  urlMatcher: function (flags) {
    var regexp = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
    return new RegExp(regexp.source, flags);
  },


  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$;
