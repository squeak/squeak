var _ = require("underscore");
var $$ = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DEFAULTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: get full options (with default values), merge two objects prefering the value of one over the other when same keys specified
  ARGUMENTS: (
    ?options,
    ?defaults,
  ){ @this=<systemOptions> « see systemOptions type in $$.defaults types » }
  RETURN: <object>
*/
function defaultsInternal (options, defaults) {

  // system options
  var systemOptions = this;

  // make sure defaults and options are iterable
  if (!defaults) var defaults = {};
  if (!options)  var options = {};

  // create result object
  var OPTIONS = {};

  // list all keys that should be created in new options object
  var allDefaultsAndOptionsKeys = _.chain([
    _.keys(defaults),
    _.keys(options)
  ])
    .flatten()
    .uniq()
    .value()
  ;

  // iterate all keys
  allDefaultsAndOptionsKeys.forEach(function (key) {

    //
    //                              RECURSE OBJECTS IF ASKED
    // = recursion is specified in defaults[key] or options[key] object with "_recursiveOption = true"
    // = or with recurseEverything system option

    if (
      ( $$.isObjectOrArray(defaults[key]) || $$.isObjectOrArray(options[key]) )
      && (
        systemOptions.recurseEverything
        || (defaults[key] && defaults[key]._recursiveOption)
        || (options[key] && options[key]._recursiveOption)
      )
    ) OPTIONS[key] = defaultsInternal.call(systemOptions, options[key], defaults[key])

    //
    //                              QUEUE FUNCTIONS IF ASKED
    // = queueFunctions system option is set to true

    else if (
      systemOptions.queueFunctions
      && _.isFunction(defaults[key])
      && _.isFunction(options[key])
    ) OPTIONS[key] = $$.queue(defaults[key], options[key])

    //
    //                              POPULATE VALUES

    else {
      if (_.has(options, key)) OPTIONS[key] = options[key]
      else OPTIONS[key] = defaults[key];
    };

    //                              ¬
    //

  });

  // return full options
  return OPTIONS;

}

/**
  DESCRIPTION:
    get full options (with default values), merge two objects prefering the value of one over the other when same keys are specified
    this is like what Object.assign does, but a bit more advanced
    if there are some suboptions that should recursely get default values assigned, set "_recursiveOption: true" to the subobject (see examples)
  ARGUMENTS: (
    ?...args <any> « if any argument is not an object or array, it will just be ignored »
  ){ @this=<systemOptions> « this is optional » }
  RETURN: <object>
  TYPES:
    systemOptions = {
      ?queueFunctions: <boolean>@default=false « if true, if a key is defined in default options and options, queue the new function to the default one instead of replacing the default (see $$.queue for details) »,
      ?recurseEverything: <boolean>@default=false « if true, will recurse all objects and arrays »,
    }
  NOTE:
    a method to (kind of) revert the action of $$.defaults is available in "extension/object"
    see $$.object.removeDefaultValues for more details
  EXAMPLES:
    $$.defaults(
      {
        a: 1,
        b: 2,
        c: {
          d: 3,
          e: 4,
        },
        f: {
          _recursiveOption: true,
          g: 5,
          h: 6,
        },
      },

      {
        a: "a was replaced",
        c: {
          "was": "replaced first but",
        },
      },

      {
        c: {
          then: "c was replaced again",
          by: "this",
        },
        f: {
          _recursiveOption: true,
          while: "f is getting more",
          and: "more",
        },
      },

      {
        f: {
          _recursiveOption: true,
          and: "more and more and",
          more: "values set and one replaced",
          g: "I was replaced, I'm not 5 anymore",
        },
      },
    );
    // > {
    // >   a: "a was replaced",
    // >   b: 2,
    // >   c: {
    // >     then: "c was replaced again",
    // >     by: "this",
    // >   },
    // >   f: {
    // >     _recursiveOption: true,
    // >     g: "I was replaced, I'm not 5 anymore",
    // >     h: 6,
    // >     while: "f is getting more",
    // >     and: "more and more and",
    // >     more: "values set and one replaced",
    // >   },
    // > }
*/
$$.defaults = $$.scopeFunction({

  /**
    alias of $$.defaults
  */
  main: function () {
    var systemOptions = this;
    var resultingOptions = arguments[0];
    _.each(_.rest(arguments), function (options) {
      // get defaults if it exists and is object
      if (_.isObject(options)) resultingOptions = defaultsInternal.call(systemOptions, options, resultingOptions)
      else if (options) console.error("Error getting defaults from non-object element", resultingOptions, options);
    });
    return resultingOptions;
  },

  /**
    DESCRIPTION:
      apply defaults, but if there are functions both in default and target, queue them
      shortcut for passing `queueFunctions: true` option to $$.defaults
    ARGUMENTS: ( see $$.defaults )
    RETURN: <object>
    EXAMPLES:
      let foo = 0;
      var bar = $$.defaults.queueFunctions(
        {
          a: 1,
          b: 2,
          incrementFoo: function () { foo++ },
        },
        {
          a: 2,
          incrementFoo: function () { foo++ },
        },
      ); // { a: 2, b: 2, incrementFoo: function () { ... this function executes boths incrementFoo functions in order }, }
      bar.incrementFoo(); // so now foo is 2
  */
  queueFunctions: function(){
    return $$.defaults.apply($$.defaults({ queueFunctions: true, }, this), arguments);
  },

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CLONE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: same as _.clone, but will recusively clone to the requested depth (= will also clone subobjects)
  ARGUMENTS: (
    !object <object>,
    ?level <integer>@default=1 « 1 means it doesn't recurse, 2 means it clones also the first level of subobjects... »,
  )
  RETURN: <object>
*/
$$.clone = function (object, level) {
  var newObject = _.clone(object);
  // do it for sub objects as deep as asked
  if (level > 1) {
    _.each(newObject, function(d,i){
      // for each subobject create clone
      if ($$.isObjectOrArray(d)) newObject[i] = $$.clone(newObject[i], level-1)
      // for other elements, just copy
      else newObject[i] = newObject[i];
    });
  };
  // return new object
  return newObject;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PLUCK
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: just like _.pluck, but accepting deep keys
  ARGUMENTS: (
    collection « collection to pluck »,
    deepKey <deepKey> « this.is.a.deep.key »,
  )
  RETURN: <any>
  EXAMPLES:
    $$.pluck([
      { name: "Abraham", deep: { key: "foo", }}
      { name: "Abraham", deep: { key: "bar", }}
    ], "deep.key"); // ["foo", "bar"]
*/
$$.pluck = function (collection, deepKey) {
  return _.map(collection, function(entry){
    return $$.getValue(entry, deepKey);
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MANDATORY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: verify that the given list of key (values) are defined and appropriate (if it's not the case, will log errors)
  ARGUMENTS: (
    object <object>,
    mandatoryKeys <{
      [keyName]: <function(object[keyName]):<boolean>> « each function will executed on object, and if some return false, some alerts will be raised »
    }>,
  )
  RETURN: <object>
*/
$$.mandatory = function (object, mandatoryKeys) {
  return _.each(mandatoryKeys, function (mandatoryKeyCheck, mandatoryKeyName) {
    if (!mandatoryKeyCheck(object[mandatoryKeyName])) $$.log.detailedError(
      "$$.mandatory",
      "Mandatory key '"+ mandatoryKeyName +"' wasn't properly set in:",
      object
    );
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
