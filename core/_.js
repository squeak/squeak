
var _ = require("underscore");

//
//                              create main function and scopeFunction because it's useful as basis to create other things

var $$ = (function () {

  // create $$ function
  function main (options) { };

  /**
    DESCRIPTION: create a function and set it's keys
    ARGUMENTS: ( object )
    RETURN: <function>
    EXAMPLES:
      let funcWithMethods = $$.scopeFunction({
        main: function () {
          // this will be executed when you run funcWithMethods()
        },
        someMethod: function () {
          // this will be executed when you run funcWithMethods.someMethod()
        },
      });
  */
  main.scopeFunction = function (object) {
    return (function () {
      // create main function if it doesn't exist
      if (!object.main) object.main = function(){ console.log("no main function for this scopeFunction"); };
      // add all object keys to the main function that will be returned
      _.each(object, function (value, key) { object.main[key] = value; });
      // return main function
      return object.main;
    })();
  };

  // add main to main function
  main.main = main;

  return main;

})();

//
//                              DEFINE IF RUNNING IN NODE OR BROWSER

/**
  DESCRIPTION: this variable value is "node" if running in node and "browser" if running in the browser
  TYPE: <"node"|"browser">
*/
$$.squeakInstanceRunningIn = typeof window === 'undefined' ? "node" : "browser";

/**
  DESCRIPTION: is true if squeak is ran in browser, else false
  TYPE: <boolean>
*/
$$.isBrowser = $$.squeakInstanceRunningIn === "browser" ? true : false;

/**
  DESCRIPTION: is true if squeak is ran in node, else false
  TYPE: <boolean>
*/
$$.isNode = $$.squeakInstanceRunningIn === "node" ? true : false;

//                              ¬
//

module.exports = $$;
