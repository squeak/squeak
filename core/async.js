var _ = require("underscore");
var $$ = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ASYNC EACH
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    a pretty elaborate function to allow callback after all asynchronous requests ending of iterated object
    not so useful, using the node library called "async" is more powerful
  ARGUMENTS: (
    !array <array|object> « array to iterate over »,
    !iterationFunc <function(
      value <any>,
      key <string|number>,
      asyncIteration <function(ø)> « MUST be called in iterationFunc »,
      iterations <{
        sync: <integer> « synchronous iterations counter »,
        async: <integer> « synchronous iterations counter »,
        break: <boolean> « if iteration broken by iterationFunc »,
        syncIsDone: <function (ø) : boolean> « test it synchronous iterations are finished »,
        finalCallbackCheck: <function(type<"async"|"sync">)> « test if final callback should be executed and execute it »,
        executedCallbackType: <undefined|"async"|"sync"> « who executed final callback, defined when done »,
        asyncIteration: <function(ø)> « increase iterations.async counter and execute iterations.finalCallbackCheck »,
      }> « internal object of asyncEach function »,
    ):<void|"break">> «
      - the iteration function MUST execute asyncIteration() in it's body, where the async iteration has been complete
      - return "break" in iterationFunc to stop iterating (it will still execute final callback)
    »
    !finalCallback <function(ø)>,
  )
  RETURN: <void>
*/
$$.asyncEach = function(array, iterationFunc, finalCallback){

  var iterations = {

    // iterations
    sync: 0,
    async: 0,
    // iteration breaking
    break: false,

    // executedCallbackType: <"sync"|"async"> // defined by finalCallbackCheck

    // synchronous iterations increment finished
    syncIsDone: function () { return iterations.sync == _.size(array) || iterations.break; },
    // synchronous iterations increment finished && async increment == sync incement => EXECUTE FINAL CALLBACK
    finalCallbackCheck: function (type) {
      if (iterations.syncIsDone() && iterations.sync == iterations.async) {
        // save type of finalCallback execution
        iterations.executedCallbackType = type;
        // execute callback
        finalCallback.apply(this, arguments);
      };
    },

    // async iteration increment and execute final callback if last iteration
    asyncIteration: function () {
      iterations.async++;
      iterations.finalCallbackCheck("async");
    },

  };


  for (i in array) {
    // synchronous iteration increment
    iterations.sync++

    // execute asked iteration function
    var funcReturn = iterationFunc(array[i], i, iterations.asyncIteration, iterations);
    //  ^^^^^^^^^^ in this function, asynchronous iteration will be incremented

    // if function returned break, stop here
    if (funcReturn == "break") {
      iterations.break = true;
      iterations.finalCallbackCheck("async");
      break;
    };
  };

  // execute callback here if async were faster than sync // ERROR: useless, it makes callback run twice
  // iterations.finalCallbackCheck("async");

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ASYNC EACH WITH PROMISE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: iterate an array, and return promise when done
    can handle heavy processing with setting how many entries to process at once (see maxInFlight option)
  ARGUMENTS: (
    !array <array> « array to iterate over »,
    !iterationFunc <function(
      value <any>,
      index <integer>,
      asyncIteration <function(ø)> « MUST be called in iterationFunc, return break to stop iteration »,
      iterations <{
        count: <integer> « number of entries in array that have been processed (or at least started to be) »,
        inFlightCounter: <integer> « a state of the iteration counting, for internal use (does not mean much from outside because it increase and decreases all the time in the progress, will be 0 when all done) »,
        break: <boolean> « if iteration broken by iterationFunc »,
        inflightRun: <function> « function ran to process x number of entries »,
        asyncIteration: <function(ø)> « decrease inFlightCounter and run inflightRun »,
      }> « internal object of asyncEach function »,
    )>,
    ?maxInFlight <integer:defaul=10> « how many elements will be iterated at once, if one iteration takes lots of calculation, reduce this number »,
  )
  RETURN: <promise>
*/
$$.asyncEachPromise = function (array, iterationFunc, maxInFlight) {

  var iterations = {

    // iterations
    count: 0,
    inFlightCounter: 0,
    maxInFlight: maxInFlight || 10,
    // iteration breaking
    break: false,

    // will be run until all entries have been processed
    // inflightRun: <function> // defined in promise

    // async iteration increment and execute final callback if last iteration
    asyncIteration: function () {
      iterations.inFlightCounter--;
      iterations.inflightRun();
    },

  };


  return new Promise(function (resolve, reject) {

    iterations.inflightRun = function () {

      // while room to run more, run them
      while (iterations.inFlightCounter < iterations.maxInFlight && iterations.count < array.length && !iterations.break) {

        iterations.count++;
        iterations.inFlightCounter++;
        var index = iterations.count-1;

        // iteration
        var funcReturn = iterationFunc(array[index], index, iterations.asyncIteration, iterations)

        // break case
        if (funcReturn == "break") {
          iterations.break = true;
          break;
          // TODO: should this launch reject() ???
        };

      };

      // all done here => pass to then
      if (iterations.inFlightCounter === 0 && (iterations.count >= array.length || iterations.break)) resolve();

    };

    // start looping
    iterations.inflightRun();

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
