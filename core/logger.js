var _ = require("underscore");
var $$ = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LEVEL CORRESPONDANCES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let levelCorrespondances = {
  debug:    [ "error", "success", "warning", "info", "debug" ],
  info:     [ "error", "success", "warning", "info"          ],
  warning:  [ "error", "success", "warning"                  ],
  error:    [ "error", "success",                            ],
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  TIME STAMP
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function stamp (timestamp) {
  var date = (new Date()).toJSON();
  if (timestamp == "minute") return date.replace("T", "_").replace(":", "h").replace(/\:[^\:]*$/, "m")
  else if (timestamp == "second") return date.replace("T", "_").replace(":", "h").replace(":", "m").replace(/\..*$/, "s")
  else if (timestamp == "millisecond") return date.replace("T", "_").replace(":", "h").replace(":", "m").replace(".", "s").replace("Z", "")
  else return date;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FIGURE OUT LOG TYPE COLOR
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function logTypeColor ($$loggerOptions) {
  return $$loggerOptions.logTypesColors[$$loggerOptions.logType] || $$loggerOptions.logTypesColors.other;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FIGURE OUT CONTEXT COLOR
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var contextColorCache = {};
function contextColor ($$loggerOptions) {
  var context = $$loggerOptions.context;

  // missing context
  if (!context) return loggerOptions.colors[0];

  // get context color from cache
  if (contextColorCache[context]) return contextColorCache[context];

  // make a unique colorNumber from the context string
  var colorNumber = 0;
  for (var i = 0; i < context.length; i++) {
    colorNumber += context.charCodeAt(i);
  };

  // save context color to cache
  contextColorCache[context] = $$loggerOptions.contextColors[colorNumber % $$loggerOptions.contextColors.length];

  // return the corresponding entry in available colors array
  return contextColorCache[context];

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  COLOR STRING FOR BROWSER OR CONSOLE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: send a pretty log to your chosen console
  ARGUMENTS: (
    !messages <any[]> « any ammount of messages to log »,
    ?logTypeOrOpts <"string"|$$.logger.options>,
  })
  RETURN: <void>
*/
function sendLog (messages, logTypeOrOpts) {

  //
  //                              COMMON

  // GET FULL OPTIONS
  var $$loggerOptions = $$.defaults(
    $$.logger.options,
    this.options,
    _.isString(logTypeOrOpts) ? { logType: logTypeOrOpts, } : logTypeOrOpts
  );

  // SKIP LOGGING IF LEVEL IS NOT APPROPRIATE
  if ($$loggerOptions.logType && _.indexOf(levelCorrespondances[$$loggerOptions.level], $$loggerOptions.logType) === -1) return;

  // FIGURE OUT IF TO USE CONSOLE.LOG, CONSOLE.ERROR...
  var consoleToUse = $$.logger.options.consoleToUse[$$loggerOptions.logType] || $$.logger.options.consoleToUse.other;

  // VARIABLES
  if (!_.isObject(messages[0])) {
    var message = messages[0];
    var additionalMessages = _.rest(messages);
  }
  else {
    var message = "";
    var additionalMessages = messages;
  };
  var context = $$loggerOptions.context;

  // FIGURE OUT CONTEXT AND LOG COLORS
  var colorForContext = contextColor($$loggerOptions);
  var colorForLog = $$loggerOptions.logColor || logTypeColor($$loggerOptions);

  //
  //                              LOG IN NODE

  if ($$.isNode) {

    // ADD ICON
    if ($$loggerOptions.displayIcon && $$loggerOptions.icons[$$loggerOptions.logType]) message = $$loggerOptions.icons[$$loggerOptions.logType] +" "+ message;

    // ADD CONTEXT
    if ($$.log.node) message = (
      $$.log.node.style(context +" |", colorForContext) +" "+
      $$.log.node.style(message, colorForLog)
    )
    else message = context +" | "+ message;

    // ADD TIMESTAMP
    if ($$loggerOptions.timestamp) message = stamp($$loggerOptions.timestamp) +" "+ message;

  }

  //
  //                              LOG IN BROWSER

  else var message = [
    ($$loggerOptions.timestamp ? stamp($$loggerOptions.timestamp) +" " : "") +"%c"+ context +" |%c "+ message,
    "color:" + colorForContext +";",
    "color:" + colorForLog +";"
  ];

  //
  //                              LOG

  consoleToUse.apply(this, _.flatten([message, additionalMessages], true));

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: create a logger attached to the given context to display
  ARGUMENTS: (
    contextOrOptions: <string|{
      !context: <string> « will be displayed at beginning of each log »,
      ... all keys in $$.logger.options
    }> « pass here either a context string or an object containing options for the logger (including context) »,
  )
  RETURN: <scopeFunction{
    main:    <function(...messages<any>):<void>> « normal log »,
    debug:   <function(...messages<any>):<void>> « debug log »,
    info:    <function(...messages<any>):<void>> « info log »,
    warning: <function(...messages<any>):<void>> « warning log »,
    success: <function(...messages<any>):<void>> « success log »,
    error:   <function(...messages<any>):<void>> « error log »,
    custom:  <function(message<any>, opts<$$.logger.options>):<void>> « customized log »,
  }>
*/
$$.logger = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see $$.logger
  */
  main: function (contextOrOptions) {

    // CREATE LOGGER
    var logger = $$.scopeFunction({
      main: function ()                { sendLog.call(logger, arguments);            },
      debug: function ()               { sendLog.call(logger, arguments, "debug");   },
      info: function ()                { sendLog.call(logger, arguments, "info");    },
      error: function ()               { sendLog.call(logger, arguments, "error");   },
      warning: function ()             { sendLog.call(logger, arguments, "warning"); },
      success: function ()             { sendLog.call(logger, arguments, "success"); },
      custom: function (message, opts) { sendLog.call(logger, [message], opts);      },
      options: _.isString(contextOrOptions) ? { context: contextOrOptions, } : contextOrOptions,
    });

    // RETURN LOGGER
    return logger;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      default options for $$.logger
      customizing these will affect all loggers created with $$.logger (past and future)
      however options passed at logger creation or at log sending have priority over those general ones
    TYPE: <{
      ?level: <"debug"|"info"|"warning"|"error"> «
        level of logs to display
        "debug" will display all logs
        "info" will display all logs except debug logs
        "warning" will display all logs except debug and info logs
        "error" will display only error and success logs
        whatever the level use, logs without level (the ones ran through $$log will be displayed)
      »,
      ?timestamp: <"minute"|"second"|"millisecond"|true>@default=false « should there be a timestamp displayed »,
      ?displayIcon: <boolean>@default=true,
      ?icons: <{
        ?log: <string>@default="",
        ?info: <string>@default="🛈",
        ?warning: <string>@default="⚠",
        ?success: <string>@default="✔",
        ?error: <string>@default="✖",
      }> « customize icons to use »,
      ?contextColors: <string[]> « the list of possible colors to use to style contexts »,
      ?logTypesColors: <{
        ?info: <string>@default="#0FF",
        ?warning: <string>@default="#FA0",
        ?success: <string>@default="#0F0",
        ?error: <string>@default="#F00",
        ?other: <string>@default="" « if not specified, will use this one »,
      }> « which color to use to log different types of logs »,
      ?consoleToUse: <{
        ?info: <string>@default=console.info,
        ?warning: <string>@default=console.warn,
        ?success: <string>@default=console.log,
        ?error: <string>@default=console.error,
        ?other: <string>@default=console.other « if not specified, will use this one »,
      }> « which function to use to send log message »,
      ?logColor: <colorString>@default=undefined « if defined, this color will be used to color the log message »,
    }>
  */
  options: {
    timestamp: false,
    displayIcon: true,
    level: "info",
    icons: {
      _recursiveOption: true,
      log: "",
      debug: "",
      info: "🛈",
      warning: "⚠",
      success: "✔",
      error: "✖",
    },
    contextColors: [
      '#0000CC', '#0000FF', '#0033CC', '#0033FF', '#0066CC', '#0066FF', '#0099CC', '#0099FF', '#00CC00', '#00CC33', '#00CC66', '#00CC99', '#00CCCC', '#00CCFF',
      '#3300CC', '#3300FF', '#3333CC', '#3333FF', '#3366CC', '#3366FF', '#3399CC', '#3399FF', '#33CC00', '#33CC33', '#33CC66', '#33CC99', '#33CCCC', '#33CCFF',
      '#6600CC', '#6600FF', '#6633CC', '#6633FF', '#66CC00', '#66CC33',
      '#9900CC', '#9900FF', '#9933CC', '#9933FF', '#99CC00', '#99CC33',
      '#CC0000', '#CC0033', '#CC0066', '#CC0099', '#CC00CC', '#CC00FF', '#CC3300', '#CC3333', '#CC3366', '#CC3399', '#CC33CC', '#CC33FF', '#CC6600', '#CC6633', '#CC9900', '#CC9933', '#CCCC00', '#CCCC33',
      '#FF0000', '#FF0033', '#FF0066', '#FF0099', '#FF00CC', '#FF00FF', '#FF3300', '#FF3333', '#FF3366', '#FF3399', '#FF33CC', '#FF33FF', '#FF6600', '#FF6633', '#FF9900', '#FF9933', '#FFCC00', '#FFCC33'
    ],
    logTypesColors: {
      _recursiveOption: true,
      debug: "",
      info: "#FF0",
      warning: "#FA0",
      success: "#0F0",
      error: "#F00",
      other: "",
    },
    consoleToUse: {
      _recursiveOption: true,
      error: console.error,
      warning: console.warn,
      success: console.log,
      info: console.info,
      other: console.log,
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});
