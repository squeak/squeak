var _ = require("underscore");
var $$ = require("./_");

/**
  DESCRIPTION: same as console.log
  ARGUMENTS: ( ...args )
  RETURN: <void>
*/
$$.log = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see $$.log
  */
  main: console.log,

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INFO, SUCCESS, WARNING, ERROR STYLES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: log a yellow message in console
    ARGUMENTS: (
      message <string>,
      bold <boolean> « if true, string will be logged bolded »
    )
    RETURN: <void>
  */
  infoColor:    function (msg, bold) {  return $$.log.style(msg, $$.isNode ? "yellowBright" : "#FF0", bold);  },

  /**
    DESCRIPTION: log a green message in console
    ARGUMENTS: (
      message <string>,
      bold <boolean> « if true, string will be logged bolded »
    )
    RETURN: <void>
  */
  successColor: function (msg, bold) {  return $$.log.style(msg, $$.isNode ? "greenBright" : "#0F0",  bold);  },

  /**
    DESCRIPTION: log an orange message in console
    ARGUMENTS: (
      message <string>,
      bold <boolean> « if true, string will be logged bolded »
    )
    RETURN: <void>
  */
  warningColor: function (msg, bold) {  return $$.log.style(msg, "#FA0",                              bold);  },

  /**
    DESCRIPTION: log a red message in console
    ARGUMENTS: (
      message <string>,
      bold <boolean> « if true, string will be logged bolded »
    )
    RETURN: <void>
  */
  errorColor:   function (msg, bold) {  return $$.log.style(msg, $$.isNode ? "redBright" : "red",     bold);  },

  /**
    DESCRIPTION:
      for browser: same as console.info
      for node: will log first argument with 🛈 in yellow and others normally
    ARGUMENTS: (
      !message <string> « main message »,
      ... <any> « any additional message »,
    )
    RETURN: <void>
  */
  info: $$.isNode ? function (message) {
    $$.log.infoColor(" 🛈 "+ message);
    _.each(_.rest(arguments), function (messg) { console.info(messg); });
  } : console.info,

  /**
    DESCRIPTION:
      for browser: same as console.log
      for node: will log first argument with ✔ in green and others normally
    ARGUMENTS: (
      !message <string> « main message »,
      ... <any> « any additional message »,
    )
    RETURN: <void>
  */
  success: $$.isNode ? function (message) {
    $$.log.successColor(" ✔ "+ message);
    _.each(_.rest(arguments), function (messg) { console.log(messg); });
  } : console.log,

  /**
    DESCRIPTION:
      for browser: same as console.warn
      for node: will log first argument with ⚠ in orange and others normally
    ARGUMENTS: (
      !message <string> « main message »,
      ... <any> « any additional message »,
    )
    RETURN: <void>
  */
  warning: $$.isNode ? function (message) {
    $$.log.warningColor(" ⚠ "+ message);
    _.each(_.rest(arguments), function (messg) { console.warn(messg); });
  } : console.warn,

  /**
    DESCRIPTION:
      for browser: same as console.error
      for node: will log first argument with ✖ in red and others normally
    ARGUMENTS: (
      !message <string> « main message »,
      ... <any> « any additional message »,
    )
    RETURN: <void>
  */
  error: $$.isNode ? function (message) {
    $$.log.errorColor(" ✖ "+ message);
    _.each(_.rest(arguments), function (err) { console.error(err); });
  } : console.error,

  /**
    DESCRIPTION: log a line
    ARGUMENTS: (
      !color <see $$.log.style>,
      !bold <see $$.log.style>,
      !style <see $$.log.style>,
    )
    RETURN: <void>
  */
  line: function (color, bold, style) {
    $$.log.style(Array($$.isNode ? process.stdout.columns : 50).join("—"), color, bold, style);
  },

  /**
    DESCRIPTION: same as console.group
    ARGUMENTS: ( ...args )
    RETURN: <void>
  */
  group: console.group,

  /**
    DESCRIPTION: same as console.groupEnd
    ARGUMENTS: ( ...args )
    RETURN: <void>
  */
  groupEnd: console.groupEnd,

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  UTILITIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: log an array of values, except the first one
    ARGUMENTS: ( ?args <any[]>, )
    RETURN: <void>
  */
  rest: function (args) {
    var restArgs = _.rest(args);
    if (restArgs) console.log.apply(this, restArgs);
  },

  /**
    DESCRIPTION: log errors really clearly
    ARGUMENTS: (
      ?context <string> « logged in red »
      !message <string> « logged in bold red »,
      ... <any> « any amount of other things to log »
    )
    RETURN: <void>
  */
  detailedError: function (context, message) {
    console.log("————————————————————————————————————————————————————————");
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    // log error context and message in red
    if (context) $$.log.errorColor(context, false);
    $$.log.errorColor(message, true);
    // log other arguments
    var otherArguments = _.rest(arguments, 2);
    if (otherArguments) _.each(otherArguments, function(toLog,i){ console.error(toLog); });
    console.log("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡");
    console.log("————————————————————————————————————————————————————————");
  },

  /**
    DESCRIPTION: same as detailedError but returns a callback function that executed will log detailed error messages
    ARGUMENTS: (
      ... any message you want to log before logging the arguments of the callback function
    )
    RETURN: <function>
  */
  detailedErrorCallback: function () {
    var detailedErrorArgs = arguments;
    return function () {
      return $$.log.detailedError.apply(this, _.flatten([detailedErrorArgs, arguments], true));
    };
  },

  /**
    DESCRIPTION: return and log in one line
    ARGUMENTS: (
      <any> « value to return »,
      ...arguments <any> « things to log »
    )
    RETURN: <any>
  */
  returnLog: function (returnFuncOrValue) {
    console.log.apply(this, _.rest(arguments)); // Maybe could make styling of log like alerts, errors, (search into different way to log)
    return returnFuncOrValue; // for now, if it's a function result I want, it has to be executed when calling
  },

  /**
    DESCRIPTION: customize display of a log message
    ARGUMENTS: (
      msg <string> « message to log (¡¡ it will be stringified !!) »,
      color <string> « color of log message (in node, must be a key of $$.log.chalk.colors) »,
      bold <boolean> « (ignored in node) »,
      style <string> « any css styling you'd like (ignored in node) »,
    )
  */
  style: function (msg, color, bold, style) {

    // LOG IN NODE // NOTE: ignores additional styles
    if ($$.isNode) {
      if ($$.log.node) console.log($$.log.node.style(msg, color, { bold: bold, }))
      else {
        if (!missingLogNodeDependencyChecked) {
          missingDependency("If you want support for colors and style in node logs, you need to install log.node plugin!");
          missingLogNodeDependencyChecked = true;
        };
        console.log(msg);
      };
    }

    // LOG IN BROWSER
    else {
      var resultStyle = style ? style.replace(/\;*$/, ";") : "";
      if (color) resultStyle += "color:" + color +";";
      if (bold) resultStyle += "font-weight:bold;";
      console.log("%c" + msg, resultStyle);
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

var missingLogNodeDependencyChecked = false;
function missingDependency (msg) {
  if ($$.isNode) console.log("\x1b[41m\x1b[30m\x1b[1m\x1b[37m"+ msg +"\x1b[0m")
  else $$.log.style(msg, "red", true);
};

// error: function (message, httpCode) {
//   var error = new Error(message);
//   if (httpCode) error.http_code = httpCode;
//   return error;
// },

module.exports = $$;
