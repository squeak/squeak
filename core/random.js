var _ = require("underscore");
var $$ = require("./_");

$$.random = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: generate random number
    ARGUMENTS: (
      !min <number|[<number>, <number>] « if array, max is not necessary »,
      ?max <number> « is ignored if min is an array »,
    )
    RETURN: <number>
    EXAMPLES:
      $$.random.number(0, 1); // 0.41185778518081506
  */
  number: function (min, max) {
    if (_.isArray(min)) return $$.random.number(min[0], min[1])
    else if (_.isUndefined(max)) return min
    else return Math.random() * (max - min) + min;
  },

  /**
    DESCRIPTION: generate random integer
    ARGUMENTS: ( see $$.random.number )
    RETURN: <integer>
    EXAMPLES:
      $$.random.integer(0, 100); // 29
      $$.random.integer(0, 100); // 1
  */
  integer: function (min, max) {
    if (_.isArray(min)) return $$.random.integer(min[0], min[1])
    else if (_.isUndefined(max)) return min
    else return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  /**
    DESCRIPTION: generate random percentage
    ARGUMENTS: ( see $$.random.number )
    RETURN: <string>
    EXAMPLES:
      $$.random.percentage(); // "85%"
      $$.random.percentage(10, 20); // "13%"
  */
  percentage: function (min, max) { return $$.random.integer(min || 0, max || 100) +"%"; },

  /**
    DESCRIPTION: generate an integerBetwwen 0 and 255
    ARGUMENTS: ( ø )
    RETURN: <integer>
  */
  numberColor: function () { return $$.random.integer(0,255); },

  /**
    DESCRIPTION: generate random color
    ARGUMENTS: (
      ?transparency <
        | number between 0 and 1 « set the opacity you want to have 0 is completely transparent, 1 completely opaque »
        | "noTransparency" | false « fully opaque random color »
        | [<number between 0 and 1>, <number between 0 and 1>] « range of possible transparencies »
      >
    )
    RETURN: <rgbaColorString>
    EXAMPLES:
      $$.random.color(); // "rgba(112,182,5,0.3362927011416571)"
      $$.random.color(1); // "rgba(130,18,77,1)"
  */
  color: function (transparency) {
    if (transparency == "noTransparency" || transparency === false) transparency = 1
    else if (typeof transparency == "number") transparency = transparency
    else if (_.isArray(transparency)) transparency = $$.random.number(transparency)
    else transparency = $$.random.number(0,1);
    return "rgba("+$$.random.numberColor()+","+$$.random.numberColor()+","+$$.random.numberColor()+","+transparency+")"
  },

  /**
    DESCRIPTION: return randomly false or true
    ARGUMENTS: (
      ?probability <number between 0 and 1> « the closer this is to 1, the more it's likely to be true »
    )
    RETURN: <boolean>
    EXAMPLES:
      $$.random.boolean();    // false (has 50% of change to be true, 50% to be false)
      $$.random.boolean(0.1); // false (has 10% of change to be true, 90% to be false)
  */
  boolean: function (probability) {
    // not defined
    if (typeof probability == "undefined") probability = 0.5
    // null or 0 return always false
    else if (probability == null || probability == 0) return false;
    // rest
    return Math.random() < probability;
  },

  /**
    DESCRIPTION: choose random entry in array
    ARGUMENTS: ( <array> )
    RETURN: <any>
    EXAMPLES:
      $$.random.entry([ "a", "list", { of: "values", yep: 999, } ]); // { of: "values", yep: 999, }
      $$.random.entry([ "a", "list", { of: "values", yep: 999, } ]); // "a"
  */
  entry: function (array) {
    if (_.isArray(array) || _.isString(array)) return array[$$.random.integer(0, array.length-1)]
    else if (_.isObject(array)) return array[$$.random.entry(_.keys(array))]
    else {
      $$.log.detailedError(
        "$$.random.entry",
        "Type of given 'array' is neither an array nor an object. Hard to choose a random entry",
        array,
        "(the 'array' itself has been returned)"
      );
      return array;
    };
  },

  /**
    DESCRIPTION:
      returns a random number, but applying a log10 transformation of the min and max values
      the benefit of this is to give equal importance to the space between 0 and 1 and between 1 and infinity
      for example there will be the same probability to get a number between 0.001 and 1, than between 1 and 1000
    ARGUMENTS: (
      !min: <number>,
      !max: <number>,
    )
    RETURN: <number>
  */
  number_log10: function (min, max) {
    var logmin = Math.log10(min);
    var logmax = Math.log10(max);
    return Math.pow(10, $$.random.number(logmin, logmax));
  },

  /**
    DESCRIPTION:
      will return a random ratio between the two given ones
      if you pass only one ratio, it will be returned as a number
    ARGUMENTS: (
      !min <string|[string, string]> « e.g. "4/3" »,
      ?max <string> « e.g. "16/9" »,
    )
    RETURN: <number>
    EXAMPLES:
      $$.random.ratio("4/3", "16/9"); // 0.3280920194485836
  */
  ratio: function (min, max) {
    if (_.isArray(min) && min.length == 2) return $$.random.ratio(min[0], min[1])
    if (_.isString(min)) {
      MIN = min.split("/");
      min = Math.abs(MIN[0]) / Math.abs(MIN[1]);
    };
    if (_.isString(max)) {
      MAX = max.split("/");
      max = Math.abs(MAX[0]) / Math.abs(MAX[1]);
    };
    return $$.random.number_log10(min, max);
  },

  /**
    DESCRIPTION: get random left/right top/bottom orientations
    ARGUMENTS: (
      ?probabilityH <number between 0 and 1> « the closer to 0, the more it's probable that it'll be "left" »,
      ?probabilityV <number between 0 and 1> « the closer to 0, the more it's probable that it'll be "top" »,,
    )
    RETURN: <{
      h: <"left"|"right">,
      v: <"top"|"bottom">,
    }>
    EXAMPLES:
      $$.random.direction();     // { h: "left",  v: "bottom", }
      $$.random.direction(1, 1); // { h: "left",  v: "top",    }
      $$.random.direction(0, 0); // { h: "right", v: "bottom", }
  */
  direction: function (probabilityH, probabilityV) {
    return {
      h: $$.random.boolean(probabilityH) ? "left" : "right",
      v: $$.random.boolean(probabilityV) ? "top" : "bottom",
    };
  },

  /**
    DESCRIPTION: randomly generate a password
    ARGUMENTS: (
      ?length <integer|[integer, integer]>@default=40 « if it is an array, will create a password of random length between the two given lengths »,
      ?allowedCharacters
        <string|string[]>
        @default="π¤ÒÍ#g≠)Œ>Ó§IqGWexoΠÂYD:lUsµ.zp,√·PàÏBAÎHM|V7m8j3cuSh¢Ê©=Æk@TÌFÚÅ¬Ω®÷<E+œf2∫&4™réê¨CQ€$t1X;iª0ßδvRΔ~y%†£ºçw?9bZÈïNîa♦…^J/dæ!KOËn(*‡ŸΣè"
        « will get any character in the string or any element in the array as individual character »,
    )
    RETURN: <string>
  */
  password: function (length, possibleCharacters) {

    // default options
    if (!length) length = 40;
    if (!possibleCharacters) possibleCharacters = "π¤ÒÍ#g≠)Œ>Ó§IqGWexoΠÂYD:lUsµ.zp,√·PàÏBAÎHM|V7m8j3cuSh¢Ê©=Æk@TÌFÚÅ¬Ω®÷<E+œf2∫&4™réê¨CQ€$t1X;iª0ßδvRΔ~y%†£ºçw?9bZÈïNîa♦…^J/dæ!KOËn(*‡ŸΣè";

    // generate password
    var secret = "";
    for (i=0; i<$$.random.integer(length); i++) {
      secret += $$.random.entry(possibleCharacters);
    };
    return secret;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = $$;
