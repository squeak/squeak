var _ = require("underscore");
var $$ = require("./_");
var detectPointer = require("detect-pointer").default;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: return the type of the value (same as typeof but returns "array" and not "object" for javascript arrays)
  ARGUMENTS: ( <any> )
  RETURN: <string>
*/
$$.typeof = function (val) {
  if (_.isArray(val)) return "array"
  else if (val === null) return "null"
  else return typeof val;
};

/**
  DESCRIPTION: test if given value is an array, function or object
  ARGUMENTS: ( <any> )
  RETURN: <"array"|"function"|"object">
  EXAMPLES:
    $$.objectType([1, 2, 3]); // "array"
    $$.objectType({}); // "object"
    $$.objectType(function(){}); // "function"
*/
$$.objectType = function (object) {
  if (_.isArray(object)) return "array"
  else if (_.isFunction(object)) return "function"
  else return "object";
};

/**
  DESCRIPTION: test if the value is a javascript object (= {}) (= not an array or function)
  ARGUMENTS: ( <any> )
  RETURN: <boolean>
  EXAMPLES:
    $$.isObjectLiteral({}); // true
    $$.isObjectLiteral([]); // false
    $$.isObjectLiteral(function(){}); // false
*/
$$.isObjectLiteral = function (object) {
  return _.isObject(object) && !_.isArray(object) && !_.isFunction(object) && !$$.isElement(object);
};

/**
  DESCRIPTION: test if the value is a javascript object or array (= {}|[]) (= not a function)
  ARGUMENTS: ( <any> )
  RETURN: <boolean>
  EXAMPLES:
    $$.isObjectOrArray({}); // true
    $$.isObjectOrArray([]); // true
    $$.isObjectOrArray(function(){}); // false
*/
$$.isObjectOrArray = function (object) {
  return _.isArray(object) || $$.isObjectLiteral(object);
};

/**
  DESCRIPTION: test if string is proper json
  ARGUMENTS: ( <any> )
  RETURN: <boolean>
  EXAMPLES:
    $$.isJson('{ "al": "bert" }'); // true
    $$.isJson('al bert'); // flase
*/
$$.isJson = function (string) {
  try { JSON.parse(string); return true }
  catch (err) { return false; };
};

/**
  DESCRIPTION:
    test if the given value is a javascript Element
    in node, this will always answer false, because the Element type doesn't exist
  ARGUMENTS: ( <any> )
  RETURN: <boolean>
  EXAMPLES:
    $$.isElement(document.getElementById("elementPresentInPage")); // true
    $$.isElement(document.getElementById("elementNotPresentInPage")); // false
*/
$$.isElement = function (elem) {
  return $$.isNode ? false : elem instanceof Element;
};

/**
  DESCRIPTION: test if the given value is a valid jquery element (containing at least one element)
  ARGUMENTS: (
    elem <any>
    laxist <boolean>@default=false « if true will accept the jquery element, even if it's empty (= it doesn't contain any elements) »,
  )
  RETURN: <boolean>
  EXAMPLES:
    $$.isJqueryElement($("#elementPresentInPage")); // true
    $$.isJqueryElement($("#elementNotPresentInPage")); // false
    $$.isJqueryElement($("#elementNotPresentInPage", true)); // true
*/
$$.isJqueryElement = function (elem, laxist) {
  // not really checking properly if it's a jquery object with instanceof jquery, because squeak shouldn't depend on jquery
  return elem && elem.jquery && (laxist || $$.isElement(elem[0]));
};

/**
  DESCRIPTION: test if given value is a collection (= an array of objects)
  ARGUMENTS: (
    coll <any>,
    fast <boolean> « if true will only check first and last elements in array »,
  )
  RETURN: <"array"|"function"|"object">
  EXAMPLES:
    $$.isCollection([{ a:1, } { a: 2, }, { c: "d" }]); // true
    $$.isCollection([1, {}, 3]); // false
    $$.isCollection([1, {}, 3], true); // true
*/
$$.isCollection = function (collection, fast) {
  // is not even an array
  if (!_.isArray(collection)) return false
  // just check first and last entry
  else if (fast) return $$.isObjectLiteral(collection[0]) && $$.isObjectLiteral(object[collection.length-1])
  // check if at least one entry is not an object
  else {
    var nonObjectEntries = _.find(collection, function (entry) { return !$$.isObjectLiteral(entry) });
    return nonObjectEntries ? false : true;
  };
};

/**
  DESCRIPTION: test if the given value is an appropriate absolute folder path (mean it's one line and starts and ends with "/")
  ARGUMENTS: ( <any> )
  RETURN: <boolean>
  EXAMPLES:
    $$.isAbsoluteFolderPath("/absolute/path/to/directory/"); // true
    $$.isAbsoluteFolderPath("/absolute/path/to/file"); // false
    $$.isAbsoluteFolderPath("relative/path/to/directory/"); // false
    $$.isAbsoluteFolderPath("/"); // true
*/
$$.isAbsoluteFolderPath = function (string) {
  if (!_.isString(string)) return false
  else if (string == "/") return true
  else if (string.match(/^\/.+\/$/)) return true
  else return false;
};

/**
  DESCRIPTION: test if the given value is an appropriate array of absolute folder paths
  ARGUMENTS: ( <any> )
  RETURN: <boolean>
  EXAMPLES:
    $$.isAbsoluteFolderPathsArray(["/path/to/directory/", "/other/directory/"]); // true
*/
$$.isAbsoluteFolderPathsArray = function (stringsArray) {
  if (!_.isArray(stringsArray)) return false
  else if (_.every(stringsArray, $$.isAbsoluteFolderPath)) return true
  else return false;
};

/**
  DESCRIPTION: test if a given number is between the other given ones
  ARGUMENTS: (
    !number <number>,
    !range <number[]> « range doesn't have to be sorted »,
    ?strict <boolean> « if true, will use > and< comparers else will use <= >= »,
  )
  RETURN: <boolean>
*/
$$.isBetween = function (number, range, strict) {
  // make sure range is sorted
  var sortedRange = _.sortBy(range);
  // check strictly
  if (strict) return number > sortedRange[0] && number < sortedRange[sortedRange.length-1] ? true : false
  // check between or equal
  else return number >= sortedRange[0] && number <= sortedRange[sortedRange.length-1] ? true : false;
};

/**
  DESCRIPTION:
    same as _.isEqual, but compact all object values before
    if some keys are undefined in one and absent in the other, it answers true (see the example for a simple illustration)
    this is useful to compare entries that will then be stringified to json (for example to be saved in a database)
  ARGUMENTS: (
    !arg1 <any>,
    !arg2 <any>,
  )
  RETURN: <boolean>
  EXAMPLES:
    $$.isEqual({ key: "value", otherKey: undefined, }, { key: "value", }); // true
*/
$$.isEqual = function (arg1, arg2) {
  if (_.isObject(arg1)) arg1 = JSON.parse(JSON.stringify(arg1));
  if (_.isObject(arg2)) arg2 = JSON.parse(JSON.stringify(arg2));
  return _.isEqual(arg1, arg2);
};

/**
  DESCRIPTION: check if the current device is a touch device
  ARGUMENTS: ( ø )
  RETURN: <boolean>
*/
$$.isTouchDevice = function () {
  return detectPointer.anyCoarse || detectPointer.anyNone;
};

// removed because it doesn't seem useful
// $$.devicePixelProportion = $$.isTouchDevice() ? 2.3 : 1;

/**
  DESCRIPTION: test if the given value is a javascript File
  ARGUMENTS: ( <any> )
  RETURN: <boolean>
*/
$$.isFile = function (object) {
  // return object.constructor == File;
  return object instanceof File;
};

/**
  DESCRIPTION: test the given variable is a blob
  ARGUMENTS: ( val <any> « the value you want to test » )
  RETURN: <boolean>
*/
$$.isBlob = function (val) {
  return val instanceof Blob;
};

/**
  DESCRIPTION: verify that the passed string, look like a regexp (= contains a slash first and another slash (plus any amount of flags) at end)
  ARGUMENTS: ( ?string <any> )
  RETURN: <boolean>
*/
$$.isRegExpString = function (string) {
  return _.isString(string) && string.match(/^\//) && string.replace(/^\//, "").match(/\/[gmi]*$/);
};

/**
  DESCRIPTION: allow to test if multiple values are undefined
  ARGUMENTS: ( ... <any> )
  RETURN: <boolean>
  EXAMPLES:
    $$.isUndefined(true, false, undefined);      // false
    $$.isUndefined(undefined, false, undefined); // false
    $$.isUndefined(undefined, undefined);        // true
*/
$$.isUndefined = function () {
  for (var i = 0; i < arguments.length; i++) { if (!_.isUndefined(arguments[i])) return false; };
  return true;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
