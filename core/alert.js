var _ = require("underscore");
var $$ = require("./_");

/**
  DESCRIPTION: alert a message
  ARGUMENTS: ( message <string> )
  RETURN: <void>
  EXAMPLES:
    $$.alert("A message that will be alerted.")
*/
$$.alert = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see $$.alert
  */
  main: function (message) {
    if ($$.isNode) $$.log.detailedError("squeak.alert", "Cannot alert in node!")
    else return window.alert(message);
  },

  /**
    DESCRIPTION: alert and log an error
    ARGUMENTS: ( message <string> )
    RETURN: <void>
    EXAMPLES:
      $$.alert.error("An error message that will be alerted.")
  */
  error: function (message) {
    $$.log.error.apply(window, arguments);
    return alert.apply(window, arguments)
  },

  /**
    DESCRIPTION: alert and log an info
    ARGUMENTS: ( message <string> )
    RETURN: <void>
    EXAMPLES:
      $$.alert.info("An info message that will be alerted.")
  */
  info: function (message) {
    $$.log.info.apply(window, arguments);
    return alert.apply(window, arguments)
  },

  /**
    DESCRIPTION: alert and log a warning
    ARGUMENTS: ( message <string> )
    RETURN: <void>
    EXAMPLES:
      $$.alert.warning("A warning message that will be alerted.")
  */
  warning: function (message) {
    $$.log.warning.apply(window, arguments);
    return alert.apply(window, arguments)
  },

  /**
    DESCRIPTION: alert and log a detailed error
    ARGUMENTS: ( see $$.log.detailedError )
    RETURN: <void>
    EXAMPLES:
      $$.alert.detailedError(
        "file/calling/this:method",
        "Error message",
        "... any additional options"
      )
  */
  detailedError: function (context, message) {
    $$.log.detailedError.apply($$.log.detailedError, arguments);
    alert(context +"\n"+ message);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = $$;
