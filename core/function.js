var _ = require("underscore");
var $$ = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RESULT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    return the given value, or if it's a function, the value it returns on execution
    passes arguments and context to function
  ARGUMENTS: (
    !given <any|function> « value or function »,
    ?...args « any other argument will be passed to the function (if given is a function) »
  )
  RETURN: <any>
  EXAMPLES:
    $$.result("blob"); // "blob"
    $$.result(function (arg1) { return arg1; }, "blob"); // "blob"
*/
$$.result = function (given) {
  if (_.isFunction(given)) return given.apply(this, _.rest(arguments))
  else return given;
};

/**
  DESCRIPTION: same as $$.result but for every value in an array or object
  ARGUMENTS: (
    <object|array>,
    ?...args « any other argument will be passed to the function if it's one »
  )
  RETURN: <object|array>
  EXAMPLES:
    $$.result(["blob", "blib"]); // ["blob", "blib"]
    $$.result([function (arg1, arg2) { return arg1; }, "blob", function (arg1, arg2) { return arg2; }], "blob", "blib"); // ["blob", "blib"]
*/
$$.results = function (object) {
  var resultsArguments = arguments;
  return _[_.isArray(object) ? "map" : "mapObject"](
    object,
    function (val, i) {
      var args = _.rest(resultsArguments);
      args.unshift(val);
      return $$.result.apply(this, args, true);
    }
  );
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  QUEUE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: return a function that if executed will execute in order all functions passed as argument
  ARGUMENTS: (
    ...args <function( ...args <any>, )>
  )
  RETURN: <function( ...args <any> )>
  EXAMPLES:
    let queuedFunctions = $$.queue(
      function (passedArg) { testValue += " text string"; },
      function (passedArg) { testValue += " saying "; },
      function (passedArg) {
        testValue += passedArg;
        return testValue;
      }
    );
    let testValue = "some";
    queuedFunctions("hello"); // "some text string saying hello"
    testValue = "another"
    queuedFunctions("hi");    // "another text string saying hi"
*/
$$.queue = function () {
  var queueArgs = arguments;
  return function () {
    var self = this;
    var calledFuncArgs = arguments;
    var result;
    _.each(queueArgs, function (arg) {
      if (_.isFunction(arg)) result = arg.apply(self, calledFuncArgs);
    });
    return result;
  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  METHODS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: attach methods to an object, passing it as context (this) for those functions execution
  ARGUMENTS: (
    !object <object> « object to which attach methods »,
    !methods <object of which each value is a function> « list of methods to attach »,
    ?options <{
      ?context: <object> « if you don't want the object to be the context for methods, you can pass a custom context here (the context is `this` in a function) »,
      ?ifExist: <"ignore"|"bind"|"replace">@default="bind" «
        what to do if a method with the same name already exist in the object:
          "replace" will replace the value by the new value
          "bind" will replace the value by the new value, but only if that new value is a method, otherwise the previous one will be kept
          "ignore" or any other value will just not change the key:value in the object
      »,
      ?attachNonFunctions: <boolean>@default=true «
        if true, non function values will also be attache to the result object
        if false, they will be ignored
      »,
      ?cloneObject: <boolean>@default=true « pass false to attach methods directly to the passed object »,
    }>,
  )
  RETURN: <object> « clone of the passed object, with the passed methods attached and bound to it »
  EXAMPLES:
    let myBookObject = $$.methods(
      {
        title: "The World as Will and Representation",
      },
      {
        getTitleAndAuthor: function () {
          return this.author +" — "+ this.title;
        },
        setTitleAndAuthor: function (string) {
          let titleAndAuthorArray = string.split(" — ");
          this.author = titleAndAuthorArray[0];
          this.title = titleAndAuthorArray[1];
          return string;
        },
      }
    );
    myBookObject.author = "Arthur Schopenhauer";
    myBookObject.getTitleAndAuthor(); // "Arthur Schopenhauer — The World as Will and Representation"
    myBookObject.setTitleAndAuthor("Samuel Beckett — First Love"); // "Samuel Beckett — First Love"
    myBookObject.getTitleAndAuthor(); // "Samuel Beckett — First Love"
*/
$$.methods = function (obj, methods, options) {

  //
  //                              MAKE SURE TO WORK ON A BRAND NEW OBJECT BEFORE MAKING ANY CHANGE (!IMPORTANT)

  if (options && options.cloneObject !== false) var object = _.clone(obj)
  else var object = obj;

  //
  //                              DEFAULTS

  var defaultOptions = {
    ifExist: "bind",
    context: object,
    attachNonFunctions: true,
    cloneObject: true,
  };
  options = $$.defaults(defaultOptions, options);

  //
  //                              ATTACH METHOD FUNC

  function attachMethod (object, method, methodName, options) {
    // attach bound method
    if (_.isFunction(method)) object[methodName] = _.bind(method, options.context)
    // attach non function method if asked
    else if (options.attachNonFunctions) object[methodName] = method;
  };

  //
  //                              ATTACH METHODS

  _.each(methods, function (method, methodName) {

    // method seem to exist already
    if (object[methodName]) {
      if (options.ifExist == "replace") attachMethod(object, method, methodName, options)
      else if (options.ifExist == "bind") {
        if (_.isFunction(object[methodName])) attachMethod(object, method, methodName, options)
        else $$.log.detailedError("squeak.methods", "Cannot bind given method, it's not a function.", object[methodName], "key: "+ methodName +" in:", object);
      };
    }

    // method doesn't already exist
    else attachMethod(object, method, methodName, options);

  });

  //
  //                              RETURN

  return object;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INCREMENTER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: store increment value for a group, each time you call the method, it will return the last returned value for this group +1
  ARGUMENTS: (
    !group <string>
    ?howMuch <integer>@default=1 « how much to increment »
  )
  RETURN: <integer>
  EXAMPLES:
    $$.increment("books"); // 1
    $$.increment("books"); // 2
    $$.increment("films"); // 1
    $$.increment("books"); // 3
    $$.increment.minus("books"); // 2
    $$.increment.reset("films"); // 0
    $$.increment.get("films"); // 0
    $$.increment.get("books"); // 2
*/
$$.increment = $$.scopeFunction({

  /**
    see $$.increment
  */
  main: function () { return $$.increment.plus.apply(this, arguments); },

  /**
    see $$.increment ($$.increment is in fact an alias of $$.increment.plus)
  */
  plus: function (group, howMuch) {
    if (_.isUndefined($$.increment.groups[group])) $$.increment.groups[group] = 0;
    return $$.increment.groups[group] = $$.increment.groups[group] + (howMuch || 1);
  },

  /**
    DESCRIPTION: same as $$.increment.plus but instead of adding, substracts
    ARGUMENTS: ( see $$.increment )
    RETURN: see $$.increment
  */
  minus: function (group, howMuch) {
    if (_.isUndefined($$.increment.groups[group])) $$.increment.groups[group] = 0
    return $$.increment.groups[group] = $$.increment.groups[group] - (howMuch || 1);
  },

  /**
    DESCRIPTION: get how many times increment has been ran for a group
    ARGUMENTS: ( !group <string> )
    RETURN: <integer>
  */
  get: function (group) {
    return $$.increment.groups[group];
  },

  /**
    DESCRIPTION: reset incrementer for this group to 0
    ARGUMENTS: (
      !group <string>,
      ?value <integer>@default=0,
    )
    RETURN: <0|integer>
  */
  reset: function (group, value) {
    return $$.increment.groups[group] = value || 0;
  },

  /**
    DESCRIPTION: list of counts for incrementators
    TYPE: <{ [groupName]: <integer> }>
  */
  groups: {},

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
