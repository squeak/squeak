var _ = require("underscore");
var sugarObjectGet = require("sugar/object/get");
var sugarObjectSet = require("sugar/object/set");
var sugarObjectHas = require("sugar/object/has");
// var sugarObjectReject = require("sugar/object/reject");
var $$ = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: get object value, with deep.key string
  ARGUMENTS: (
    !object <object>,
    !keyString <string> «
      deep.key
      check sugar.object.set for details on all the possibilities of syntax you can use
    »
  )
  RETURN: <any>
  EXAMPLES:
    $$.getValue({ deep: { key: 1, }}, "deep.key"); // 1
    $$.getValue({ deep: { key: 1, }}, "deep"); // { key: 1, }
*/
$$.getValue = function (object, keyString) {
  try { return sugarObjectGet(object, keyString); }
  catch (err) { return undefined; }; // in case something on the way is wrongly defined (asked array but object for example)
};

/**
  DESCRIPTION:
    get a value in object specifying a key or a function to get it
    if you want this function
  ARGUMENTS: (
    !object <object>,
    ?keyOrFuncOrArray <
      | function(object)
      | string
      | number
      | an array of the previous ones
    > «
      - if keyOrFunc is a function, it will be executed with the object as argument and it's return will be returned
      - if it's a string or number, it will return $$.getValue(object, keyOrFunc)
      - if it's an array, it will return an array of executions of $$.getValueOrResult on the entry
    »,
    ?defaultValue <any> « a value to return in case the gotten value is undefined »,
  )
  RETURN: <any>
  EXAMPLES:
    $$.getValueOrResult({ date: "2213-12-18", ev: "hello", }, "date"); // "2213-12-18"
    $$.getValueOrResult({ date: "2213-12-18", ev: "hello", }, function (obj) { return obj.ev }); // "hello"
    $$.getValueOrResult(
      { h: "hello", y: { o: { u: "you" } } },
      [ "h", "y.o.u", ]
    ); // ["hello", "you"]
    $$.getValueOrResult.call(
      { h: "hello", },
      { b: "brave", w: "world", },
      [
        function (obj) { return this.h +" "+ obj.b +" new";  },
        "w"
      ]
    ); // [ "hello brave new", "world" ]
*/
$$.getValueOrResult = function (entry, keyOrFuncOrArray, defaultValue) {
  var self = this;

  // map array of keys or functions and apply $$.getValueOrResult to them
  if (_.isArray(keyOrFuncOrArray)) return _.map(keyOrFuncOrArray, function (keyOrFunc) {
    return $$.getValueOrResult.call(self, entry, keyOrFunc, defaultValue);
  })

  // return the result of the function
  else if (_.isFunction(keyOrFuncOrArray)) var result = keyOrFuncOrArray.call(this, entry)

  // return $$.getValue result
  else var result = $$.getValue(entry, keyOrFuncOrArray);

  // return result or default value
  return _.isUndefined(result) ? defaultValue : result;

};

// getValue: function(object, keyString){
//   var keys = keyString.split(".");
//   var string = "object";
//   // make sure all keys on the way exist
//   for (i = 0; i < keys.length; i++) {
//     string += "."+ keys[i]
//     try { eval(string); }
//     catch (err) { return $$.log.returnLog(null, "Cannot find, keys don't exist on the way"+ objString); };
//   }
//   // if no problem return value
//   return eval(string);
// },


/**
  DESCRIPTION:
    set object value, with deep.key string
    ⚠ note that it modifies the given object
  ARGUMENTS: (
    !object <object>,
    !keyString <string> «
      very.deep.key
      check sugar.object.set for details on all the possibilities of syntax you can use
      if the value you're setting is undefined, since sugar doesn't handle setting undefined value, $$.setValue will fall back on $$.removeValue which is not supporting as many syntax as sugar, so be careful with this
      so if you wish to remove a key or set it undefined, it's better to use directly $$.removeValue
    »,
    !value <any>,
  )
  RETURN: <object>
  EXAMPLES:
    $$.setValue({ deep: { key: 1, }}, "deep.key", "yo"); // { deep: { key: "yo", }}
    $$.setValue({ deep: { key: 1, }}, "deep.otherKey", "ya"); // { deep: { key: 1, otherKey: "ya" }}
*/
$$.setValue = function (object, keyString, value) {
  if (_.isUndefined(value)) $$.removeValue(object, keyString)
  else return sugarObjectSet(object, keyString, value);
};


/**
  DESCRIPTION: check if a value exist in the object (if it's key is present)
  ARGUMENTS: (
    !object <object>,
    !keyString <string> «
      very.deep.key
      check sugar.object.has for details on all the possibilities of syntax you can use
    »,
  )
  RETURN: <boolean>
  EXAMPLES:
    $$.hasValue({ deep: { key: 1, }}, "deep.key"); // true
    $$.hasValue({ deep: { key: 1, }}, "deep.otherKey"); // false
*/
$$.hasValue = function (object, keyString) {
  return sugarObjectHas(object, keyString);
};


/**
  DESCRIPTION:
    remove a key from an object
    ⚠ note that it modifies the given object
  ARGUMENTS: (
    !object <object|array>,
    !keyString <string> «
      only support simple deep.key dot syntax
      for an array, write it like this: array.0.objectInArray.deep.key
    »,
  )
  RETURN: <object>
  EXAMPLES:
    $$.removeValue({ deep: { key: 1, }}, "deep.key");      // { deep: {} }
    $$.removeValue({ deep: { key: 1, }}, "deep");          // {}
    $$.removeValue({ deep: { key: 1, }}, "deep.otherKey"); // { deep: { key: 1, }}
    $$.removeValue([ { deep: { key: 1, otherKey: 2, } }, ], "0.deep.key"); // [{ deep: { otherKey: 2, }}]
    $$.removeValue({ deep: { key: 1, }}, "something.that.does.not.exist"); // { deep: { key: 1, }}
*/
$$.removeValue = function (object, keyString) {

  var objectChanged = object;

  var deepKey = (keyString +"").split(".");

  // iterate all keys to recurse in object
  $$.breakableEach(deepKey, function (partialKey, i) {
    // if the key already doesn't exist, just break here and even stop recursing deeper, there's just nothing that need to be done
    if (!_.has(objectChanged, partialKey)) return "break";
    // iterate further if not last partialKey
    if (i < deepKey.length-1) objectChanged = objectChanged[partialKey]
    // remove, if last.sub.__key__
    else {
      if (_.isArray(objectChanged)) entryValue.splice(partialKey, 1)
      else delete objectChanged[partialKey];
    };
  });

  return object;

  // var key
  // $$.getValue(object, keyStr)
  // return sugarObjectReject(object, keyString);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$;
